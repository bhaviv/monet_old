<?php
/**
 * Template Name: Sport Review Template
 */

/**
 * For some template we allow the editor to choose a custom background image.
 * The value will contain the full url of the image and will be used to display the image.
 *
 * @ver string
 */
get_header();
if ( have_posts() ) while ( have_posts() )  the_post();
?>
    <div class="container">
        <!-- Arrows -->
        <?php include "reuse/review/arrows.php" ?>
        <!-- End Arrows -->

        <!-- Review Coupon -->
        <?php include "reuse/review/coupon.php"; ?>
        <!-- End Review Coupon -->

        <!-- Review Title -->
        <div class="row">
            <div class="col-md-12">
                <h2 class="review-title">Rating</h2>
            </div>
        </div>
        <!-- End Review Title -->

        <!-- Review Rating -->
        <?php review_rating('betting'); ?>
        <!-- End Review Title -->

        <?php
        if(!empty(get_field('video_url'))):?>
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Video</h2>
                <!-- End Review Title -->

                <!-- Review Video -->
                <div class="review-video-container">
                    <iframe class="video center-block" width="" height="" role="application" src="<?php the_field('video_url'); ?>" frameborder="0" allowfullscreen=""></iframe>
                </div>
                <!-- End Review Video -->
            </div>
        </div>
         <?php
        endif;
        ?>

        <!-- Overview -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Overview</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_content(); ?>
            </div>
        </div>
        <!-- End Overview -->

        <!-- Safety and Trustworthiness -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Safety and Trustworthiness</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('safety_and_trustworthiness'); ?>
            </div>
            <!-- Sprite Safety -->
            <?php include get_theme_file_path('/reuse/sprite-features.php') ?>
            <!-- End Sprite Safety -->
        </div>
        <!-- End Safety and Trustworthiness -->

        <!-- Deposits & Withdrawals -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Deposits & Withdrawals</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('deposit_and_withdrawals'); ?>
            </div>
            <!-- Payment Methods Cards -->
            <?php include "reuse/review/payment-methods.php"; ?>
            <!-- End Payment Methods Cards -->
        </div>
        <!-- End Deposits & Withdrawals -->

        <!-- Mobile App -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Mobile App</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('mobile_app'); ?>
            </div>
            <!-- Horizontal icon cards -->
            <?php icon_list_available('devices','review/icon-list/default.php'); ?>
            <!-- Horizontal icon cards -->
        </div>
        <!-- End Mobile App -->

        <!-- Free Bet Offer -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Free Bet Offer</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('free_bet_offer'); ?>
            </div>
        </div>
        <!-- End Bet Offer -->

        <!-- Betting Markets -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Betting Markets and Odds Quality</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('betting_markets_and_odds_quality'); ?>
            </div>
            <!-- Vertical icon cards -->
            <?php icon_list_visible_customized('sports_markets'); ?>
            <!-- End Vertical icon cards -->
        </div>
        <!-- Betting Markets -->

        <!-- In-Play Betting -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">In-Play Betting</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('in-play_betting'); ?>
            </div>
            <!-- Vertical icon cards -->
            <?php icon_list_visible('special_in_play_features'); ?>
            <!-- End Vertical icon cards -->
        </div>
        <!-- End In-Play Betting -->

        <!-- Support -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Support</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('support'); ?>
            </div>
            <!-- Horizontal icon cards with telephone -->
            <?php include "reuse/review/icon-list/with-telephone.php"; ?>
            <!-- End Vertical icon cards -->
        </div>
        <!-- End Support -->

        <!-- The Final Word -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">The Final Word</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text bottom-50">
                <?php the_field('the_final_word'); ?>
            </div>
        </div>
        <!-- End The Final Word -->

        <!-- Review Rating -->
        <?php review_rating('betting'); ?>
        <!-- End Review Title -->

        <!-- The Author -->
        <div class="row">
            <?php include "reuse/review/author.php" ?>
        </div>
        <!-- End the Author -->

        <!-- Bonus light card -->
        <div class="row">
            <?php include "reuse/review/bonus-light-card.php" ?>
        </div>
        <!-- End Bonus light card -->

        <!-- Comments -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Comments</h2>
                <!-- End Review Title -->
            </div>
            <?php include get_theme_file_path('/reuse/comment.php') ?>
        </div>
        <!-- End Comments -->


    </div>

<?php
get_footer();
