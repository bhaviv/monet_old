<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package playright
 */

/**
 * In some of the template settings we allow the editor to choose a custom priamry menu
 * When the value return is a valid number we render the selected menu.
 * 
 * @ver integer
 */
$custom_menu_id = get_field('custom_menu');

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php wp_head(); ?>

    <?php
    /***
     * When render inline style is enabled the style.css file will be included as inline style
     * Relation to images fonts and priority css rules may be affected if not correctly configured.
     */
    if (!env('RENDER_INLINE_STYLE')):
        $css = file_get_contents(__DIR__ . '/style.css');
        printf('<style>%s</style>',$css);
    endif;
    ?>

</head>

<body <?php body_class(); ?>>

<div id="site-wrapper">
    <div id="site-canvas">
        <div class="container-fluid">
            <header class="row">
                <div class="col-md-3 col-lg-2 hidden-xs">
                    <a href="<?php echo get_option('home'); ?>" title="<?php bloginfo( 'name' ); ?>" class="logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/logo-play-right-black.png" />
                    </a>
                </div>
                <div class="col-md-9 col-lg-10">
                    <a href="#" class="toggle-nav">
                        <div class="hamburger hidden-sm hidden-md hidden-lg"></div>
                    </a>
                    <a href="<?php echo get_option('site_url'); ?>" title="<?php bloginfo( 'name' ); ?>"
                    class="logo-mobile hidden-sm hidden-md hidden-lg">
                    <img src="<?php echo get_template_directory_uri(); ?>/front/img/PR-logo-Mobile7.png">
                    </a>
                    <?php
                        $menu = ['menu_id' => 'primary-menu'];

                        if ($custom_menu_id){
                            // Custom menu was detected, use it
                            $menu['menu'] = $custom_menu_id;
                        }else{
                            // No custom menu detected, use default
                            $menu['theme_location'] = 'header';
                        }

                        // Render the menu
                        wp_nav_menu($menu);
                    ?>
                    <div class="breadcrumbs">
                        <?php echo breadcrumbs(); ?>
                    </div>
                </div>
            </header>
        </div>

