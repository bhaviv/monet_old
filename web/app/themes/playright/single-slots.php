<?php
/**
 * Template Name: Slots Review Template
 */

/**
 * For some template we allow the editor to choose a custom background image.
 * The value will contain the full url of the image and will be used to display the image.
 *
 * @ver string
 */
get_header();

 $slider_images = get_field('slider_images');
 
 if (!$slider_images){
     $slider_images = [];
 }


if ( have_posts() ) while ( have_posts() )  the_post();
?>
    <div class="container review-slots">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-sm-pull-left col-md-push-8 col-xs-12">
                <div class="row hidden-lg hidden-md hidden-sm">
                    <div class="col-xs-12">
                        <!--Main Title-->
                        <h1 class="text-center"><?php the_title() ?></h1>
                        <!--End Main Title-->
                        <!--Slots slider mobile-->
                        <?php include "reuse/slider-multiitem-mobile.php" ?>
                        <!-- End Slots slider mobile -->
                        <!--Main Title-->
                        <h2 class="text-left text-uppercase"><?php the_title() ?></h2>
                        <!--End Main Title-->
                        <!--Slots slider mobile-->
                        <?php include "reuse/dynamiclists/mobile-brand-list.php" ?>
                        <!-- End Slots slider mobile -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <!-- Review Title on a right side-->
                        <h3 class="review-title-slots text-uppercase text-left">General information</h3>
                        <!-- End Review Title on a right side-->

                        <!--Side features list -->
                        <?php

                        $device_support = get_field('devices');
                        if (!$device_support){
                            $device_support  = [];
                        }


                        echo make_slots_list([
                            'Launched' => get_field('year_launched'),
                            'Developer' => get_field('developer'),
                            'Slot Type' => get_field('type'),
                            'Progressive' => get_field('progressive'),
                            'Volatility' => get_field('volatility'),
                            'Mobile' => in_array('Mobile',$device_support)]);
                        ?>
                        <!-- End Side features list -->
                    </div>

                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <!-- Review Title on a right side-->
                        <h3 class="review-title-slots text-uppercase text-left">Features</h3>
                        <!-- End Review Title on a right side-->
                        <!--Side features list -->
                        <?php
                            $other_features = get_field('other_features');

                            echo make_slots_list([
                                'Reels' => get_field('reels'),
                                'Paylines' => get_field('pay_lines'),
                                'Autoplay' => in_array('Autoplay',$other_features),
                                'Multipliers' => in_array('Multipliers',$other_features),
                                'Scatters' => in_array('Scatters',$other_features),
                                'Wilds' => in_array('Wilds',$other_features)
                            ]);
                        ?>
                        <!-- End Side features list -->
                    </div>

                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <!-- Review Title on a right side-->
                        <h3 class="review-title-slots text-uppercase text-left">Stakes and payouts</h3>
                        <!-- End Review Title on a right side-->
                        <!--Side features list -->
                        
                        <?php 
                            echo make_slots_list([
                                'Bet stakes' => sprintf('%s - %s',get_field('min_bet_size'),get_field('max_bet_size')),
                                'Payout' => get_field('payout'),
                                'Jackpot' => get_field('jackpot')
                            ]);
                        ?>

                        <!-- End Side features list -->
                    </div>

                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <!-- Review Title on a right side-->
                        <h3 class="review-title-slots text-uppercase text-left">Rating</h3>
                        <!-- End Review Title on a right side-->
                        <!-- Rating -->
                        <?php review_rating('slots'); ?>
                        <!-- End Rating -->
                    </div>
                </div>
                <div class="row hidden-xs hidden-sm">
                    <div class="col-md-12">
                        <!-- Review Title on a right side-->
                        <h3 class="review-title-slots more-slots-reviews text-left">More Slots Review</h3>
                        <!-- End Review Title on a right side-->
                        <!--Side Reviews List-->
                        <?php include "reuse/side-reviews-list.php" ?>
                        <!-- End Side Reviews List -->
                    </div>
                </div>

            </div>
            <div class="col-md-8 col-md-pull-4 col-sm-12 col-xs-12">
                <div class="row hidden-xs">
                    <div class="col-md-12">
                        <!--Main Title-->
                        <h1 class="text-center"><?php the_title() ?></h1>
                        <!--End Main Title-->
                    </div>
                    <div class="col-md-12">
                        <!--Slots slider-->
                        <?php include "reuse/slider-multiitem.php" ?>
                        <!-- End Slots slider -->
                    </div>
                    <div class="col-md-12">
                        <!--Slots DL slider-->
                        <?php include "reuse/dynamiclists/slider-dl.php" ?>
                        <!-- End Slots DL slider -->
                    </div>
                </div>

                <?php

                $text_sections = [
                    'Overview' => function(){ return get_the_content(); },
                    'Gameplay and Features' => 'gameplay_and_features',
                    'Jackpot' => 'jackpots',
                    'Graphics and Sound' => 'graphic_and_sound',
                    'The Final Word' => 'the_final_word'
                ];
                
                foreach($text_sections as $title => $content):
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Review Title -->
                        <h2 class="review-title text-left"><?php echo $title; ?></h2>
                        <!-- End Review Title -->
                    </div>
                    <div class="col-md-12 review-text">
                        <?php 
                            if (is_callable($content)){
                                print($content());
                            }else{
                                print(get_field($content));
                            }
                        ?>
                    </div>
                </div>

                <?php endforeach; ?>

                <!-- The Author -->
                <div class="row">
                    <?php include "reuse/review/author.php" ?>
                </div>
                <!-- End the Author -->

                <!-- Comments -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Review Title -->
                        <h2 class="review-title text-left">Comments</h2>
                        <!-- End Review Title -->
                    </div>
                    <?php include "reuse/comment.php" ?>
                </div>
                <!-- End Comments -->
            </div>
        </div>









    </div>

<?php
get_footer();
