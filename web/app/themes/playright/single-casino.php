<?php
/**
 * Template Name: Casino Review Template
 */

/**
 * For some template we allow the editor to choose a custom background image.
 * The value will contain the full url of the image and will be used to display the image.
 *
 * @ver string
 */
get_header();
if ( have_posts() ) while ( have_posts() )  the_post();
?>
    <div class="container">
        <!-- Arrows -->
        <?php include "reuse/review/arrows.php" ?>
        <!-- End Arrows -->

        <!-- Review Coupon -->
        <?php include "reuse/review/coupon.php"; ?>
        <!-- End Review Coupon -->

        <!-- Review Title -->
        <div class="row">
            <div class="col-md-12">
                <h2 class="review-title">Rating</h2>
            </div>
        </div>
        <!-- End Review Title -->

        <!-- Review Rating -->
        <?php review_rating('casino'); ?>
        <!-- End Review Title -->


        <!-- Overview -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Overview</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_content(); ?>
            </div>
        </div>
        <!-- End Overview -->

        <!-- Safety and Trustworthiness -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Safety and Trustworthiness</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('safety_and_trustworthiness'); ?>
            </div>
            <!-- Sprite Safety -->
            <?php include get_theme_file_path('/reuse/sprite-features.php') ?>
            <!-- End Sprite Safety -->
        </div>
        <!-- End Safety and Trustworthiness -->

        <!-- Deposits & Withdrawals -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Deposits & Withdrawals</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('deposit_and_withdrawals'); ?>
            </div>
            <!-- Payment Methods Cards -->
            <?php include "reuse/review/payment-methods.php"; ?>
            <!-- End Payment Methods Cards -->
        </div>
        <!-- End Deposits & Withdrawals -->

        <!-- Welcome bonus -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Welcome bonus</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('welcome_bonus'); ?>
            </div>
        </div>
        <!-- End Welcome bonus -->

        <!-- Game Selection -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Game Selection</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('game_selection'); ?>
            </div>
            <!-- Vertical icon cards -->
            <?php icon_list_customized(['slots', 'progressive_slots', 'roulette', 'blackjack', 'baccarat/punto_banco', 'craps'
                , 'casino_hold_‘em', 'video_poker', 'live_dealer', 'arcade_games'],'review/icon-list/vertical-numeric.php'); ?>
            <!-- End Vertical icon cards -->
        </div>
        <!-- End Game Selection -->

        <!-- Mobile App -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Mobile App</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('mobile_app'); ?>
            </div>
            <h3 class="text-uppercase text-center sub-title"> Platforms available</h3>
            <!-- Horizontal icon cards -->
            <?php icon_list_available('devices','review/icon-list/default.php'); ?>
            <!-- Horizontal icon cards -->
        </div>
        <!-- End Mobile App -->

        <!-- Promotions -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Promotions</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('promotions'); ?>
            </div>
        </div>
        <!-- End Promotions -->

        <!-- Support -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Support</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('support'); ?>
            </div>
            <!-- Horizontal icon cards with telephone -->
            <?php include "reuse/review/icon-list/with-telephone.php"; ?>
            <!-- End Vertical icon cards -->
        </div>
        <!-- End Support -->

        <!-- The Final Word -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">The Final Word</h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text bottom-50">
                <?php the_field('the_final_word'); ?>
            </div>
        </div>
        <!-- End The Final Word -->

        <!-- Review Rating -->
        <?php review_rating('casino'); ?>
        <!-- End Review Title -->

        <!-- Bonus light card -->
        <div class="row">
            <?php include "reuse/review/bonus-light-card.php" ?>
        </div>
        <!-- End Bonus light card -->

        <!-- The Author -->
        <div class="row">
            <?php include "reuse/review/author.php" ?>
        </div>
        <!-- End the Author -->

        <!-- Comments -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Comments</h2>
                <!-- End Review Title -->
            </div>
            <?php include get_theme_file_path('/reuse/comment.php') ?>
        </div>
        <!-- End Comments -->
    </div>

<?php
get_footer();
