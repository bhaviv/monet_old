<?php
/**
 * Template Name: Events list template
 */

get_header(); ?>
    <!-- Ticker  -->
    <div class="container-fluid">
        <div class="row">
            <?php include "reuse/ticker.php" ?>
        </div>

    </div>
    <!--End Ticker  -->

    <!--Main content -->
    <div class="container">
        <!--  Title-->
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="text-capitalize text-center"><?php the_field('h1_title'); ?></h1>
                <?php the_content(); ?>

            </div>
            <div class="col-md-10 col-md-offset-1 col-sm-12 text-center">

            </div>
        </div>
        <!--End Title-->

        <div class="row events-list">
            <div class="col-sm-12 col-md-6">
                <div class="post-container">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <a href="#">
                                <div class="post-thumbnail" style="background-image:url( https://playright.co.uk/top-assets/sites/5/images/Sport-logos/Events/image-3-sport-events.jpg );"> </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-xs-12 post-excerpt">
                            <div class="post-headers">
                                <a href="#">
                                    <h2>Cheltenham</h2>
                                </a>
                                <a href="#">
                                    <p>The Cheltenham Festival is one of the leading National Hunt horse racing events in the UK, and that means there's some serious value out there for punters of all persuasions.</p>
                                </a>
                            </div>
                            <div class="details">
                               <a class="read-more">Read more</a>
                                <a class="arrow-right text-right" href="#"> <i class="icon-arrow-left"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="post-container">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <a href="#">
                                <div class="post-thumbnail" style="background-image:url( https://playright.co.uk/top-assets/sites/5/images/Sport-logos/Events/image-3-sport-events.jpg );"> </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-xs-12 post-excerpt">
                            <div class="post-headers">
                                <a href="#">
                                    <h2>Cheltenham</h2>
                                </a>
                                <a href="#">
                                    <p>The Cheltenham Festival is one of the leading National Hunt horse racing events in the UK, and that means there's some serious value out there for punters of all persuasions.</p>
                                </a>
                            </div>
                            <div class="details">
                                <a class="read-more">Read more</a>
                                <a class="arrow-right text-right" href="#"> <i class="icon-arrow-left"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Main content -->
<?php
get_footer();
