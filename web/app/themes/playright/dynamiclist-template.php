<?php
/**
 * Template Name: Dynamic List Template Betting
 */

get_header();
if ( have_posts() ) while ( have_posts() )  the_post();
?>

<div class="background-wrapper" style="background:url(<?php the_field('background_image',false); ?>) fixed no-repeat;background-size: cover;">
    <!-- Ticker (ADD TICKER) -->
    <div class="container-fluid">
        <div class="row">
        </div>

    </div>
    <!--End Ticker  -->
    <!-- Desktop Header Section -->
    <div class="container hidden-xs">
        <div class="row main-title-with-icon hidden-xs">
            <div class="col-md-12">
                <div class="title-sport pull-right">
                    <h1 class="text-uppercase"><?php the_field('h1_title'); ?></h1>
                    <?php the_content(); ?>
                </div>
                <img class="pull-left" src="<?php the_field('main_title_icon'); ?>" />
            </div>
        </div>
    </div>
    <!-- End Of Desktop Header Section -->

    <!-- Mobile Header Section -->
    <div class="container-fluid hidden-sm hidden-md hidden-lg">
        <div class="row title-background-mobile" style="background:url(<?php the_field('mobile_background_image',false); ?>) no-repeat; background-size: cover">
            <div class="col-xs-12">
                <?php the_field('mobile_header_content'); ?>
            </div>
        </div>
    </div>
    <!-- End Of Desktop Header Section -->

    <!-- Start filter row -->
    <div class="container hidden-xs">
        <div class="row">
            <div class="col-md-12">
                <?php include "reuse/dl-filter.php" ?>
                <?php include "reuse/disclosure.php" ?>
                <?php include "reuse/certificates.php" ?>
            </div>
            <div class="col-md-12">
                <div class="selected-filters">
                    
                </div>
            </div>
        </div>

    </div>
    <!-- End filter row -->

    <!-- Start Disclosure mobile -->
    <div class="container hidden-sm hidden-md hidden-lg">
        <a class="disclosure-mobile" href="#disclaimer">Advertiser disclosure</a>
    </div>
    <!-- End Disclosure mobile -->

    <!-- Primary Dynamic List should be printed here -->
    <div class="container">
        <?php include "reuse/dynamiclists/betting.php" ?>
    </div>
    <!-- End Primary Dynamic List -->
</div>

<!-- Recommended casino Sites Dynamic list -->
<div class="container text-center recommended-casino">
    <h2>Our Recommended Casino Sites</h2>
    <div class="row">
        <?php include "reuse/dynamiclists/crossbanner.php" ?>
        <div class="col-md-12">
            <a class="default-button see-casino"
               role="link"
               href="<?php echo get_theme_mod('cross_banner_see_more_link'); ?>">SEE TOP-RATED CASINOS</a>
        </div>
    </div>
</div><!-- End of Recommendation Section -->



<!-- Slider -->
<div class="container hidden-sm hidden-xs">
    <div class="row">
        <div class="col-md-12 slider">
            <?php include "reuse/dynamiclists/slider.php" ?>
        </div>
    </div>
</div>
<!-- End of Slider -->

<div class="container">
    <h2><strong><?php the_field('article_title'); ?></strong></h2>
    <?php the_field('article_content'); ?>
</div>
<!-- End of Article Section -->

<!-- Join Our Mail List Form -->
<?php include "reuse/newsletter-form.php" ?>

<?php
get_footer();