<?php
/**
 * Template Name: Reviews template
 */

get_header();
if ( have_posts() ) while ( have_posts() )  the_post();
?>
<div class="container reviews">
    <!--  Reviews Title-->
    <div class="row">
        <div class="col-md-12 text-center">
            <h1 class="text-capitalize text-center"><?php the_field('h1_title'); ?></h1>
            <?php the_content(); ?>
        </div>
    </div>
    <!--End  Reviews Title-->

    <div class="row">
        <div class="col-md-12">
            <?php include "reuse/dynamiclists/reviews-list.php" ?>
        </div>
    </div>
</div>

<?php
get_footer();