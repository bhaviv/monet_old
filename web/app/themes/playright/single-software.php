<?php
/**
 * Template Name: Single Software Template
 */

/**
 * For some template we allow the editor to choose a custom background image.
 * The value will contain the full url of the image and will be used to display the image.
 *
 * @ver string
 */
get_header();
if ( have_posts() ) while ( have_posts() )  the_post();
?>
    <div class="container review-software">
        <!--  Title-->
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-capitalize text-center"><?php the_field('h1_title'); ?></h1>
                <?php the_content(); ?>
            </div>
        </div>
        <!--End Title-->

        <div class="row">
            <!--Review coupon-->
            <?php include "reuse/review/review-coupon-software.php" ?>
            <!--End Review coupon-->
        </div>

        <div class="row">
            <!--Slots slider-->
            <div class="col-md-12 slider">
                <?php include "reuse/dynamiclists/slider-dl.php" ?>
            </div>
            <!-- End Slots slider -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Video</h2>
                <!-- End Review Title -->

                <!-- Review Video -->
                <div class="review-video-container">
                    <iframe class="video center-block" width="" height="" role="application" src="<?php the_field('video'); ?>" frameborder="0" allowfullscreen=""></iframe>
                </div>
                <!-- End Review Video -->
            </div>
        </div>

        <!-- Guide to Microgaming Software -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title"><?php the_field('article_title'); ?></h2>
                <!-- End Review Title -->
            </div>
            <div class="col-md-12 review-text">
                <?php the_field('article_content'); ?>
            </div>
        </div>
        <!-- End Guide to Microgaming Software -->

        <!-- The Author -->
        <div class="row">
            <?php include "reuse/review/author.php" ?>
        </div>
        <!-- End the Author -->

        <!-- Comments -->
        <div class="row">
            <div class="col-md-12">
                <!-- Review Title -->
                <h2 class="review-title">Comments</h2>
                <!-- End Review Title -->
            </div>
            <?php include "reuse/comment.php" ?>
        </div>
        <!-- End Comments -->


    </div>

<?php
get_footer();