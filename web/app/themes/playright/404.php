<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package playright
 */

/**
 * Template Name: 404
 */

get_header(); ?>
<div class="container page404">
    <div class="row wrapper">
        <div class="col-sm-6 col-xs-7">
            <p class="whoops">WHOOPS</p>
            <p><b>We couldn't find the page you are looking for</b></p>
            <button class="text-uppercase">GO Back<i class="icon-arrow-left"></i></button>
        </div>
        <div class="col-sm-6 col-xs-7">
            <img src="http://s3-eu-west-1.amazonaws.com/prwp/app/uploads/20180104122234/404.png"  class="img-responsive">
        </div>
    </div>

    <div class="row title-with-line">
        <div class="col-md-12">
            <h2>PlayRight Recommends</h2>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
            <p class="subtitle text-center">Check out our current favorite casino, betting and bingo sites</p>
        </div>
    </div>
</div>
    <!-- DYNAMIC LIST SLIDER -->
        <?php include "reuse/dynamiclists/dynamic-list-hp.php" ?>
    <!-- END OF DYNAMIC LIST SLIDER -->
<?php
get_footer();
