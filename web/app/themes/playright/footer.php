<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package playright
 */

?>
        <div class="container-fluid">
            <footer class="row">
                <div class="container footer-menu-contact">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="col">
                                <h4><?php echo esc_attr(get_theme_mod('footer_links_title')); ?></h4>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <?php
                                wp_nav_menu([
                                    'theme_location' => 'footer',
                                    'menu_id'        => 'primary-menu',
                                ]);
                                ?>
                            </div>
                            <div class="col-sm-6 col-md-6"></div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h4><?php echo esc_attr(get_theme_mod('footer_contact_title')); ?></h4>
                            <p><?php echo esc_attr(get_theme_mod('footer_contact_description')); ?></p>

                            <form method="post" action="">
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" name="name" placeholder="Name" required>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <textarea name="message" class="form-control" placeholder="Your Message" required></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 button">
                                        <button type="submit" class="default-button">Post</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="container-fluid footer-background">
                    <div class="container">
                        <div class="row footer-regulation">
                            <div class="col-md-2 logo-footer">
                                <a href="<?php echo get_option('home'); ?>" title="<?php bloginfo( 'name' ); ?>">
                                    <img src="<?php echo get_template_directory_uri(); ?>/front/img/logo-footer-play-right.png" class="logo-footer">
                                </a>
                            </div>
                            <div class="col-md-10 disclaimer">
                            <span id="disclaimer">
                                <?php echo get_theme_mod('footer_disclaimer'); ?>
                            </span>
                            </div>
                        </div>
                        <div class="row footer-additional">
                            <div class="col-md-8">
                                <p><?php echo esc_attr(get_theme_mod('footer_additional_disclaimer')); ?></p>
                            </div>
                            <div class="col-md-4">
                                <a><img src="<?php echo get_template_directory_uri(); ?>/front/img/ecogra.png"></a>
                                <a><img src="<?php echo get_template_directory_uri(); ?>/front/img/gamecare.png"></a>
                            </div>
                            <div class="col-xs-12 text-center">All rights reserved © <?php echo date('Y'); ?> Playright.co.uk</div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<div class="smooth_scroller" style="position:fixed;right:25px;bottom:25px;cursor:pointer;">
    <img src="<?php echo get_template_directory_uri(); ?>/front/img/FloatingBackToTop.png" />
</div>
<?php wp_footer(); ?>
</body>
</html>
