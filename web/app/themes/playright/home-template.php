<?php

/**
 * Template Name: Home Page
 */

get_header();
?>
<div class="container-fluid main-content-wrapper">
    <div class="container title-with-line">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_field('first_section_title') ?></h2>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <p class="subtitle text-center"><?php the_field('first_section_subtitle') ?></p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row video-text">
            <div class="col-md-6  col-sm-12 col-xs-12 col-md-offset-1">
                <?php the_field('first_section_content') ?>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 nopadding">
                <div class="video-container">
                    <iframe src="<?php the_field('first_section_youtube_url') ?>" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>


<div  class="container">
    <div class="row brand-of-the-month">
        <?php
        $brand_review_left = get_field('brand_review_left');
        ?>
        <div class="col-md-6 col-xs-12 border space-from-right ">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-wrapper">
                        <img src="<?php echo get_the_post_thumbnail_url($brand_review_left->ID); ?>" />
                        <div class="text">
                            <p><?php echo post_type_to_singular($brand_review_left->post_type); ?> of the Month</p>
                            <p><?php echo esc_attr($brand_review_left->post_title); ?></p>
                        </div>
                    </div>
                    <p class="post-paragraph"><?php echo $brand_review_left->post_excerpt; ?></p>
                    <div class="tablet-link">
                        <a class="default-button text-center"
                        title="<?php echo esc_attr($brand_review_left->post_title); ?>"
                        href="<?php echo get_permalink($brand_review_left->ID); ?>"
                        target=“_blank”>Read <?php echo esc_attr($brand_review_left->post_title); ?> Review
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $brand_review_right = get_field('brand_review_right');
        ?>
        <div class="col-md-6 col-xs-12 space-from-left">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-wrapper">
                        <img src="<?php echo get_the_post_thumbnail_url($brand_review_right->ID); ?>" />
                        <div class="text">
                            <p><?php echo post_type_to_singular($brand_review_right->post_type); ?> of the Month</p>
                            <p><?php echo esc_attr($brand_review_right->post_title); ?></p>
                        </div>
                    </div>
                    <p class="post-paragraph"><?php echo $brand_review_right->post_excerpt; ?></p>
                    <div class="tablet-link">
                        <a class="default-button text-center"
                        title="<?php echo esc_attr($brand_review_right->post_title); ?>"
                        href="<?php echo get_permalink($brand_review_right->ID); ?>"
                        target=“_blank”>Read <?php echo esc_attr($brand_review_right->post_title); ?> Review
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
 
    <!-- SECOND SECTION -->
    <div class="container title-with-line">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_field('second_section_title') ?></h2>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <p class="subtitle text-center"><?php the_field('second_section_subtitle') ?></p>
            </div>
        </div>
    </div>
    <!-- END OF SECOND SECTION -->

    <!-- DYNAMIC LIST SLIDER -->
        <?php include "reuse/dynamiclists/dynamic-list-hp.php" ?>
    <!-- END OF DYNAMIC LIST SLIDER -->

    <!-- THIRD SECTION -->
    <div class="container title-with-line">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_field('third_section_title') ?></h2>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <p class="subtitle text-center"><?php the_field('third_section_subtitle') ?></p>
            </div>
        </div>
    </div>
    <!-- THIRD SECTION -->


    
    <div class="container compare-hp">
        <div class="row">
            <div class="col-lg-4 col-md-7 col-sm-6 col-xs-12 item">
                <div class="sprite-hp star center-block"></div>
                <h3 class="text-center">How We Rate Gambling Sites</h3>
                <p class="text-center">We showing to much staff , like a safety and security , software , game selection and much more</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp shield center-block"></div>
                <h3 class="text-center">Safety & Security</h3>
                <p class="text-center">Does the site take care of its responsibilities to customers?</p>
            </div>
            <div class="col-lg-4 col-md-7 col-sm-6 col-xs-12 item">
                <div class="sprite-hp deposits center-block"></div>
                <h3 class="text-center">Deposits & Withdrawals</h3>
                <p class="text-center">How quick and easy it to get your money on and off the site?</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp game center-block"></div>
                <h3 class="text-center">Game Selection</h3>
                <p class="text-center">Whether casino, bingo or betting – we want lots of options.</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp software center-block"></div>
                <h3 class="text-center">Software Quality</h3>
                <p class="text-center">Is the platform stable? How easy is it to find what you’re looking for?</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp devices center-block"></div>
                <h3 class="text-center">Device Compatibility</h3>
                <p class="text-center">Can you get the same experience on your phone or tablet as on your laptop?</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp bonuses center-block"></div>
                <h3 class="text-center">Bonuses & Promotions</h3>
                <p class="text-center">Is there a good welcome bonus? How easy is it to meet the terms and conditions?</p>
            </div>
        </div>
    </div>

    <!-- Join Our Mail List Form -->
    <?php include "reuse/newsletter-form.php" ?>

    <div class="container hidden-xs footer-images-line">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/visa.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/mastercard.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/paypal.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/american-express.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/skrill.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/western-union.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();