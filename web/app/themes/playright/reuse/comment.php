<div class="col-md-12">
    <div class="comment-box">
        <form class="form comment-box-form" action="">
            <input type="hidden" name="type" value="like_dislike">
            <input type="hidden" name="parent_id">
            <div class="comment-box-stars-rating"></div>
                <div class="col-md-12">
                    <div class="row rating-section">
                        <p class="pull-left">Rate and Write a Review</p>
                        <div class="comment-box-rating tp-rating rating pull-right">
                            <input type="hidden" name="rating" value="0">
                            <li class="tp-rating__item active"><i class="stars"></i></li>
                            <li class="tp-rating__item active"><i class="stars"></i></li>
                            <li class="tp-rating__item active" ><i class="stars"></i></li>
                            <li class="tp-rating__item active"><i class="stars"></i></li>
                            <li class="tp-rating__item"><i class="stars"></i></li>
                            <li class="tp-rating__item"><i class="stars"></i></li>
                            <li class="tp-rating__item"><i class="stars"></i></li>
                            <li class="tp-rating__item"><i class="stars"></i></li>
                            <li class="tp-rating__item"><i class="stars"></i></li>
                            <li class="tp-rating__item"><i class="stars"></i></li>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                        <input type="text" id="form-pros-cons" class="form-control" name="author" placeholder="Name"
                               type="text" required>
                        <div class="text-error comment-author-error"></div>
                    </div>

                    <div class="col-md-3">
                        <input type="text" class="form-control" name="email" type="email" placeholder="E-mail" required>
                        <div class="text-error comment-email-error"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="prompt-like row">
                            <div class="col-md-12 like-dislike">
                                <textarea class="form-control overtextable like-dislike-group" name="like"
                                          id="like"></textarea>
                                <div class="review-button plus"></div>
                                <span class="review-question"><strong>What did you like? </strong></br> Click and comment</span>
                                <div class="text-error comment-like-error"></div>
                            </div>
                        </div>

                        <div class="prompt-dislike row">
                            <div class="col-md-12 like-dislike">
                                <textarea class="form-control overtextable like-dislike-group" name="dislike"
                                          id="dislike"></textarea>
                                <div class="review-button minus"></div>
                                <span class="review-question"><strong>What did you dislike?</strong></br> Click and comment</span>
                                <div class="text-error comment-dislike-error"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <p class="hidden char-count"><span>50</span> characters left
                        <p>
                    </div>
                    <div class="col-md-8 checkbox">
                        <input class="checkbox-custom" type="checkbox" value="1" role="button" aria-pressed="false"
                               name="declare">
                        <label for="declare">
                            I declare that my review is based on my own experience and represents my genuine opinion of
                            this Betting site. I’m not a sports book employee
                            and have not been offered any incentive or payment by the betting site to write this review. I
                            understand that Playright has zero-tolerance
                            policy on fake reviews.
                        </label>
                        <div class="text-error comment-declare-error"></div>
                    </div>
                    <div class="col-md-8">
                        <button type="submit" class="default-button pull-left" role="link">Post</button>
                    </div>
                </div>

                <div class="comment-box-message"></div>
            </form>
        </div>


        <script type="text/template" class="comment-box-successful">
            <div>
                <h2 class="thanks-success">Thank you for your review</h2>
            </div>
        </script>

        <script type="text/template" class="comment-box-fail">
            <div>
                <h2 class="thanks-fail">There was problem with one of your fields please try again</h2>
            </div>
        </script>
    </div> <!-- end comment-pros-con -->