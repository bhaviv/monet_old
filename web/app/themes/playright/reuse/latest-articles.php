<div class="latest-articles">
    <?php
        $use_latest_articles = get_field('use_latest_articles');
        $current_post_id = get_the_ID();
    ?>
    <?php
        if ($use_latest_articles):
            // Get last articles from database.
            
            $academy_posts = new WP_Query([
                'post_type' => 'academy',
                'posts_per_page' => 5
            ]);
            
            if ($academy_posts->have_posts()){
                while ($academy_posts->have_posts()) {
                    $academy_posts->the_post();
                    if ($current_post_id != get_the_ID()):
                    ?>
                    <div class="read-more-wrapper">
                        <div class="featured-image"
                            style="background-image:url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>)"></div>
                        <div class="inner-content-wrapper">
                            <h4><a target="_blank" href="<?php echo get_permalink(get_the_ID()); ?>"><?php the_title(); ?></a></h4>
                            <span class="post-date"><?php the_date(); ?></span>
                            <p><?php the_excerpt(); ?></p>
                            <a href="<?php echo get_permalink(get_the_ID()); ?>" class="post-more-info" role="button" aria-expanded="false">Read More</a>
                        </div>
                    </div>
                    <?php
                    endif;
                }
            }else{
                // whoops no posts...
            }

        else:
            $articles = get_field('related_posts');

            if ($articles && is_array($articles)):
                foreach($articles as $article):
                $article = $article['post'];
                ?>
                <div class="read-more-wrapper">
                    <div class="featured-image"
                        style="background-image:url(<?php echo get_the_post_thumbnail_url($article->ID); ?>)"></div>
                    <div class="inner-content-wrapper">
                        <h4><a target="_blank" href="<?php echo get_permalink($article->ID); ?>"><?php echo $article->post_title; ?></a></h4>
                        <span class="post-date"><?php echo $article->post_date; ?></span>
                        <p><?php echo $article->post_excerpt; ?></p>
                        <a href="<?php echo get_permalink($article->ID); ?>" class="post-more-info" role="button" aria-expanded="false">Read More</a>
                    </div>
                </div>
                <?php
            endforeach;
            endif;
        endif;
        ?>
</div>
