<div class="dl-filter">
    <div class="sort-filter">
        <div class="open-filter dropdown-toggle">
            <span class="title text-uppercase">Filter</span>
            <span class="arrow">
                        <span class="caret"></span>
                    </span>
            <span class="sr-only">Toggle</span>

            <div class="dropdown-wrapper" id="filter">
                <div class="dropdown dropdown-filter">
                    <ul class="filters">
                        
                    </ul>
                </div>
            </div>
        </div>
        <div class="open-sort dropdown-toggle" id="sorter">
            <span class="title text-uppercase">Sort By</span>
            <span class="arrow">
                        <span class="caret"></span>
                    </span>
            <span class="sr-only">Toggle</span>

            <div class="dropdown-wrapper">
                <div class="dropdown dropdown-filter">
                    <ul class="sorters">
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>