<div class="review-coupon-software">
    <div class="col-sm-12 col-lg-4 col-lg-offset-0 col-md-6">
        <img class="img-responsive" src="<?php the_field('logo'); ?>" alt="logo">
    </div>
    <div class="col-sm-8 col-sm-offset-2 col-lg-8 col-lg-offset-0 col-md-6 col-md-offset-0">
        <div class="content-wrapper">
            <h3 class="title-info text-left text-uppercase">GENERAL INFORMATION</h3>
            <div class="row info-fields">
                <div class="col-lg-4 col-sm-6 col-xs-6 info-field text-uppercase">
                    <b>License:</b> <span><?php echo get_field('license') ? 'Yes' : 'No'; ?></span>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-6 info-field text-uppercase">
                    <b>Country:</b> <span><?php the_field('country'); ?></span>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-6 info-field text-uppercase">
                    <b>Established:</b> <span><?php the_field('established'); ?></span>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-6 info-field text-uppercase">
                    <b>Casinos:</b> <span><?php the_field('casinos'); ?></span>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-6 info-field text-uppercase">
                    <b>Games:</b> <span><?php the_field('games'); ?></span>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-6 info-field text-uppercase">
                    <b>Rating:</b> <span><?php the_field('rating'); ?></span>
                </div>
            </div>
        </div>
    </div>
</div>