<div class="col-lg-10 col-lg-offset-1 col-xs-12 review-payment-methods">
    <div class="row text-center">
        
            <?php

            $deposits_and_withdrawals = [
                'deposit' => [],
                'withdrawal' => [],
                'all' => []
            ];
            $deposit_methods = get_field('deposit_methods');

            if (!is_array($deposit_methods)){
                $deposit_methods = [];
            };

            foreach($deposit_methods as $deposit_method){
                $deposits_and_withdrawals['all'][$deposit_method] = true;
                $deposits_and_withdrawals['deposit'][$deposit_method] = true;
            }

            foreach($withdrawal_methods as $withdrawal_method){
                $deposits_and_withdrawals['all'][$withdrawal_method] = true;
                $deposits_and_withdrawals['withdrawal'][$withdrawal_method] = true;
            }


            if (isset($deposits_and_withdrawals['all'])):
                foreach($deposits_and_withdrawals['all'] as $payment_method => $bool):
                    $deposit = isset($deposits_and_withdrawals['deposit'][$payment_method]) ? 'icon-checkmark' : 'icon-cancel---close';
                    $withdrawal = isset($deposits_and_withdrawals['withdrawal'][$payment_method]) ? 'icon-checkmark' : 'icon-cancel---close';
                endforeach;
                
                foreach($deposit_methods as $deposit_method => $value):
            ?>

                <div class="col-md-3 col-sm-4 col-xs-10 offset-col-xs-1 col-center">
                    <div class="payment-method text-center">
                        <div class="row">
                            <div class="col-xs-6 border-right">
                                <div class="options text-uppercase">Deposit</div>
                            </div>
                            <div class="col-xs-6">
                                <div class="options text-uppercase">Withdraw</div>
                            </div>
                            <div class="col-xs-12 ">
                                <div class="card">
                                    <div class="col-xs-6 status">
                                        <i class="<?php echo $deposit; ?> abs-centered"></i>
                                    </div>
                                    <div class="col-xs-6 status">
                                        <i class="<?php echo $withdrawal; ?> abs-centered"></i>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 payment-methods">
                                            <div class="sprite <?php echo sanitize_title($value); ?>"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        <?php
                endforeach;
            endif;
        ?>
        
    </div>
</div>