<?php

$deposit_methods = get_field('deposit_methods');
$withdrawal_methods = get_field('withdrawal_methods');

if (!is_array($deposit_methods)){
    $deposit_methods = [];
}

if (!is_array($withdrawal_methods)){
    $withdrawal_methods = [];
}

$paypal_accepted = array_filter($deposit_methods, function ($deposit_method){
    return $deposit_method == 'PayPal';
});

$software_providers = get_field('software_providers');
if (!is_array($software_providers)){
    $software_providers = [];
}

?>
<div class="review-coupon">
    <div class="row wrapper-white">
        <div class="col-md-12">
            <h1><?php the_title() ?> Review</h1>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row statistic">
                <div class="col-md-3 col-sm-12">
                    <a href="#">
                        <img src="<?php the_field('main_logo'); ?>" alt="brand logo" class="img-responsive"/>
                    </a>
                </div>
                <div class="col-md-5 col-sm-6">
                    <ul>
                        <li>Parent Company: <b class="text-uppercase"><?php the_field('parent_company'); ?></b>
                        </li>
                        <li>Customer Payout: <b class="text-uppercase">N/A</b>
                        </li>
                        <?php $reviewer = get_field('reviewer'); ?>
                        <li>Reviewer: <b class="text-uppercase"><?php printf('%s %s',$reviewer['user_firstname'],$reviewer['user_lastname']); ?></b>
                        </li>
                        <li>Date of Review: <b class="text-uppercase"><?php the_date('d F Y'); ?></b>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6">
                    <ul>
                        <li> <span class="vip">VIP Scheme: </span> <b><?php echo get_field('vip_scheme') ? 'Yes' : 'No'; ?></b>
                        </li>
                        <li>Launched: <b><?php the_field('year_launched'); ?></b>
                        </li>
                        <li>PayPal Accepted: <b> <?php echo $paypal_accepted ? 'Yes' : 'No'; ?> </b>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- This widget is different for casino and betting. Please show it only if needed. And please add sprite classes -->
        <div class="col-sm-10 col-sm-offset-1 hidden-sm hidden-xs wrapper-features">
            <ul class="software-providers">
                <?php
                foreach($software_providers as $software_provider => $value):
                ?>
                <li class="sprite <?php echo sanitize_title($value); ?>"></li>
                    <?php
                endforeach;
                ?>
            </ul>
        </div>

    </div>
    <div class="row bottom-section">
        <div class="col-md-10 col-md-offset-1">
            <div class="col-md-9 col-sm-12 text-center bonus">
                <?php echo strip_tags(get_field('bonus_default'),'<p><strong><b><i><span><small>'); ?>
            </div>
            <div class="col-md-3 col-sm-12 text-center">
                <a class="bonus-now-bg" title="Get your bonus now" href="<?php the_field('outlink_desktop_default'); ?>" target="_blank"></a>
            </div>
        </div>
    </div>
</div>