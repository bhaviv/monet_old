<div class="row review-rating text-center">
    <?php
    if (!isset($ratings)):
        echo '<!-- Invalid ratings value. -->';
    else:
        foreach($ratings as $rating):
            $rating_class_score = (float)$rating['value'] * 10;
        ?>
            <div class="col-lg-2 col-sm-4 col-center text-center">
                <div class="circularProgress --<?php echo $rating_class_score; ?> col-center">
                    <div class="circle-progress-overlay">
                        <p class="rating text-center">
                            <?php echo $rating['value']; ?>
                            <span class="text-uppercase"><?php echo $rating['title']; ?></span>
                        </p>
                    </div>
                </div>
            </div>
        <?php
        endforeach;
    endif;
    ?>
</div>