<?php
if (!isset($reviewer)){
    $reviewer = get_field('reviewer');
}

if (isset($reviewer)):
?>
    <div class="col-md-12 author text-center">
        <img src="<?php the_field('profile_image','user_' . $reviewer['ID']); ?>" alt="author avatar" class="img-responsive author-pic">
        <div class="author-wrapper">
            <span class="name text-uppercase"><?php printf('%s %s',$reviewer['user_firstname'],$reviewer['user_lastname']); ?></span>
            <span class="description"><?php echo $reviewer['user_description']; ?></span>
        </div>
        <i class="icon-quotes"></i>
    </div>
<?php
endif;