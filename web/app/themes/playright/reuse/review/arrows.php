<div class="brand-arrows text-uppercase text-center">
    <?php
        $prev = get_permalink(get_adjacent_post(false,'',false));
        $next = get_permalink(get_adjacent_post(false,'',true));
    ?>
    <a class="previous-brand" role="switch" title="Previous Brand" href="<?php echo $prev; ?>"><i class="icon-arrow-left"></i><p>prev brand</p></a>
    <a class="next-brand" role="switch" title="Next Brand" href="<?php echo $next; ?>"><p>next brand</p><i class="icon-arrow-left reverse"></i></a>
</div>