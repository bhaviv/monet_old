<div class="col-md-12 bonus-light-card">
    <div class="wrapper">
        <div class="text">
            <?php echo strip_tags(get_field('bonus_default'),'<p><strong><b><i><span><small><h1><h2><h3><h4><h5><h6>'); ?>
        </div>
        <div class="button">
            <a class="get-bonus" title="Get your bonus now" role="link" target="_blank" href="<?php the_field('outlink_desktop_default'); ?>" rel="nofollow"></a>
        </div>
    </div>
</div>