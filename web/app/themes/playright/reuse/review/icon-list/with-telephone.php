<?php
$telephone = get_field('telephone');
$live_chat = get_field('live_chat');
$ticket_system = get_field('ticket_system');
$email = get_field('email');
?>

<div class="col-md-12 review-icon-list-with-telephone">
    <div class="row features text-center">
        <div class="field col-xs-6 col-sm-6 col-md-5 offset-1 col-lg-3 col-center">
            <div class="feature-wrapper text-center text-capitalize center-block">
                <div class="feature">
                    <i class="icon-mobile-site"></i>
                    <div class="title">
                        Phone
                    </div>
                </div>
                <div class="state">
                    <i class="<?php echo $telephone ? 'icon-checkmark' : 'icon-cancel---close'; ?>"></i>
                </div>
            </div>
            <div class="additional-field center-block"><?php echo $telephone ? $telephone : 'N/A'; ?></div>
        </div>

        <div class="field col-xs-6 col-sm-6 col-md-5 offset-1 col-lg-3 col-center">
            <div class="feature-wrapper text-center text-capitalize center-block">
                <div class="feature">
                    <i class="icon-chat-games"></i>
                    <div class="title">
                        Live Chat
                    </div>
                </div>
                <div class="state">
                    <i class="<?php echo $live_chat ? 'icon-checkmark' : 'icon-cancel---close'; ?>"></i>
                </div>
            </div>
            <div class="additional-field center-block">
            <?php
                echo $live_chat ? $live_chat : 'N/A';
            ?>
            </div>
        </div>

        <div class="field col-xs-6 col-sm-6 col-md-5 offset-1 col-lg-3 col-center">
            <div class="feature-wrapper text-center text-capitalize center-block">
                <div class="feature">
                    <i class="icon-e-mail"></i>
                    <div class="title">
                        E-Mail
                    </div>
                </div>
                <div class="state">
                    <i class="<?php echo $email ? 'icon-checkmark' : 'icon-cancel---close'; ?>"></i>
                </div>
            </div>
            <div class="additional-field center-block">
            <?php
                echo $email ? sprintf('<a target="_blank" href="mailto:%s">%s</a>',$email,$email) : 'N/A';
            ?>
            </div>
        </div>

        <div class="field col-xs-6 col-sm-6 col-md-5 offset-1 col-lg-3 col-center">
            <div class="feature-wrapper text-center text-capitalize center-block">
                <div class="feature">
                    <i class="icon-e-mail"></i>
                    <div class="title">
                        Ticket System
                    </div>
                </div>
                <div class="state">
                    <i class="<?php echo $ticket_system ? 'icon-checkmark' : 'icon-cancel---close'; ?>"></i>
                </div>
            </div>
            <div class="additional-field center-block">
            <?php
                echo $ticket_system ? 'Yes' : 'No';
            ?>
            </div>
        </div>
    </div>
</div>