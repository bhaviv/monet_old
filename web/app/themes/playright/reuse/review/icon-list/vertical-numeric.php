<div class="col-md-12 review-icon-list-vertical">
    <div class="row text-center">
        <?php 
            if (is_array($items)):
                foreach($items as $key => $value):?>
                    <?php
                       if (!(isset($available_key) && $available_key == 'sports_markets' && !$value)):?>
                        <div class="col-lg-2 col-sm-3 col-xs-6 col-center">
                            <div class="feature-card <?php echo $value ? '' : 'disabled'; ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="top-section">
                                            <?php $value = $value == 0 ? '' : $value;?>
                                            <p class="additional-field text-center"><strong><?php echo $value ?> </strong></p>
                                        </div>
                                    </div>
                                    <div class="col-md-12 bottom-section">
                                        <div class="feature-wrapper">
                                            <i class="icon-<?php echo sanitize_title($key); ?>"></i>
                                            <p class="field-name"><?php echo $key; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php   
                        endif;
                endforeach;
            endif;
        ?>
    </div>
</div>
