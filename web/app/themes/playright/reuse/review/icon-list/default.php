<div class="col-md-8 col-md-offset-2 review-icon-list text-center">
    <?php 
    if (is_array($items)):
        foreach($items as $key => $value):
    ?>
        <div class="col-md-3 col-sm-4 col-xs-12 col-center">
            <div class="row feature-card feature-items <?php echo $value ? '' : 'disabled'; ?>">
                <div class="col-md-9 ol-sm-9 col-xs-8 feature text-center">
                    <i class="icon-<?php echo sanitize_title($key); ?>"></i>
                    <div class="field-title"><?php echo $key; ?></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-4 status">
                    <i class="<?php echo $value ? 'icon-checkmark' : 'icon-cancel---close'; ?>"></i>
                </div>
            </div>
        </div>
    <?php
        endforeach;
    endif;
?>
</div>