<div class="container exclusive-bonus-form">
    <div class="row">
        <h2 class="text-center"><?php echo get_theme_mod('newsletter_form_title'); ?></h2>
        <div class="col-md-8 col-md-offset-2">
            <p class="text-center"><?php echo get_theme_mod('newsletter_form_description'); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-sm-12 col-md-offset-1">
            <form class="text-center">
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Your Name" required="">
                    <div class="text-error"></div>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="name" placeholder="Your Email" required="">
                    <div class="text-error"></div>
                </div>
                <button type="submit" class="default-button">Send</button>
                <div class="terms text-left">
                    <input type="checkbox" name="newsletter_tc" class="checkbox-custom">
                    <label for="newsletter-tc icon-checkmark">
                        <a href="<?php echo esc_attr(get_theme_mod('newsletter_form_terms_link')); ?>"
                           target="_blank">I have read and agreed to the terms &amp; conditions.</a>
                        <div>
                            <small> *We will never spam you, or pass on your personal data without permission.</small>
                        </div>
                    </label>
                    <div class="text-error"></div>
                </div>
            </form>
        </div>
    </div>
</div>