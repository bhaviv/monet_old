<?php

$related_slot_reviews = get_field('related_slot_reviews');

foreach($related_slot_reviews as $review):
$review = $review['slot_review'];
?>
<div class="side-reviews-list text-center">
    <h3 class="text-left"><?php echo $review->post_title; ?></h3>
    <img src="<?php echo get_the_post_thumbnail_url($review->ID); ?>" alt="img-logo">
    <a href="<?php echo get_permalink($review->ID); ?>" class="default-button">full review</a>
</div>
<?php
endforeach;