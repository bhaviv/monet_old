<?php
if (isset($slider_images)):
?>
<div class="slider-multiitem-mobile text-center">
    <div class="slick-carousel-instance"  data-slick='{"arrows": false}'>
        <?php 
        foreach($slider_images as $slider):
        ?>
        <div class="slick-element">
            <div class="element" style="background: url(<?php echo $slider['image']; ?>) center center / cover no-repeat;"></div>
        </div>
        <?php
        endforeach;
        ?>
    </div>
    <a class="default-button text-center" href="<?php the_field('outlink_desktop_default'); ?>">Play Game</a>
</div>
<?php
endif;