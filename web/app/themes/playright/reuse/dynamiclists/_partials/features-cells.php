<?php
/**
 * Sub-Template Name: Features Cell
 */
?>

<div class="cell features hidden-sm">
    <ul class="list"> 
           <?php foreach($features as $feature):
            $feature_key =   'icon-'.str_replace(' ', '-', strtolower($feature)); ?>
                <li data-toggle="<?php $feature ?>">
                    <i class="<?php echo $feature_key ?>"></i>
                    <span class="hover-tooltip"><?php echo $feature ?></span>
                </li>
            <?php endforeach; ?>
    </ul>
</div>

