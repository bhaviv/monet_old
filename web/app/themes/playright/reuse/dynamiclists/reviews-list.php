<div class="row reviews-list hidden-xs">
    <div class="item col-md-12">
        <div class="row top-layer">
            <div class="col-md-3 col-lg-2 col-sm-3 col-xs-4 logo-border">
                <a href="#" rel="nofollow" target="_blank">
                  <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="brand logo" class="img-responsive">
                </a>
            </div>
            <div class="col-md-9 col-lg-10 col-sm-9 col-xs-8">
                <h3>32 Red Casino</h3>
                <p class="post-paragraph">Since it opened its virtual doors in 2002, 32Red has picked up pretty much every major award in the industry, from Casino of the Year to Casino of the Decade. It is the definition of a big, established online casino. Everything from banking and support to game choice (on both mobile and desktop) is done to the highest standard.
                </p>
            </div>
            <div class="col-md-3 col-lg-2 col-sm-3 col-xs-4 text-center">
                <div class="rate-it">
                    <i class="stars" data-rate_amount="4.5">
                    </i>
                </div>
                <div class="score">(174)</div>
            </div>
            <div class="col-md-12 col-lg-10 col-sm-9 col-xs-8">
                <a class="review-link" title="Read Review" href="../casino/32red" target="“_blank”">Read Review</a>
            </div>
        </div>
        <div class="row bottom-layer">
            <div class="col-md-9 col-sm-8">
                <div class="exclusive-bonus">
                    <p style="text-align: left;"><span style="font-size: 24pt;">Special Bonus - Get 100% up to £1500</span></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 text-right button">
                <a class="default-button green" role="link" href="#" target="_blank" rel="nofollow" title="GET YOUR BONUS NOW">Get your bonus now</a>
            </div>
        </div>
    </div>

    <div class="item col-md-12">
        <div class="row top-layer">
            <div class="col-md-3 col-lg-2 col-sm-3 col-xs-4 logo-border">
                <a href="#" rel="nofollow" target="_blank">
                    <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="brand logo" class="img-responsive">
                </a>
            </div>
            <div class="col-md-9 col-lg-10 col-sm-9 col-xs-8">
                <h3>32 Red Casino</h3>
                <p class="post-paragraph">Since it opened its virtual doors in 2002, 32Red has picked up pretty much every major award in the industry, from Casino of the Year to Casino of the Decade. It is the definition of a big, established online casino. Everything from banking and support to game choice (on both mobile and desktop) is done to the highest standard.
                </p>
            </div>
            <div class="col-md-3 col-lg-2 col-sm-3 col-xs-4 text-center">
                <div class="rate-it">
                    <i class="stars" data-rate_amount="4.5">
                    </i>
                </div>
                <div class="score">(174)</div>
            </div>
            <div class="col-md-12 col-lg-10 col-sm-9 col-xs-8">
                <a class="review-link" title="Read Review" href="../casino/32red" target="“_blank”">Read Review</a>
            </div>
        </div>
        <div class="row bottom-layer">
            <div class="col-md-9 col-sm-8">
                <div class="exclusive-bonus">
                    <p style="text-align: left;"><span style="font-size: 24pt;">Special Bonus - Get 100% up to £1500</span></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 text-right button">
                <a class="default-button green" role="link" href="#" target="_blank" rel="nofollow" title="GET YOUR BONUS NOW">Get your bonus now</a>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 hidden-lg hidden-md hidden-sm reviews-list-mobile">
    <div class="item row">
        <div class="top-layer row">
            <div class="col-xs-6">
                <a href="#" rel="nofollow" target="_blank">
                    <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="brand logo" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-6">
                <div class="rate-it text-right">
                    <i class="stars" data-rate_amount="4.5">
                    </i>
                </div>
                <div class="score">(174)</div>
            </div>
            <div class="col-xs-12 get-bonus text-center">
                <div class="text">
                    <p style="text-align: center;"><span style="font-size: 16pt;"><strong>500% up to $4000<br></strong></span><span style="font-size: 8pt;">T&amp;C's Apply</span></p>
                    <a class="default-button green" role="link" href="#" target="_blank" rel="nofollow" title="GET YOUR BONUS NOW">Get your bonus now</a>
                </div>
            </div>
            <div class="col-xs-12 bottom">
                <p class="post-paragraph">Since it opened its virtual doors in 2002, 32Red has picked up pretty much every major award in the industry, from Casino of the Year to Casino of the Decade. It is the definition of a big, established online casino. Everything from banking and support to game choice (on both mobile and desktop) is done to the highest standard.
                </p>
                <a class="review-link" title="Read Review" href="../casino/32red" target="“_blank”">Read Review</a>
            </div>
        </div>
    </div>

    <div class="item row">
        <div class="top-layer row">
            <div class="col-xs-6">
                <a href="#" rel="nofollow" target="_blank">
                    <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="brand logo" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-6">
                <div class="rate-it text-right">
                    <i class="stars" data-rate_amount="4.5">
                    </i>
                </div>
                <div class="score">(174)</div>
            </div>
            <div class="col-xs-12 get-bonus text-center">
                <div class="text">
                    <p style="text-align: center;"><span style="font-size: 16pt;"><strong>500% up to $4000<br></strong></span><span style="font-size: 8pt;">T&amp;C's Apply</span></p>
                    <a class="default-button green" role="link" href="#" target="_blank" rel="nofollow" title="GET YOUR BONUS NOW">Get your bonus now</a>
                </div>
            </div>
            <div class="col-xs-12 bottom">
                <p class="post-paragraph">Since it opened its virtual doors in 2002, 32Red has picked up pretty much every major award in the industry, from Casino of the Year to Casino of the Decade. It is the definition of a big, established online casino. Everything from banking and support to game choice (on both mobile and desktop) is done to the highest standard.
                </p>
                <a class="review-link" title="Read Review" href="../casino/32red" target="“_blank”">Read Review</a>
            </div>
        </div>
    </div>
</div>