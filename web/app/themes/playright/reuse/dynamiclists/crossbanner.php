<?php
    // We pass null to the second argument to resolve the list using the list id.
    // It's a good idea to do so when using a "static" list that don't have any relation to a post/page.
    Autoload\DynamicListResolver::make('crossbanner_dynamic_list',null,get_theme_mod('cross_banner_unicorn_list'));
?>

<div id="crossbanner_dynamic_list"></div>

<script type="text/html" id="crossbanner_dynamic_list_template" style="display:none">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="casino-wrapper">
            <div class="corner">Casino</div>
            <div class="logo">
                <a href="{outlink}" target="_blank">
                    <img src="{logo}">
                </a>
            </div>
            <div class="bonus-text">
                {bonus_text}
            </div>
            <div class="button">
                <a href="{outlink}" target="_blank" class="default-button green">get bonus</a>
            </div>
        </div>
    </div>
</script>