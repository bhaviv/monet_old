<?php
    Autoload\DynamicListResolver::make('dynamiclist_slider_area',get_the_ID());
?>

<script type="text/html" id="dynamiclist_slider_area_template">
    <div class="slide-element">
        <div class="dynamic-item-wrapper hidden-xs">
            <div class="cell logo" style="background-color:#40c018">
                <a target="_blank" href="{outlink}">
                    <img src="{logo}" alt="brand-logo" class="img-responsive center-block">
                </a>
            </div>
            <div class="cell bonus">
                <div class="text">
                    {bonus_text}
                </div>
                <div class="more">
                    More Info
                    <div class="bonus-box">
                        {bonus_text}
                    </div>
                </div>

            </div>
            <div class="cell features hidden-sm">
                <ul class="list list-unstyled list-inline">
                    <li data-toggle="VIP Program">
                        <i class="icon-vip-program"></i>
                        <span class="hover-tooltip">VIP Program</span>
                    </li>
                    <li data-toggle="Live Dealer">
                        <i class="icon-live-dealer"></i>
                        <span class="hover-tooltip">Live Dealer</span>
                    </li>
                    <li data-toggle="Mobile Compatible">
                        <i class="icon-mobile-compatible"></i>
                        <span class="hover-tooltip">Mobile Compatible</span>
                    </li>
                    <li data-toggle="Downloadable Software">
                        <i class="icon-downloadable-software"></i>
                        <span class="hover-tooltip">Downloadable Software</span>
                    </li>
                    <li data-toggle="Instant Play">
                        <i class="icon-instant-play"></i>
                        <span class="hover-tooltip">Instant Play</span>
                    </li>
                    <li data-toggle="Play With PayPal">
                        <i class="icon-play-with-paypal"></i>
                        <span class="hover-tooltip">Play With PayPal</span>
                    </li>
                </ul>
            </div>
            <div class="cell get-bonus">
                <a class="button" href="{outlink}" rel="nofollow"></a>
            </div>
        </div>

        <div class="mobile-brands-list hidden-md hidden-sm hidden-lg">
            <div class="archive-wrapper col-xs-12">
                <div class="col-xs-6">
                    <a href="{outlink}" target="_blank">
                        <img src="{logo}" alt="brand logo" class="img-responsive">
                    </a>
                    <div class="rate-it">
                        <i class="stars"
                            data-rate_amount="4.5"></i>
                        <a class="review-link" title="Brand NAme"
                            target="_blank"><u>Read Review</u></a>
                    </div>
                </div>
                <div class="col-xs-6">
                    <a class="text-uppercase bonus-button pull-right" title="Get bonus" href="{outlink}"
                        target="_blank">
                    </a>
                </div>
                <div class="col-xs-12">
                    <hr>
                    <span>{bonus_text}</span>
                </div>
            </div>
        </div>
    </div>
</script>

<div class="slider-dl">
    <div class="slick-carousel-instance" id="dynamiclist_slider_area" data-slick='{"arrows": false,"dots": true}'>
        
    </div>
</div>

