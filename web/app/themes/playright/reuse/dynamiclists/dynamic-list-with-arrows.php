<div class="dynamic-list-with-arrows brands-table">
    <div class="headers-wrapper">
        <!-- headers -->
        <div class="headers">
            <div class="cell logo">Bookmaker</div>
            <div class="cell bonus">Welcome bonus</div>
            <div class="cell rating">Rating</div>
            <div class="cell features hidden-sm">Our score</div>
            <div class="cell get-bonus">Bet now</div>
        </div><!-- .headers -->
    </div>

    <div class="funfun-dynamiclist-wrapper">
        <div class="table-row">
            <div class="cell logo" style="background-color:#0a1f44"">
                <a href="{outlink}" target="_blank" rel="nofollow">
                    <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Desktop--Retina/NetBet-MainLogo-480x165.png" alt="brand logo" class="img-responsive">
                </a>
                <div class="white-box"></div>

                <div class="triangle t2" style="border-left: 24px solid #0a1f44;"></div>
                <div class="triangle t1" style="border-left: 24px solid #fff;"></div>
                <div class="triangle t3" style="border-left: 24px solid #0a1f44;"></div>
            </div>
            <div class="cell bonus">
                <div class="content">
                    <p style="text-align: center;">
                        <b><span style="font-size: 18pt;">Deposit £50 Play with&nbsp;£100</span><br></b>+ 10 Vegas Bonus Spins<br><span style="text-align: left; font-size: 10px;">T&amp;C's Apply</span>
                    </p>
                </div>
            </div>
            <div class="cell rating rating-instance">
                <div class="score">
                    <span class="description"></span>
                </div>
                <div class="score-stars">
                    <div class="rate-it">
                        <ul class="tp-rating" data-default="{stars}">
                            <li class="active"><i class="stars"></i></li>
                            <li class="active"><i class="stars"></i></li>
                            <li class="active"><i class="stars"></i></li>
                            <li class="active"><i class="stars"></i></li>
                            <li class="active"><i class="stars"></i></li>
                        </ul>
                    </div>
                    <span class="votes_text">Votes ({votes})</span>
                </div>
            </div>
            <div class="cell features hidden-sm">
                <div>9.8</div>
                <a class="review-link" title="{brand_name}" href="{review_url}" target="_blank">Read Review</a>
            </div>
            <div class="cell get-bonus">
                <a title="Get bonus" class="get-bonus-button" target="_blank" rel="nofollow" href="{outlink}">Get <strong class="text-uppercase">bonus</strong></a>
                <a class="visit-site" title="Visit Site" href="{outlink}" target="_blank">Visit site</a>
            </div>
        </div>
    </div>

</div>
