<?php
    Autoload\DynamicListResolver::make('dynamic_list_slider',get_the_ID());
?>

<script type="text/html" id="dynamic_list_slider_template">
    <div class="slider-element row">
        <div class="col-md-7">
            <div class="left-slide-part">
                <div class="logo">
                    <img src="{logo}"/>
                    <h3>{title}</h3>
                </div>
                <div class="text">
                    {excerpt}
                    <a href="{review_url}" title="Read Review">Read Review</a>
                </div>
                <div class="benefits-wrapper">
                    <div class="benefit-card">{registration_process}</div>
                    <div class="benefit-card device-list">
                        <p class="device">
                            <i class="{slider_device_support_desktop_class}"></i> Desktop
                        </p>
                        <p class="device">
                            <i class="{slider_device_support_tablet_class}"></i> Tablet
                        </p>
                        <p class="device">
                            <i class="{slider_device_support_mobile_class}"></i> Mobile
                        </p>
                    </div>
                    <div class="benefit-card">{bonus_text}</div>
                </div>
            </div>
            <a class="default-button" href="{outlink}" target="_blank">Get bonus</a>
        </div><!-- .col-md-7 -->

        <div class="col-md-5 right-part">
            <img class="img-responsive" src="{lending_page_screenshot}"/>
        </div><!-- .col-md-5 -->
    </div>
</script><!-- dynamic_list_slider_template -->

<div id="dynamic_list_slider"
     class="slick-carousel-instance"
     data-slick='{"nextArrow":"<i class=\"icon-arrow-left next-slide\"></i>", "prevArrow":"<i class=\"icon-arrow-right previous-slide\"></i>"}'>
</div><!-- dynamic_list_slider -->
