<div class="container dynamic-list-hp">
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <h3 class="text-center text-uppercase betting">Betting</h3>
            </div>

            <?php
            Autoload\DynamicListResolver::make('dynamiclist_area_betting',get_the_ID());
            ?>

            <script type="text/html" id="dynamiclist_area_betting_template">
                <div class="col-md-12 col-sm-12 wrapper">
                    <div class="row top-wrapper">
                        <div class="col-xs-5">
                            <img src="{logo}" class="img-responsive">
                        </div>
                        <div class="col-xs-7">
                            <a class="default-button green-bg pull-right text-center">PLAY NOW</a>
                        </div>
                    </div>
                    <div class="row bottom-wrapper">
                        <div class="col-xs-4 rating rating-instance">
                            <div class="score" style="display:none">
                                <span class="description"></span>
                            </div>
                            <div class="score-stars">
                                <div class="rate-it">
                                    <ul class="tp-rating" data-default="{stars}">
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                    </ul>
                                </div>
                                <span class="votes_text" style="display:none">Votes ({votes})</span>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-8">
                            <a class="review-link pull-right text-right" title="{title}" href="{outlink}" target="_blank">{title}</a>
                        </div>
                    </div>
                </div>
            </script>

            <div id="dynamiclist_area_betting"></div>

            <div class="col-md-12 col-sm-12">
                <a class="check-button text-center text-uppercase center-block" title="Check betting list" href="<?php the_field('betting_list_link'); ?>" target=“_blank”>Check betting list</a>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <h3 class="text-center text-uppercase casino">Casino</h3>
            </div>

            <?php
            Autoload\DynamicListResolver::make('dynamiclist_area_casino',get_the_ID());
            ?>

            <script type="text/html" id="dynamiclist_area_casino_template">
                <div class="col-md-12 col-sm-12 wrapper">
                    <div class="row top-wrapper">
                        <div class="col-xs-5">
                            <img src="{logo}" class="img-responsive">
                        </div>
                        <div class="col-xs-7">
                            <a class="default-button green-bg pull-right text-center">PLAY NOW</a>
                        </div>
                    </div>
                    <div class="row bottom-wrapper">
                        <div class="col-xs-4 rating rating-instance">
                            <div class="score" style="display:none">
                                <span class="description"></span>
                            </div>
                            <div class="score-stars">
                                <div class="rate-it">
                                    <ul class="tp-rating" data-default="{stars}">
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                    </ul>
                                </div>
                                <span class="votes_text" style="display:none">Votes ({votes})</span>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-8">
                            <a class="review-link pull-right text-right" title="{title}" href="{outlink}" target="_blank">{title}</a>
                        </div>
                    </div>
                </div>
            </script>

            <div id="dynamiclist_area_casino"></div>

            <div class="col-md-12 col-sm-12">
                <a class="check-button text-center text-uppercase center-block" title="Check casino list" href="<?php the_field('casino_list_link'); ?>" target=“_blank”>Check casino list</a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <h3 class="text-center text-uppercase bingo">Bingo</h3>
            </div>

            <?php
            Autoload\DynamicListResolver::make('dynamiclist_area_bingo',get_the_ID());
            ?>

            <script type="text/html" id="dynamiclist_area_bingo_template">
                <div class="col-md-12 col-sm-12 wrapper">
                    <div class="row top-wrapper">
                        <div class="col-xs-5">
                            <img src="{logo}" class="img-responsive">
                        </div>
                        <div class="col-xs-7">
                            <a class="default-button green-bg pull-right text-center">PLAY NOW</a>
                        </div>
                    </div>
                    <div class="row bottom-wrapper">
                        <div class="col-xs-4 rating rating-instance">
                            <div class="score" style="display:none">
                                <span class="description"></span>
                            </div>
                            <div class="score-stars">
                                <div class="rate-it">
                                    <ul class="tp-rating" data-default="{stars}">
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                        <li class="active"><i class="stars"></i></li>
                                    </ul>
                                </div>
                                <span class="votes_text" style="display:none">Votes ({votes})</span>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-8">
                            <a class="review-link pull-right text-right" title="{title}" href="{outlink}" target="_blank">{title}</a>
                        </div>
                    </div>
                </div>
            </script>

            <div id="dynamiclist_area_bingo"></div>

            <div class="col-md-12 col-sm-12">
                <a class="check-button text-center text-uppercase center-block" title="Check bingo list" href="<?php the_field('bingo_list_link'); ?>" target=“_blank”>Check bingo list</a>
            </div>
        </div>
    </div>

</div>