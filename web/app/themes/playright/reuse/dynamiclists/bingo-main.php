<?php
    Autoload\DynamicListResolver::make('dynamiclist_area',get_the_ID());
?>

<!-- Templates -->

<script type="text/html" id="dynamiclist_area_template">
    <div class="table-row">
        <div class="cell logo" style="background-color:{background_color}">
            <a href="{outlink}" target="_blank" rel="nofollow">
                <img class="img-responsive" src="{logo}"/>
            </a>
        </div>
        <div class="cell bonus">
            <div class="content">
                {bonus_text}
            </div>
        </div>
        <div class="cell features hidden-sm">
            <ul class="list">
                <li data-toggle="VIP Program">
                    <i class="icon-vip-program"></i>
                    <span class="hover-tooltip">VIP Program</span>
                </li>
                <li data-toggle="Live Dealer">
                    <i class="icon-live-dealer"></i>
                    <span class="hover-tooltip">Live Dealer</span>
                </li>
                <li data-toggle="Mobile Compatible">
                    <i class="icon-mobile-compatible"></i>
                    <span class="hover-tooltip">Mobile Compatible</span>
                </li>
                <li data-toggle="Downloadable Software">
                    <i class="icon-downloadable-software"></i>
                    <span class="hover-tooltip">Downloadable Software</span>
                </li>
                <li data-toggle="Instant Play">
                    <i class="icon-instant-play"></i>
                    <span class="hover-tooltip">Instant Play</span>
                </li>
                <li data-toggle="Play With PayPal">
                    <i class="icon-play-with-paypal"></i>
                    <span class="hover-tooltip">Play With PayPal</span>
                </li>
            </ul>
            <p class="rooms">{number_of_rooms} rooms</p>
        </div>
        <div class="cell rating rating-instance">
            <div class="score">
                <span class="description"></span>
            </div>
            <div class="score-stars">
                <div class="rate-it">
                    <ul class="tp-rating" data-default="{stars}">
                        <li class="active"><i class="stars"></i></li>
                        <li class="active"><i class="stars"></i></li>
                        <li class="active"><i class="stars"></i></li>
                        <li class="active"><i class="stars"></i></li>
                        <li class="active"><i class="stars"></i></li>
                    </ul>
                </div>
                <span class="votes_text">Votes ({votes})</span>
            </div>
        </div>
        <div class="cell features hidden-sm">
            <div>{score}</div>
            <a class="review-link" title="{brand_name}" href="{review_url}" target="_blank">{title} Review</a>
        </div>
        <div class="cell get-bonus">
            <a title="Get bonus" class="get-bonus-button background-green" target="_blank" rel="nofollow" href="{outlink}">Play <span class="text-uppercase">Now</span></a>
            <a class="visit-site" title="Visit Site" href="{outlink}" target="_blank">Visit site</a>
        </div>
    </div>
</script><!-- dynamiclist_area_template -->

<script type="text/html" id="dynamiclist_area_mobile_template">
    <div class="archive-wrapper effect2">
        <div class="col-xs-6 margin-mobile">
            <a href="{outlink}" rel="nofollow" target="_blank">
                <img src="{mobile_logo}" alt="{title}" class="img-responsive"/>
            </a>
            <div class="rate-it">
                <i class="stars" data-rate_amount="4.5">
                </i>
            </div>
            <div class="score-row">
                <span>Our Score</span> <span class="score">{score}</span>
            </div>
        </div>
        <div class="col-xs-6 arrow-bg">
            <div class="triangle"></div>
            <a title="Get bonus" class="bonus-txt" href="#" target="_blank" rel="nofollow">
             {bonus_text}
            </a>
            <a class="get-bonus-button background-blue" title="Get bonus" href="{outlink}" target="_blank" rel="nofollow"><strong>Play</strong> Now</a>
        </div>
    </div>
</script><!-- dynamiclist_area_mobile_template -->

<!-- Wrappers -->

<div class="dl-bingo brands-table hidden-xs">

    <div class="headers-wrapper">
        <div class="headers">
            <div class="cell logo">Bookmaker</div>
            <div class="cell bonus">Welcome bonus</div>
            <div class="cell deposit hidden-sm">features</div>
            <div class="cell rating">Rating</div>
            <div class="cell features hidden-sm">Our score</div>
            <div class="cell get-bonus">Bet now</div>
        </div><!-- .headers -->
    </div><!-- .headers-wrapper -->

    <div id="dynamiclist_area"
         class="funfun-dynamiclist-wrapper"
         data-filter-options='{"selector": "#filter", "fields": ["regulators","devices"]}'
         data-sort-options='{"selector": "#sorter","fields": ["rating"]}'>
    </div><!-- dynamiclist_area -->

</div><!-- .brands-table -->

<div id="dynamiclist_area_mobile"
     class="dl-mobile-shadow-effect mobile-bingo hidden-sm hidden-md hidden-lg">
</div><!-- dynamiclist_area_mobile -->