<div class="sidelist row">

    <?php
        Autoload\DynamicListResolver::make('side_dynamiclist_area','sidelist',get_theme_mod('side_dynamiclist_unicorn_list'));
    ?>

    <script type="text/html" id="side_dynamiclist_area_template">
        <div class="item-wrapper col-md-12">
            <div class="top row">
                <div class="col-xs-6">
                    <img src="{logo}" alt="{title}" class="img-responsive pull-left">
                </div>
                <div class="col-xs-6">
                    <a class="default-button text-center text-uppercase pull-right" role="link" href="{outlink}" rel="nofollow">Visit {title}</a>
                </div>
            </div>
            <div class="bottom row">
                <div class="col-xs-12">
                    <a class="pull-right" href="{review_url}">{title}</a>
                    <hr>
                </div>
            </div>
        </div>
    </script>

    <div id="side_dynamiclist_area"></div>

</div>