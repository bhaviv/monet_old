<?php
    Autoload\DynamicListResolver::make('dynamiclist_area',get_the_ID());
?>

<!-- Templates -->

<script type="text/html" id="dynamiclist_area_template">
    <div class="table-row">
        <div class="cell logo" style="background-color:{background_color}">
            <a href="{outlink}" target="_blank" rel="nofollow">
                <img class="img-responsive" src="{logo}"/>
            </a>
        </div>
        <div class="cell bonus">
            <div class="content">
                {bonus_text}
            </div>
        </div>
        <div class="cell deposit hidden-sm">
            <div class="min-deposit">{minimum_deposit}</div>
        </div>
        <div class="cell rating rating-instance">
            <div class="score">
                <span class="description"></span>
            </div>
            <div class="score-stars">
                <div class="rate-it">
                    <ul class="tp-rating" data-default="{stars}">
                        <li class="active"><i class="stars"></i></li>
                        <li class="active"><i class="stars"></i></li>
                        <li class="active"><i class="stars"></i></li>
                        <li class="active"><i class="stars"></i></li>
                        <li class="active"><i class="stars"></i></li>
                    </ul>
                </div>
                <span class="votes_text">Votes ({votes})</span>
            </div>
        </div>
        <div class="cell features hidden-sm">
            <div>{score}</div>
            <a class="review-link" title="{brand_name}" href="{review_url}" target="_blank">{title} Review</a>
        </div>
        <div class="cell get-bonus">
            <a title="Get bonus" class="get-bonus-button" target="_blank" rel="nofollow" href="{outlink}"></a>
            <a class="visit-site" title="Visit Site" href="{outlink}" target="_blank">Visit site</a>
        </div>
    </div>
</script><!-- dynamiclist_area_template -->

<script type="text/html" id="dynamiclist_area_mobile_template">
    <div class="archive-wrapper effect2">
        <div class="col-xs-6 margin-mobile">
            <a href="{outlink}" rel="nofollow" target="_blank">
                <img src="{mobile_logo}" alt="{title}" class="img-responsive"/>
            </a>
            <div class="rate-it">
                <i class="stars" data-rate_amount="4.5">
                </i>
            </div>
            <div class="score-row">
                <span>Our Score</span> <span class="score">{score}</span>
            </div>
        </div>
        <div class="col-xs-6 arrow-bg">
            <div class="triangle"></div>
            <a title="Get bonus" class="bonus-txt" href="#" target="_blank" rel="nofollow">
             {bonus_text}
            </a>
            <a class="text-uppercase get-bonus-button pull-right" title="Get bonus" href="{outlink}" target="_blank" rel="nofollow"> </a>
        </div>
    </div>
</script><!-- dynamiclist_area_mobile_template -->

<!-- Wrappers -->

<div class="dl-betting brands-table hidden-xs">

    <div class="headers-wrapper">
        <div class="headers">
            <div class="cell logo">Bookmaker</div>
            <div class="cell bonus">Welcome bonus</div>
            <div class="cell deposit hidden-sm">min.deposit</div>
            <div class="cell rating">Rating</div>
            <div class="cell features hidden-sm">Our score</div>
            <div class="cell get-bonus">Bet now</div>
        </div><!-- .headers -->
    </div><!-- .headers-wrapper -->

    <div id="dynamiclist_area"
         class="funfun-dynamiclist-wrapper"
         data-filter-options='{"selector": "#filter", "fields": ["regulators","devices"]}'
         data-sort-options='{"selector": "#sorter","fields": ["rating"]}'>
    </div><!-- dynamiclist_area -->

</div><!-- .brands-table -->

<div id="dynamiclist_area_mobile"
     class="dl-mobile-shadow-effect hidden-sm hidden-md hidden-lg">
</div><!-- dynamiclist_area_mobile -->