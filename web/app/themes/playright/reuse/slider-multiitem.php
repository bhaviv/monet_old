<?php
if (isset($slider_images)):
?>
<div class="slider-multiitem text-center">
    <div class="slider-for slick-carousel-instance x1"
         data-slick='{"slidesToShow": 1,"arrows": false, "asNavFor": ".x2"}'>
         <?php 
        foreach($slider_images as $slider):
        ?>
            <div class="slide-element" style="background: url(<?php echo $slider['image']; ?>) center center / cover no-repeat;"></div>
        <?php
        endforeach;
        ?>
    </div>
    <div class="slider-nav slick-carousel-instance x2"
         data-slick='{"slidesToShow": 3,"asNavFor": ".x1","centerMode": true,"slidesToScroll": 1,"focusOnSelect": true,"centerPadding": false,"nextArrow":"<i class=\"icon-arrow next-slide\"></i>","prevArrow":"<i class=\"icon-arrow previous-slide\"></i>"}'>
       
        <?php 
        foreach($slider_images as $slider):
        ?>
            <div class="slide-element">
                <div class="element" style="background: url(<?php echo $slider['image']; ?>) center center / cover no-repeat;"></div>
            </div>
        <?php
        endforeach;
        ?>
    </div>
</div>
<?php
endif;