<div class="col-md-12 ticker hidden-xs">
    <div class="feed-wrapper">
        <div class="live-banner"></div>
        <div class="slick-carousel-instance" data-slick='{"slidesToShow": 6, "slidesToScroll": 1, "nextArrow":"<i class=\"icon-arrow next-slide\"></i>", "responsive":[{"breakpoint": 1500, "settings":{"slidesToShow":5}},{"breakpoint": 1300, "settings":{"slidesToShow":4}},{"breakpoint": 1100, "settings":{"slidesToShow":3}},{"breakpoint": 850, "settings":{"slidesToShow":2}},{"breakpoint": 768, "settings":{"slidesToShow":1}}]}'>
            <div class="slide-element">
                <strong>Tottenham vs West Ham</strong>
                <span>04 Jan</span>
            </div>
            <div class="slide-element">
                <strong>Tottenham vs West Ham</strong>
                <span>04 Jan</span>
            </div>
            <div class="slide-element">
                <strong>Tottenham vs West Ham</strong>
                <span>04 Jan</span>
            </div>
            <div class="slide-element">
                <strong>Tottenham vs West Ham</strong>
                <span>04 Jan</span>
            </div>
            <div class="slide-element">
                <strong>Tottenham vs West Ham</strong>
                <span>04 Jan</span>
            </div>
            <div class="slide-element">
                <strong>Tottenham vs West Ham</strong>
                <span>04 Jan</span>
            </div>
            <div class="slide-element">
                <strong>Tottenham vs West Ham</strong>
                <span>04 Jan</span>
            </div>
        </div>
    </div>
</div>