<?php
$regulators = get_field('regulators');

if (!is_array($regulators)){
    $regulators = [];
}
?>

<div class="col-lg-10 col-lg-offset-1 hidden-xs licensing-trustworthiness">
    <div class="row sprite-features text-center ">
        <?php
            foreach($regulators as $regulator => $value):
        ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8 col-center">
                    <div class="sprite-item">
                        <div class="sprite <?php echo sanitize_title($value); ?>"></div>
                    </div>
                </div>
        <?php
            endforeach;
        ?>
    </div>
</div>

<div class="col-xs-8 col-xs-offset-2 hidden-sm hidden-md hidden-lg">
    <div class="row sprite-features text-center hidden-lg hidden-md hidden-sm licensing-trustworthiness">
        <div class="slick-carousel-instance" data-slick='{"nextArrow":"<i class=\"icon-arrow next-slide\"></i>", "prevArrow":"<i class=\"icon-arrow previous-slide\"></i>"}'>
            <?php
                foreach($regulators as $regulator => $value):
            ?>
            <div class="col-center">
                <div class="slider-element ">
                    <div class="sprite-item">
                        <div class="sprite <?php echo sanitize_title($value); ?>"></div>
                    </div>
                </div>
            </div>
            <?php
                endforeach;
            ?>
        </div>
    </div>
</div>
