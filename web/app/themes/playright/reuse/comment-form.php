<div class="comment-form">
    <form>
        <div class="form-group row">
            <div class="col-md-4">
                <i class="icon-your-name form-control-feedback"></i>
                <input type="text" class="form-control" name="author" placeholder="Name" aria-label="name">
            </div>
            <div class="col-md-4">
                <i class="icon-your-email form-control-feedback"></i>
                <input type="email" class="form-control" name="email" placeholder="Email" aria-label="email">
            </div>
            <div class="col-md-4">
                <div class="pull-right">
            <span class="minimum-characters" data-textarea_selector=".reply-message"
                  data-minimum-characters="50"></span>
                    characters left
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <i class="icon-your-message form-control-feedback"></i>
                <textarea class="form-control reply-message" name="message" placeholder="Your Message"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12 col-sm-6 checkbox">
                <input type="checkbox" name="agree" required role="button" aria-checked="false">
                <span>I agree with all the site rules</span>
            </div>
            <div class="col-xs-12 col-sm-6">
                <button type="submit" class="default-button" role="link">Post</button>
            </div>
        </div>
    </form>

</div>