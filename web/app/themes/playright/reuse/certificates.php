<div class="certificates hidden-sm hidden-md">
    <span>All Bookies are:</span>
    <span><i class="icon-uk-licensed"></i>UK licensed</span>
    <span><i class="icon-mobile-site"></i>Mobile friendly</span>
    <span><i class="icon-expertly-reviewed"></i> Independently reviewed</span>
    <div class="clearfix"></div>
</div>