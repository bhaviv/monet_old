<?php
/*
 * Variables list which we get for a table
 *  $title;
 *  $table_id;
 *  $columns_types; - must get for  each column
 *  $columns_headers; - if recieved than we need to display headers
 *  $columns; - must get
 *  $class; - optional
 *  $css; - optional
 */

?>
<?php if (!empty($css)): ?>
<style>
    <?php echo strip_tags($css);?>
</style>
<?php endif; ?>

<h3 class="table-name"><?php echo $table_name?></h3>
<table class="<?php echo $class; ?>">
    <thead>
        <?php if(!empty($columns_headers)) {
            foreach ($columns_headers as $column_header) {
                ?>
                <th>
                    <?php echo $column_header['column_header'];?>
                </th>
        <?php
            }
        }
        ?>
    </thead>
    <tbody>
        <?php
        for ($row = 0; $row < count($columns[0]['rows']); $row++) {
            echo '<tr>';
            for ($col = 0; $col < count($columns_types); $col++) {
                $column_type = $columns_types[$col]['column_type'];
                ?>

                <td><?php
                    if ($column_type == 'column_text') {?>

                        <?php echo $columns[$col]['rows'][$row][$column_type]?>
                    <?php } else { ?>
                        <img src=" <?php echo $columns[$col]['rows'][$row][$column_type]?>">

                    <?php } ?>

                </td>
                <?php
            }
            echo '</tr>';
        }
        ?>
</tbody>
</table>
