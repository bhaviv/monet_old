'use strict'

let brands = [
    {
        rating: [
            {rating_text: 'Overall',rating_value: 9},
            {rating_text: 'Support',rating_value: 9},
            {rating_text: 'Example Sort',rating_value: 10}
        ]
    },
    {
        rating: [
            {rating_text: 'Overall',rating_value: 7},
            {rating_text: 'Support',rating_value: 6},
            {rating_text: 'Example Sort 2',rating_value: 11}
        ]
    }
]


let fields = [
    'rating'
]

const SortList = require('../')

test('Rating sort fields are built correctly', () => {
    const list = new SortList(brands,fields)
    
    const sort_fields = list.build()

    expect(sort_fields['rating']).toEqual(["Overall", "Support", "Example Sort", "Example Sort 2"])
})

