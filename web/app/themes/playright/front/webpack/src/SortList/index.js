'use strict'

class SortList {

    /**
     * 
     * @param {array} brands 
     * @param {array} sort_fields 
     */
    constructor(brands,sort_fields){
        this.brands = brands
        this.sort_fields = sort_fields

        this.templates = {
            group: `<li>
                        <div class="row group-title">
                            <div class="col-sm-10">{group_name}</div>
                            <div class="col-sm-2"><i class="icon-filter-arrow"></i></div>
                        </div>
                        <ul class="sorter"></ul>
                    </li>`,
            item: `<li class="row">
                        <div class="col-sm-10 sorter-name">{name}</div>
                        <div class="col-sm-2 text-right"><i class="icon-filter-plus"></i></div>
                    </li>`
        }
    }

    /**
     * @returns {object}
     */
    build(){
       let groups = {}
       this.sort_fields.forEach( field => {
            let field_options = {}

            for (let i in this.brands){
                let brand = this.brands[i]
                let options = brand.data[field]

                switch(field){
                    case 'rating':
                        if (Array.isArray(options)){
                            options.forEach( option => {
                                for (let key in option){
                                    if (key === 'rating_text'){
                                        field_options[option[key]] = true
                                    }
                                }
                            })
                        }
                    break;
                    default:
                        if (Array.isArray(options)){
                            options.forEach( option => field_options[option] = true )
                        }
                    break;
                }
            }

            groups[field] = Object.keys(field_options)
       })

       return groups
    }

    /**
     * Sort event handler
     * 
     * @param {DynamicList} dynamicList
     * @param {string}      sort_field
     * @param {string}      group_name
     * @param {DOMElement}  list_item
     */
    sort({dynamicList,sort_field,group_name,list_item}){
        // Destroy the dynamic list
        dynamicList.destroy()

        // Re-order the brands array
        const brands = dynamicList.sortBrandsBy(group_name,sort_field)

        // Render the list
        brands.forEach( brand => {
            dynamicList.renderBrand(brand)
        })

        dynamicList.fire('afterRender')
    }

    filter({dynamicList,group_name,filter}){
        dynamicList.destroy()
        dynamicList.addFilter(group_name,filter)

        const brands = dynamicList.filterBrands()
        
        // Render the list
        brands.forEach( brand => {
            dynamicList.renderBrand(brand)
        })

        dynamicList.fire('afterRender')
    }

    /**
     * Activate sort item
     * 
     * @param {DOMELEMENT} item_node 
     * @param {DOMELEMENT} items_wrapper 
     */
    activateSortItem(item_node,items_wrapper){
        let item = items_wrapper.querySelector('li.active')
        if (item){
            item.classList.remove('active')
        }
        item_node.classList.add('active')
    }

}

module.exports = SortList