'use strict'

import SortList from './SortList'
import {render} from './StringTemplate'

class DynamicList {

    /**
     * @param {object} brands 
     */
    constructor(brands){
        this.brands = brands
        this.placement = false
        this.listSorter = false

        this.filters = {}
        this.events = {
            afterRender: []
        }
    }

    on(event,fn){
        if (this.events[event]){
            this.events[event].push(fn)
        }
    }

    fire(event){
        if (this.events[event]){
            this.events[event].forEach( callback => callback(this) )
        }
    }

    filterBrands(){
        const brands = this.brands

        return brands.filter( brand => {
            for (let group in this.filters){

                if (!brand.data[group]){
                    console.log('not defined',brand.data)
                    return false
                }
                
                if (!Array.isArray(brand.data[group])){
                    console.log('not array value',brand.data[group])
                    return false
                }

                for (let i in this.filters[group]){
                    console.log('source',brand.data[group],'is in',this.filters[group][i])
                    if (brand.data[group].indexOf(this.filters[group][i]) === -1){
                        return false
                    }
                }

                return true
            }
        })
    }

    addFilter(group,filter){
        if (!this.filters[group]){
            this.filters[group] = []
        }

        if (this.filters[group].indexOf(filter) === -1){
            this.filters[group].push(filter)
        }

        this.renderFilters()
    }

    renderFilters(){
        let filters_wrapper_node = document.querySelector(".selected-filters")
        filters_wrapper_node.innerHTML = ''

        let filter_template = `<div class="filter"><span class="name">{name}</span><span class="remove"><i class="icon-no"></i></span></div>`

        for (let group in this.filters){
            for (let i in this.filters[group]){
                let node = render(filter_template,{name: this.filters[group][i]})
                let close = node.querySelector('.remove')
                
                close.addEventListener('click', () => {
                    this.filters[group].splice(i,1)
                    this.renderFilters()
                    this.destroy();
                    
                    const brands = this.filterBrands()
                    
                    // Render the list
                    brands.forEach( brand => {
                        this.renderBrand(brand)
                    })

                    this.fire('afterRender')
                })

                filters_wrapper_node.appendChild(node)
            }
        }

        
    }

    /**
     * @returns void
     */
    render(){
        for (let i in this.brands){
            this.renderBrand(this.brands[i])
        }

        this.fire('afterRender')
    }

    /**
     * Destroy and re-render the dynamic list.
     */
    reset(){
        this.destroy().render()
    }

    /**
     * Render the dynamic list with a sort.
     * 
     * @param {string} group_name
     * @param {array}  sort_field 
     */
    sortBrandsBy(group_name,sort_field){
        let brands = this.brands

        const findRatingValue = (brand,field) => {
            if (!Array.isArray(brand.data.rating)){
                return 0;
            }
            for (let i in brand.data.rating){
                let rating = brand.data.rating[i]
                if (rating.rating_text === field){
                    return parseFloat(rating.rating_score)
                }
            }

            return 0;
        }

        brands = brands.sort( (brand_a,brand_b) => {
            let value = 0

            switch(group_name){
                case 'rating':
                    let a = findRatingValue(brand_a,sort_field)
                    let b = findRatingValue(brand_b,sort_field)
                    value = a - b
                    break;
                default:
                    value = parseFloat(brand_a.data[group_name][sort_field]) - parseFloat(brand_b.data[group_name][sort_field])
                break;
            }

            return value
        })

        return brands
    }

    /**
     * Destroy the dynamic list html.
     * 
     * @return DynamicList
     */
    destroy(){
        if (this.dynamiclist_node){
            this.dynamiclist_node.innerHTML = ''
        }

        if (this.mobile_dynamiclist_node){
            this.mobile_dynamiclist_node.innerHTML = ''
        }

        return this
    }

    /**
     * Handle the rendering of each brand.
     * 
     * @param {object} brand
     */
    renderBrand(brand){
        // Prepare the nodes once, to avoid multiple dom queries.
        if (!this.placement){
            this.placement = brand.placement
            this.dynamiclist_node = document.getElementById(this.placement)
            this.template_content = document.getElementById(`${this.placement}_template`)
            this.mobile_dynamiclist_node = document.querySelector(`#${this.placement}_mobile`)
            this.mobile_template_content = document.querySelector(`#${this.placement}_mobile_template`)

            if (this.dynamiclist_node && this.dynamiclist_node.attributes){
                
                let sort_raw_options = this.dynamiclist_node.attributes['data-sort-options'],
                    filter_raw_options = this.dynamiclist_node.attributes['data-filter-options']

                if (filter_raw_options){
                    let filter_options = JSON.parse(filter_raw_options.value)
                    this.filterListSorter = new SortList(this.brands,filter_options.fields)

                    let filter_items = this.filterListSorter.build()
                    let filter_node = document.querySelector(`${filter_options.selector} ul.filters`)

                    for (let group_name in filter_items){
                        
                        let group_node = render(this.filterListSorter.templates.group,{
                            group_name: group_name
                        })
    
                        let sorter_list = group_node.querySelector(".sorter")
    
                        filter_items[group_name].forEach( item => {
                            let list_item = render(this.filterListSorter.templates.item,{name: item})
    
                            list_item.addEventListener('click',() => {
                                this.filterListSorter.filter({dynamicList: this,filter: item,group_name: group_name})
                            })
                            sorter_list.appendChild(list_item)
                        })
    
                        filter_node.appendChild(group_node)
                    }
                }

                if (sort_raw_options){
                    let sort_options = JSON.parse(sort_raw_options.value)
                    this.listSorter = new SortList(this.brands,sort_options.fields);
                    
                    let sort_items = this.listSorter.build()
                    let sort_node = document.querySelector(`${sort_options.selector} ul.sorters`)
    
                    for (let group_name in sort_items){
        
                        let sorter_group_node = render(this.listSorter.templates.group,{
                            group_name: group_name
                        })
    
                        let sorter_list = sorter_group_node.querySelector(".sorter")
    
                        sort_items[group_name].forEach( item => {
                            let list_item = render(this.listSorter.templates.item,{name: item})
    
                            list_item.addEventListener('click',() => {
                                this.listSorter.activateSortItem(list_item,sorter_list)
    
                                this.listSorter.sort({
                                    sort_field: item,
                                    dynamicList: this,
                                    list_item: list_item,
                                    group_name: group_name
                                })
                            })
                            sorter_list.appendChild(list_item)
                        })
    
                        sort_node.appendChild(sorter_group_node)
                    }
                }

            }
        }

        // Append brand row to mobile and desktop wrappers.
        if (this.dynamiclist_node){
            let dom = render(this.template_content.innerHTML,brand.data)
            this.dynamiclist_node.appendChild(dom)
        }

        if (this.mobile_dynamiclist_node){
            let dom = render(this.mobile_template_content.innerHTML,brand.data)
            this.mobile_dynamiclist_node.appendChild(dom)
        }
    }

}

module.exports = DynamicList