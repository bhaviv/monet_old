'use strict'

/**
 * Scroll up the current document smoothly.
 */
const ScrollUp = () => {
    const currentScroll = document.documentElement.scrollTop || document.body.scrollTop
    if (currentScroll > 0) {
        window.requestAnimationFrame(ScrollUp)
        window.scrollTo(0, currentScroll - (currentScroll/5) )
    }
}

/**
 * Set the a given scroll visibility by the current scroll up value.
 * When there is no space to scroll the scroller will be hidden, and when
 * there is a space to scroll it will be visible.
 * 
 * @param {array} scrollers
 */
const SetScrollerVisibility = scrollers => {
    const currentScroll = document.documentElement.scrollTop || document.body.scrollTop
    scrollers.css('display',currentScroll > 0 ? 'block' : 'none')
}


module.exports = {ScrollUp,SetScrollerVisibility}