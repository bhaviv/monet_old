'use strict'

class Rating {
    
    constructor(rating){
        this.clicked = false;


        this.node = rating.querySelector('.tp-rating');
        this.star_nodes = this.node.querySelectorAll('li')
        this.description_node = rating.querySelector('.description')
        this.stars_description = ['Poor','Average','Good','Great','Excellet'];

        for (let i = 0;i<this.star_nodes.length;i++){
            (index => {
                this.star_nodes[i].addEventListener('click',() => {
                    this.click(index)
                    this.reset(index)
                })

                this.star_nodes[i].addEventListener('mousemove', () => {
                    if (this.clicked){
                        return;
                    }

                    this.clear()
                    this.activateStarsByIndex(index,true)
                })
            })(i)
        }

        this.node.addEventListener('mouseout',this.reset.bind(this));

        this.reset();
    }

    clear(){
        this.star_nodes.forEach( node => node.classList.remove('active'))
    }

    click(index){
        this.clicked = true;
        this.description_node.innerText = 'Thank you!'
    }

    activateStarsByIndex(index,mouse_move_action = false){
        if (mouse_move_action && !this.clicked){
            this.description_node.innerText = this.stars_description[index]
        }

        for (let i = 0;i<this.star_nodes.length;i++){
            let left = index-i;
            if (i <= index){
                if (left === 0.5){
                    this.star_nodes[i].classList.add('half')
                }else{
                    this.star_nodes[i].classList.add('active')
                }
            }
        }
    }

    reset(){
        this.clear()
        this.activateStarsByIndex(parseFloat(this.node.attributes['data-default'].value)-1)
        
        if (this.clicked){
            return;
        }

        this.description_node.innerText = ''
    }

}

module.exports = Rating;