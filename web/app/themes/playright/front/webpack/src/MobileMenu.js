'use strict'

const RegisterMobileMenuToggler = () => {
    const siteWrapper = document.getElementById('site-wrapper')
    
    document.querySelector('.toggle-nav')
            .addEventListener('click', () => siteWrapper.classList.toggle('show-nav'))
}

const RegisterMobileMenuBehavior = () => {
    document.querySelectorAll('#primary-menu li.menu-item-has-children').forEach( menu_item => {
        menu_item.addEventListener('click',function (event) {
            event.preventDefault()
            
            document.querySelectorAll("#primary-menu .active").forEach( active_menu_item => {
                if (active_menu_item != menu_item){
                    active_menu_item.classList.remove('active')
                }
            })

            this.classList.toggle('active')
        })
    })
}

module.exports = {RegisterMobileMenuToggler,RegisterMobileMenuBehavior}