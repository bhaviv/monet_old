'use strict'

import $ from "jquery"
import Rating from './Rating'
import slick from 'slick-carousel'
import DynamicList from './DynamicList'
import MobileDetect from 'mobile-detect'
import {SetScrollerVisibility,ScrollUp} from './SmoothScroll'
import {RegisterMobileMenuToggler,RegisterMobileMenuBehavior} from './MobileMenu'

/**
 * Initialize a device detector.
 */
const device = new MobileDetect(window.navigator.userAgent)

/**
 * Add device class for tablet and mobile devices.
 */
const body = $('body')

if (device.tablet()){
    body.addClass('device--tablet')
} else {
    if (device.mobile()){
        body.addClass('device--mobile')
    }
}

/**
 * Cross Site Scroller Functionality
 * The code below is an implementation of the scroll to top functionality use in every page on the site.
 * -----------------------------------------------------------------------------------------------------
 */

// Find all scrollers, all elements found will be use as triggres for the scroll to top behavior.
const scrollers = $('.smooth_scroller')

// Initialize the visibility of each scroller according to the scroller position.
SetScrollerVisibility(scrollers)

// Update scrollers visibility on every scroll event.
// Hide when there is no where to scroll to and show when there is.
window.addEventListener('scroll',() => SetScrollerVisibility(scrollers) );

// Run the scroll up function when each of the scroller is clicked.
scrollers.click(ScrollUp)


/**
 * Mobile Menu behavior functionality
 * -----------------------------------------------------------------------------------------------------
 */

RegisterMobileMenuToggler()

if (device.mobile()){
    RegisterMobileMenuBehavior()
}

/**
 * RatingJS - Stars Functionality
 * -----------------------------------------------------------------------------------------------------
 */
window.bindRating = () => {
    const ratings = document.querySelectorAll('.rating-instance')
    ratings.forEach( rating => new Rating(rating) )
}

/**
 * When brands collection prop is set, it means we have at least one dynamic list to render
 * The code below will loop each brand collection and render it to the page.
 * You can find more information about this process on the DynamicListResolver class.
 */
if (PR.brands_collection){
    PR.brands_collection.forEach( brands => {
        let dl = new DynamicList(brands)
        dl.on('afterRender', window.bindRating)
        dl.render()
    })
}

// Init slick slider
$('.slick-carousel-instance').slick()

/**
 * Academy likes functionality.
 */
$(".posts-likes-wrapper .icon-article-like").click(function (){
    var postId = $(this).parents(".posts-likes-wrapper").data("post_id");
    var key = 'liked_' + postId;    
    if (localStorage.getItem(key)){
        return;
    }

    var count = $(this).parents(".posts-likes-wrapper").find(".count");
    count.text(parseInt(count.text())+1);


    $.get(window.PR.admin_ajax_url,{action: 'like', post_id: postId }, function (){
        localStorage.setItem(key,'1')
    });
})
