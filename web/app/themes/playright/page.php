<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package playright
 */

get_header(); ?>

<div class="container">
	<?php if ( have_posts() ) while ( have_posts() )  the_post(); ?>
	<h1 class="text-center"><?php the_field('h1_title'); ?></h1>
	<div>
		<?php the_content(); ?>
	</div>
</div>

<?php
get_footer();
