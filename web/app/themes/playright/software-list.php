<?php
/**
 * Template Name: Software list template
 */

get_header();
if ( have_posts() ) while ( have_posts() )  the_post();
?>
<div class="container reviews">
    <!--  Title-->
    <div class="row">
        <div class="col-md-12 text-center">
            <h1 class="text-capitalize text-center"><?php the_field('h1_title'); ?></h1>
            <?php the_content(); ?>
        </div>
    </div>
    <!--End Title-->

    <div class="row">
        <div class="col-md-12">
            <div class="software-list-table">
                <div class="row software-list-item">
                    <div class="col-md-6 col-sm-12">
                        <div class="cell logo col-sm-4 col-xs-12">
                            <div class="logo-container">
                                <img src="https://playright.co.uk/top-assets/sites/5/images/Software-providers/Nav-logo/microgaming-casino.jpg" alt="Microgaming" class="img-responsive">
                            </div>
                        </div>
                        <div class="cell about">
                            <h2>Microgaming</h2>
                            <div class="excerpt">
                                <p><span>Microgaming is the big beast of the online casino scene. The total number of games released by Microgaming down the years already exceeds 850, and the company is launching new products at a hectic pace.&nbsp;</span></p>
                            </div>
                            <a href="/casino/microgaming" title="Read Full Review" class="read-more">Read Full Review</a>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="slick-carousel-instance"
                             data-slick='{"nextArrow":"<i class=\"icon-arrow next-slide\"></i>","prevArrow":"<i class=\"icon-arrow previous-slide\"></i>","slidesToScroll": 3, "slidesToShow":3, "responsive":[{"breakpoint": 1200, "settings":{"slidesToShow":2, "slidesToScroll":2}},{"breakpoint": 992, "settings":{"slidesToShow":3, "slidesToScroll":3}},{"breakpoint": 768, "settings":{"slidesToShow":1, "slidesToScroll":1}}]}'>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row software-list-item">
                    <div class="col-md-6 col-sm-12">
                        <div class="cell logo col-sm-4 col-xs-12">
                            <div class="logo-container">
                                <img src="https://playright.co.uk/top-assets/sites/5/images/Software-providers/Nav-logo/microgaming-casino.jpg" alt="Microgaming" class="img-responsive">
                            </div>
                        </div>
                        <div class="cell about">
                            <h2>Microgaming</h2>
                            <div class="excerpt">
                                <p><span>Microgaming is the big beast of the online casino scene. The total number of games released by Microgaming down the years already exceeds 850, and the company is launching new products at a hectic pace.&nbsp;</span></p>
                            </div>
                            <a href="/casino/microgaming" title="Read Full Review" class="read-more">Read Full Review</a>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="slick-carousel-instance"
                             data-slick='{"nextArrow":"<i class=\"icon-arrow next-slide\"></i>","prevArrow":"<i class=\"icon-arrow previous-slide\"></i>","slidesToScroll": 3, "slidesToShow":3, "responsive":[{"breakpoint": 1200, "settings":{"slidesToShow":2, "slidesToScroll":2}},{"breakpoint": 992, "settings":{"slidesToShow":3, "slidesToScroll":3}},{"breakpoint": 768, "settings":{"slidesToShow":1, "slidesToScroll":1}}]}'>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                            <div class="slide-element text-center">
                                <div class="slider-image">
                                    <div class="text-center">
                                        <img src="https://playright.co.uk/top-assets/sites/5/images/Casino/Casino-Retina-Mobile/32Red-MobileLogo-480x165.png" alt="32 Red Casino" class="slide-logo">
                                    </div>
                                </div>
                                <a href="/casino/32red" title="READ REVIEW" tabindex="0" class="default-button text-center">Read Review</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();