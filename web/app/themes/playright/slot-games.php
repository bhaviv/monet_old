<?php
/**
 * Template Name: Online Slot Games
 */

get_header();
if ( have_posts() ) while ( have_posts() )  the_post();
?>

<div class="background-wrapper" style="background:url(http://s3-eu-west-1.amazonaws.com/prwp/app/uploads/20180102092219/slots-navigational-background.jpg) top center no-repeat;">
    <div class="container online-slot-game">
        <!---- Title --->
        <div class="row">
            <div class="col-md-12">
                <h2 class="review-title"><?php the_field('h1_title'); ?></h2>
                <?php the_content(); ?>
            </div>
        </div>
        <!----End Title --->

        <!----Cards --->
        <div class="row card">
            <div class="col-sm-4 col-md-3 col-xs-12">
                <div class="flip-container">
                    <div class="flipper">
                        <div class="front">
                            <img src="https://playright.co.uk/top-assets/sites/5/images/slot-reviews/cleopatra/cleopatra.jpg" alt="brand logo" class="img-responsive">
                        </div>
                        <div class="back" style="background: url(https://playright.co.uk/top-assets/sites/5/images/slot-reviews/cleopatra/cleopatra.jpg)">
                            <div class="slot-description">
                                <div class="slot-description-row">
                                    <div class="text-uppercase">SLOT NAME:</div>
                                    <div class="text-uppercase">Cleopatra</div>
                                </div>
                                <div class="slot-description-row">
                                    <div class="text-uppercase">LAUNCHED:</div>
                                    <div class="text-uppercase">2006</div>
                                </div>
                                <div class="slot-description-row">
                                    <div class="text-uppercase">DEVELOPER:</div>
                                    <div class="text-uppercase">igt</div>
                                </div>
                                <div class="slot-description-row text-uppercase">
                                    <div>SLOT TYPE:</div>
                                    <div>Video</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slot-title text-capitalize text-center"> Cleopatra </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <a title="Full review" href="#"><button class="default-button" role="link">FULL REVIEW</button></a>
                    </div>
                </div>

            </div>
            <!----End Cards --->
        </div>
    </div>
</div>
<?php
get_footer();