<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package playright
 */

get_header(); ?>

<div class="container">
	<?php if ( have_posts() ) while ( have_posts() )  the_post(); ?>
	<h1 class="text-center"><?php the_title(); ?></h1>
	<div>
		<?php the_content(); ?>
	</div>
</div>

<?php
get_footer();