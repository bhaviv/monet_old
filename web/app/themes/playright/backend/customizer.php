<?php

if (!isset($manager) || !isset($unicorn)){
    die('Missing manager or unicorn vars.');
}

$all_bonus_types = [];

foreach($GLOBALS['BONUS_TYPES'] as $post_type => $bonus_type_data){
	$all_bonus_types = array_merge($all_bonus_types,$bonus_type_data['fields']);
}

$desktop_outlinks_types = [];

foreach($GLOBALS['DESKTOP_OUTLINK_TYPES'] as $post_type => $desktop_outlink_type_data){
	$desktop_outlinks_types = array_merge($desktop_outlinks_types,$desktop_outlink_type_data['fields']);
}

$mobile_outlinks_types = [];

foreach($GLOBALS['MOBILE_OUTLINK_TYPES'] as $post_type => $mobile_outlink_type_data){
	$mobile_outlinks_types = array_merge($mobile_outlinks_types,$mobile_outlink_type_data['fields']);
}

/**
 * Cross-Banner Settings 
 */
$manager->registerCustomizer(
	'Cross Banner',
	'From here you can manage cross banner order and links.',
	[
		[
            'label' => 'Unicorn List',
            'settings' => 'cross_banner_unicorn_list',
            'type' => 'select',
            'choices' => $unicorn->getLists()
        ],
        [
            'label' => 'Bonus Type',
            'settings' => 'cross_banner_bonus_type',
            'type' => 'select',
            'choices' => $all_bonus_types
        ],
        [
            'label' => 'Desktop OutLink Type',
            'settings' => 'cross_banner_outlink_type',
            'type' => 'select',
            'choices' => $desktop_outlinks_types
		],
		[
            'label' => 'Mobile OutLink Type',
            'settings' => 'cross_banner_mobile_outlink_type',
            'type' => 'select',
            'choices' => $mobile_outlinks_types
		],
		[
            'label' => 'See More Link',
            'settings' => 'cross_banner_see_more_link',
            'type' => 'text'
        ]
	]
);

/**
 * Register footer's news letter customizer.
 */
$manager->registerCustomizer(
	'News-Latter Form',
	'From here you can manage the current site newsletter form.',
	[
		[
			'label'    => 'Title',
			'settings' => 'newsletter_form_title',
			'type' => 'text'
		],
		[
			'label'    => 'Description',
			'settings' => 'newsletter_form_description',
			'type' => 'textarea'
		],
		[
			'label'    => 'Terms & Conditions Link',
			'settings' => 'newsletter_form_terms_link',
			'type' => 'text'
		]
	]
);

/***
 * Register the Academy top section texts customizer.
 */
$manager->registerCustomizer(
	'Academy',
	'From here you can edit the academy arcive page top content.',
	[
		[
			'label'    => 'Title',
			'settings' => 'academy_title',
			'type' => 'text'
		],
		[
			'label'    => 'Content',
			'settings' => 'academy_content',
			'type' => 'textarea'
		],
		[
			'label'    => 'Small letters',
			'settings' => 'academy_small_letters',
			'type' => 'text'
		],
		[
            'label' => 'Unicorn List',
            'settings' => 'side_dynamiclist_unicorn_list',
            'type' => 'select',
            'choices' => $unicorn->getLists()
        ],
        [
            'label' => 'Bonus Type',
            'settings' => 'side_dynamiclist_bonus_type',
            'type' => 'select',
            'choices' => $GLOBALS['BONUS_TYPES']
        ],
        [
            'label' => 'Desktop OutLink Type',
            'settings' => 'side_dynamiclist_outlink_type',
            'type' => 'select',
            'choices' => $GLOBALS['DESKTOP_OUTLINK_TYPES']
		],
        [
            'label' => 'Mobile OutLink Type',
            'settings' => 'side_dynamiclist_outlink_type',
            'type' => 'select',
            'choices' => $GLOBALS['MOBILE_OUTLINK_TYPES']
		]
	]
);

/**
 * Register Advertiser Disclosure customizer
 */
$manager->registerCustomizer(
	'Advertiser Disclosure',
	'From here you can manage the Advertiser Disclosure content showed over every dynamic list.',
	[
		[
			'label' => 'Advertiser Disclosure Text',
			'settings'       => 'advertiser_disclosure',
			'type'           => 'textarea'
		]
	]
);

/**
 * Register footer's content customizer, for logos and disclaimers.
 */
$manager->registerCustomizer(
	'Site Footer',
	'From here you can manage the current site footer texts and disclaimers.',
	[
		[
			'label'          => 'Disclaimer',
			'settings'       => 'footer_disclaimer',
			'type'           => 'textarea'
		],
		[
			'label'          => 'Additional Disclaimer',
			'settings'       => 'footer_additional_disclaimer',
			'type'           => 'textarea'
		],
		[
			'label'          => 'Footer Links Title',
			'settings'       => 'footer_links_title',
			'type'           => 'text'
		],
		[
			'label'          => 'Footer Contact Title',
			'settings'       => 'footer_contact_title',
			'type'           => 'text'
		],
		[
			'label'          => 'Footer Contact Description',
			'settings'       => 'footer_contact_description',
			'type'           => 'textarea'
		]
	]
);

/**
 * Register betting's Sports Markets.
 */

 $sports_markets_settings = [];

foreach($GLOBALS['SPORTS_MARKETS'] as $sports_key => $sports_value){

	$setting = [
		'label' => $sports_value,
		'settings' => 'sports_markets_'.$sports_value,
		'type' => 'checkbox',
	];

	$sports_markets_settings[] = $setting;	
}

$manager->registerCustomizer(
	'Sports Markets Available',
	'From here you can manage the sports markets available',
	$sports_markets_settings
);

