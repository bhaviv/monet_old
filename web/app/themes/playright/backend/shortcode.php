<?php
/**
 * Register a new shortcode for date formats.
 * 
 * @example use [date format='F M'] anywhere in site, will be converted into "December 2017".
 */
$manager->registerShortcode('date', function ($attributes) {
	if (isset($attributes['format'])){
		try{
			return date($attributes['format']);
		}catch(\Exception $ex){
			return '<< Invalid date format >>';
		}
	}

	return '<< Date format was not defined >>';
});


/**
 * [Table id=123]
 */
$manager->registerShortcode('Table', function ($attributes) {
  if (!isset($attributes['id'])){
    return 'Must define table id';
  }

  $table_id = $attributes['id'];
  $table_name = get_post_field('post_title', $table_id);
  $columns_types = get_field('table_columns_types', $table_id);
  $columns_headers = get_field('table_columns_headers', $table_id);
  $columns = get_field('table_columns', $table_id);
  $class = get_field('table_class', $table_id);
  $css = get_field('table_css', $table_id);

  ob_start();
  include THEME_DIR . '/reuse/table.php';
  return ob_get_clean();
});



/**
 * Register a new shortcode for the current month.
 * 
 * @example [current_month] -> December
 */
$manager->registerShortcode('current_month', function ($attributes) {
	return date('F');
});

/**
 * Register a new shortcode for the current year.
 * 
 * @example [current_year] -> 2017
 */
$manager->registerShortcode('current_year', function ($attributes) {
	return date('Y');
});

