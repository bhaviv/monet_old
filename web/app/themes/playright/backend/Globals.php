<?php

/**
 * A static list of the current site content types.
 * This list will be used to generate the dropdown for dynamic list pages categorization.
 * 
 * @ver array
 */
$GLOBALS['POST_TYPES'] = [
	'bingo' => 'Bingo',
	'betting' => 'Betting',
	'casino' => 'Casino',
	'slots' => 'Slots',
	'software' => 'Software'
];

/**
 * A static list of all possible bonus types.
 * This list will be used to auto generate settings in the brand edit page
 * We will also use this settings to generate dropdown options for pages of dynamic list.
 * 
 * @ver array
 */
$GLOBALS['BONUS_TYPES'] = [
	'casino' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'casino',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Default',
			'free_spins'  => 'Free Spins',
			'ireland' 	  => 'Ireland',
			'live_casino' => 'Live Casino',
			'slots' => 'Slots'
		]
	],
	'betting' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'betting',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Default',
		]
	],
	'bingo' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'bingo',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Default',
		]
	]
];

/**
 * A static list of all possible desktop outlink types.
 * This list will be used to auto generate settings in the brand edit page
 * We will also use this settings to generate dropdown options for pages of dynamic list.
 * 
 * @ver array
 */
$GLOBALS['DESKTOP_OUTLINK_TYPES'] = [
    'casino' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'casino',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Desktop Default',
			'live_casino'  => 'Live Casino',
			'free_spins' => 'Free Spins',
			'ireland_casino' => 'Ireland Casino',
			'slots' => 'Slots',
			'crossbanner' => 'Crossbanner'
		]
	],
    'betting' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'betting',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Desktop Default',
			'ireland_casino' => 'Ireland'
		]
	],
	'bingo' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'bingo',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Desktop Default'
		]
	]
]; 

/**
 * A static list of all possible mobile outlink types.
 * This list will be used to auto generate settings in the brand edit page
 * We will also use this settings to generate dropdown options for pages of dynamic list.
 * 
 * @ver array
 */
$GLOBALS['MOBILE_OUTLINK_TYPES'] = [
    'casino' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'casino',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Mobile Default',
			'slots' => 'Slots'
		]
	],
	'betting' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'betting',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Mobile Default'
		]
	],
	'bingo' => [
		'location' => [[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'bingo',
				'order_no' => 0,
				'group_no' => 2,
			]
		]], // where to show
		'fields' => [
			'default' => 'Mobile Default'
		]
	]
]; 

$GLOBALS['unicorn'] = $unicorn = new \Unicorn\Client();

$GLOBALS['RATINGS'] = [
  'casino' => [
    'design_and_usability' => 'DESGIN & USABILITY',
    'game_variety' => 'GAME VARIETY',
    'cashier' => 'CASHIER',
    'customer_support' => 'CUSTOMER SUPPORT',
    'bonus_quality' => 'BONUS QUALITY',
    'overall_score' => 'OVERALL SCORE',
  ],
  'betting' => [
    'overall' => 'OVERALL',
    'customer_support' => 'CUSTOMER SUPPORT',
    'design_and_usability' => 'DESIGN & USABILITY',
    'bonuses_and_free_bets' => 'BONUSES & FREE BETS',
    'betting_markets_and_odds' => 'BETTING MARKETS & ODDS',
    'sports_coverage' => 'SPORTS COVERAGE',
  ],
  'bingo' => [
    'overall' => 'OVERALL',
    'bonuses_and_promotions' => 'BONUSES & PROMOTIONS',
    'game_variety' => 'GAME VARIETY',
    'deposits_and_withdrawals' => 'DEPOSITS & WITHDRAWALS',
    'customer_support' => 'CUSTOMER SUPPORT',
    'design_and_usability' => 'DESGIN & USABILITY',
  ],
  'slots' => [
    'graphics' => 'GRAPHICS',
    'gameplay' => 'GAMEPLAY',
    'special_features' => 'SPECIAL FEATURES',
  ],
];

/**
 * A static list of possible sports markets types
 * This list will be used to generate a dropdown options for betting reviews
 * 
 * @ver array
 */
$GLOBALS['SPORTS_MARKETS'] = [
	'football' => 'Football',
	'horse_racing' => 'Horse Racing',
	'tennis' => 'Tennis',
	'formula_1' => 'Formula 1',
	'rugby' => 'Rugby',
	'cricket' => 'Cricket',
	'golf' => 'Golf',
	'darts' => 'Darts',
	'boxing' => 'Boxing',
	'athletics' => 'Athletics',
	'american_football' => 'American Football',
	'aussie_rules' => 'Aussie Rules',
	'mma_(ufc)' => 'MMA (UFC)',
	'baseball' => 'Baseball',
	'greyhound_racing' => 'Greyhound Racing',
	'cycling' => 'Cycling',
	'gaelic_games' => 'Gaelic Games',
	'handball' => 'Handball',
	'hockey' => 'Hockey',
	'snooker' => 'Snooker',
	'rowing' => 'Rowing',
	'sailing' => 'Sailing',
	'table_tennis' => 'Table Tennis',
	'show_jumping' => 'Show Jumping',
	'ice_hockey' => 'Ice Hockey',
	'winter_sports' => 'Winter Sports',
	'badminton' => 'Badminton',
	'basketball' => 'Basketball',
	'bowls' => 'Bowls',
	'netball' => 'Netball',
	'volleyball' => 'Volleyball',
	'esports' => 'eSports',
	'financials' => 'Financials'
];
