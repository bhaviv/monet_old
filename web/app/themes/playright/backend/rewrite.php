<?php
function __filter_rewrite_rules( $rules )
{
    $_rules = array();
    foreach ( $rules as $rule => $rewrite )
    $_rules[ str_replace( 'attachment/', '/', $rule  ) ] = $rewrite;
    return $_rules;
}
add_filter( 'rewrite_rules_array', '__filter_rewrite_rules' );

function __filter_attachment_link( $link )
{
    return preg_replace( '#attachment/(.+)$#', '/$1', $link );
}
add_filter( 'attachment_link', '__filter_attachment_link' );
