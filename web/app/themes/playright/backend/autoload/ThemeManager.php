<?php

/***
 * A quick wrapper for the use of WordPress functions
 * You can check out how we use this class at the current theme functions.php file.
 * 
 * @author Ron Masas
 */

namespace AutoLoad;

use WP_Customize_Control;
use WP_Customize_Image_Control;

class ThemeManager {

    /**
     * @ver array
     */
    protected $post_types = [];

    /**
     * @ver array
     */
    protected $widget_areas = [];

    /**
     * @ver array
     */
    protected $customizers = [];

    /**
     * Register widgets, posts types and customizer settings.
     * 
     * @return void
     */
    public function run(){
        add_action('init', [$this, 'init']);
        add_action('widgets_init', [$this, 'buildWidgets']);
        add_action('admin_menu', [$this, 'registerPageCategoriesMenu']);
        add_action('customize_register', [$this, 'buildCustomizer']);
    }

    /**
     * Register a post type.
     * 
     * @param array $args
     * @param string $suffix
     * @todo maybe extract this logic to it's own class.
     * 
     * @return ThemeManager
     */
    public function registerPostType($args,$suffix = 's'){
        
        $name = $args['name'];

        $labels = array(
            'name'                  => $name . $suffix,
            'singular_name'         => $name . $suffix,
            'menu_name'             => $name . $suffix,
            'name_admin_bar'        => $name,
            'archives'              => $name . ' Archives',
            'attributes'            => $name .' Attributes',
            'parent_item_colon'     => 'Parent '. $name,
            'all_items'             => 'All ' . $name . $suffix,
            'add_new_item'          => 'Add New ' . $name,
            'add_new'               => 'Add New',
            'new_item'              => 'New ' . $name,
            'edit_item'             => 'Edit ' . $name,
            'update_item'           => 'Update ' . $name,
            'view_item'             => 'View ' . $name,
            'view_items'            => 'View ' . $name . $suffix,
            'search_items'          => 'Search ' . $name,
            'not_found'             => 'Not found',
            'not_found_in_trash'    => 'Not found in Trash',
            'featured_image'        => 'Featured Image',
            'set_featured_image'    => 'Set featured image',
            'remove_featured_image' => 'Remove featured image',
            'use_featured_image'    => 'Use as featured image',
            'insert_into_item'      => 'Insert into ' . $name,
            'uploaded_to_this_item' => 'Uploaded to this ' . $name,
            'items_list'            => $name . $suffix . ' list',
            'items_list_navigation' => $name . $suffix . ' list navigation',
            'filter_items_list'     => 'Filter '.$name.$suffix.' list',
        );

        $post_type_args = array(
            'label'                 => $name,
            'description'           => $name,
            'labels'                => $labels,
            'supports'              => ['title', 'editor', 'thumbnail', 'revisions'],
            'taxonomies'            => [],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
        );

        if(isset($args['extra_args'])) {
          $post_type_args = array_merge($post_type_args, $args['extra_args']);
        }

        foreach($args as $key => $value){
            if (isset($post_type_args[$key])){
                $post_type_args[$key] = $value;
            }
        }

        $slug = isset($args['slug']) ? $args['slug'] : sanitize_title($name);
        
        $this->post_types[] = [
            'slug' => $slug,
            'args' => $post_type_args,
            'perma_structure_prefix' => $args['perma_structure_prefix'] ?? null,
        ];

        return $this;
    }


    public function rewritePostTypeUrls($slug, $permaStructurePrefix){
        global $wp_rewrite;
        $brand_structure = $permaStructurePrefix . "/%$slug%";
        $wp_rewrite->add_rewrite_tag("%$slug%", '([^/]+)', "$slug=");
        $wp_rewrite->add_permastruct($slug, $brand_structure, false);
    }

    public function registerPageCategoriesMenu () {
        $pageCategories = [
          'INFO' => 569,
          'Payment options' => 1,
          'PPC Bingo' => 568,
          'PPC Casino' => 567,
          'PPC Sport' => 564,
          'SEO Bingo' => 565,
          'SEO Casino' => 563,
          'SEO Sport' => 564,
          'SEO Slots' => 701,
        ];

        foreach ($pageCategories as $categoryName => $categoryId) {
           $permalink = "edit.php?s&post_status=all&post_type=page&action=-1&m=0&cat=$categoryId&filter_action=Filter&paged=1&action2=-1";
           add_pages_page($categoryName, $categoryName, 'edit_posts', $permalink);             
        }
    }

    /**
     * Register a widget area.
     * 
     * @param array $args
     * 
     * @return ThemeManager
     */
    public function registerWidgetArea(array $args){
        $this->widget_areas[] = $args;

        return $this;
    }

    /**
     * An alias for the add_action function.
     * 
     * @param string $name
     * @param callable $fn
     * 
     * @return ThemeManager
     */
    public function action($name,$fn){
        add_action($name,function () use($fn){
            $fn();
        });

        return $this;
    }

    public function init () {
      $this->registerTaxonomy();
      $this->createPostTypes();
    }


    public function registerTaxonomy () {
      /**
       * Register a new taxonomy for the academy post type.
       * Taxonomy is a type of "label" we can append to posts, in this case we use it to create "Categories" functionality.
       */
      register_taxonomy("academy_categories", ["academy"], [
        "rewrite" => true,
        "hierarchical" => true,
        "label" => "Academy Categories",
        "singular_label" => "Academy Category"
      ]);
    }

    public function createPostTypes() {
      foreach($this->post_types as $post_type){
          register_post_type($post_type['slug'],$post_type['args']);
          if ($post_type['perma_structure_prefix']) {
            $this->rewritePostTypeUrls($post_type['slug'], $post_type['perma_structure_prefix']);
          }
      }           
    }


    /**
     * Register a shortcode.
     * 
     * @param string $name
     * @param callable $fn
     * 
     * @return ThemeManager
     */
    public function registerShortcode($name,$fn){
        add_shortcode($name,$fn);
        
        return $this;
    }

    public function buildCustomizer ($wp_customize) {
      foreach($this->customizers as $customizer){

        $wp_customize->add_section($customizer['section_slug'], [
          'priority'    => 30,
          'title'       => $customizer['section'],
          'description' => $customizer['section_description'],
        ]);

        foreach($customizer['controls'] as $control){

          $wp_customize->add_setting($control['settings']);

          switch($control['type']){
          case 'image':
            $wp_customize->add_control(new WP_Customize_Image_Control(
              $wp_customize,
              $control['settings'],
              $control
            ));
            break;
          default:
            $wp_customize->add_control(new WP_Customize_Control(
              $wp_customize,
              $control['settings'],
              $control
            ));
            break;
          }
        }
      }
    }

    public function buildWidgets () {
      foreach($this->widget_areas as $widget_area){
        register_sidebar($widget_area);
      }
    }


    /**
     * A quick way to unregister emoji related scripts.
     * 
     * @return boolean
     */
    public function remove_emoji_scripts(){
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
        remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
        remove_action( 'admin_print_styles', 'print_emoji_styles' );

        return true;
    }

    /***
     * Register a new customizer settings.
     * 
     * @param string  $section_name
     * @param string  $section_description
     * @param array   $controls
     * 
     * @return ThemeManager
     */
    public function registerCustomizer($section_name,$section_description,array $controls){
        $slug = str_replace('-','_',sanitize_title($section_name));

        $this->customizers[] = [
            'section' => $section_name,
            'section_slug' => $slug,
            'section_description' => $section_description,
            'controls' => array_map(function ($control) use ($slug) {
                $control['section'] = $slug;
                return $control;
            },$controls)
        ];
        
        return $this;
    }

    /**
     * Bind multiple actions to one callback.
     * 
     * @param array $actions
     * @param callable $fn
     * 
     * @return ThemeManager
     */
    public function add_to_actions($actions,$fn){
        foreach($actions as $action){
            add_action($action,$fn);
        }

        return $this;
    }

    /**
     * Register a new no priv ajax action
     * 
     * @param string $action
     * @param callable $fn
     * 
     * @return ThemeManager
     */
    public function ajax($action,$fn){
        $callback = function () use ($fn) {
            $result = $fn();

            if (!is_array($result)){
                $result = ['error' => 'Invalid result','raw' => $result];
            }

            die(wp_send_json($result));
        };

        $this->add_to_actions([
            'wp_ajax_nopriv_' . $action,
            'wp_ajax_' . $action
        ],$callback);

        return $this;
    }
}
