<?php

if (!function_exists('register_field_group')){
    die;
}

$only_dynamic_list_templates = [
  [
    [
      'param' => 'page_template',
      'operator' => '==',
      'value' => 'dynamiclist-template.php',
      'order_no' => 0,
      'group_no' => 0,
    ]
  ],
  [
    [
      'param' => 'page_template',
      'operator' => '==',
      'value' => 'dynamiclist-template-casino.php',
      'order_no' => 0,
      'group_no' => 0,
    ]
  ],
    [
        [
            'param' => 'page_template',
            'operator' => '==',
            'value' => 'payment-template.php',
            'order_no' => 0,
            'group_no' => 0,
        ]
    ],
  [
    [
      'param' => 'page_template',
      'operator' => '==',
      'value' => 'dynamiclist-template-bingo.php',
      'order_no' => 0,
      'group_no' => 0,
    ]
  ]
];


$only_home_page_template = [
    [
        [
            'param' => 'page_template',
            'operator' => '==',
            'value' => 'home-template.php',
            'order_no' => 0,
            'group_no' => 0,
        ]
    ]
];

$only_brands = [
    [
        [
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'betting',
            'order_no' => 0,
            'group_no' => 0,
        ]
    ],
    [
        [
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'bingo',
            'order_no' => 0,
            'group_no' => 1,
        ]
    ],
    [
        [
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'casino',
            'order_no' => 0,
            'group_no' => 2,
        ]
    ],
    [
        [
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'slots',
            'order_no' => 0,
            'group_no' => 3,
        ]
    ]
];

foreach($GLOBALS['DESKTOP_OUTLINK_TYPES'] as $post_type => $post_type_data){

    $desktop_outlink_types = [];

    foreach($post_type_data['fields'] as $key => $value){
        $field = [
            'key' => 'outlink_desktop_field_' . $key,
            'label' => $value,
            'name' => 'outlink_desktop_' . $key,
            'type' => 'text',
            'default_value' => '',
            'placeholder' => 'Leave empty to use default',
            'prepend' => '',
            'append' => '',
            'formatting' => 'none',
            'maxlength' => '',
        ];
    
        if ($key === 'default'){
            $field['placeholder'] = '';
            $field['required'] = 1;
        }

        $desktop_outlink_types[] = $field;
    }

    /**
     * Add Dynamic OutLinks metabox on every brand post.
     */
    register_field_group([
        'id' => 'dynamic_'.$post_type.'_outlinks',
        'title' => 'Out Links',
        'fields' => $desktop_outlink_types,
        'location' => $post_type_data['location'],
        'options' => [
            'position' => 'side',
            'layout' => 'default',
            'hide_on_screen' => [],
        ],
        'menu_order' => 0
    ]);
}

foreach($GLOBALS['MOBILE_OUTLINK_TYPES'] as $post_type => $post_type_data){

    $mobile_outlink_types = [];

    foreach($post_type_data['fields'] as $key => $value){
        $field = [
            'key' => 'outlink_mobile_field_' . $key,
            'label' => $value,
            'name' => 'outlink_mobile_' . $key,
            'type' => 'text',
            'default_value' => '',
            'placeholder' => 'Leave empty to use default',
            'prepend' => '',
            'append' => '',
            'formatting' => 'none',
            'maxlength' => '',
        ];
    
        if ($key === 'default'){
            $field['placeholder'] = '';
            $field['required'] = 1;
        }

        $mobile_outlink_types[] = $field;
    }

    /**
     * Add Dynamic OutLinks metabox on every brand post.
     */
    register_field_group([
        'id' => 'dynamic_mobile_'.$post_type.'_outlinks',
        'title' => 'Mobile Out Links',
        'fields' => $mobile_outlink_types,
        'location' => $post_type_data['location'],
        'options' => [
            'position' => 'side',
            'layout' => 'default',
            'hide_on_screen' => [],
        ],
        'menu_order' => 0
    ]);
}

foreach($GLOBALS['BONUS_TYPES'] as $post_type => $post_type_data){
    $bonus_types = [];
    
    foreach($post_type_data['fields'] as $key => $value){
        $field = [
            'key' => 'bonus_field_' . $key,
            'label' => $value,
            'name' => 'bonus_' . $key,
            'type' => 'wysiwyg',
            'default_value' => '',
            'toolbar' => 'full',
            'media_upload' => 'yes',
        ];
    
        if ($key === 'default'){
            $field['required'] = 1;
        }
    
        $bonus_types[] = $field;
    }
    
    /**
     * Add Dynamic Bonus metabox on every brand post.
     */
    register_field_group([
        'id' => 'dynamic_'.$post_type.'_bonus',
        'title' => 'Bonus',
        'fields' => $bonus_types,
        'location' => $post_type_data['location'],
        'options' => [
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => [],
        ],
        'menu_order' => 0
    ]);
}

/**
 * @ver array
 */
$unicorn_list = $GLOBALS['unicorn']->getLists();

$dynamic_list_relation_fields = [
    [
        'key' => 'dr_primary_dynamic_list',
        'label' => 'Primary Order',
        'name' => 'dr_primary_order',
        'type' => 'select',
        'choices' => $unicorn_list
    ],
    [
        'key' => 'dr_content_type_field',
        'label' => 'Content Type',
        'name' => 'dr_content_type',
        'type' => 'select',
        'choices' => $GLOBALS['POST_TYPES']
    ]
];

$postId = isset($_GET['post']) ? $_GET['post']: (isset($_POST['post_ID']) ? $_POST['post_ID']: null);

if ($postId){

    // Get the selected content type by the current post id.
    $currently_selected_content_type = get_field('dr_content_type', $postId);

    // Check settings exists on functions.php file
    if (isset($GLOBALS['BONUS_TYPES'][$currently_selected_content_type])){
        $dynamic_list_relation_fields[] = [
            'key' => 'dr_bonus_type',
            'label' => 'Bonus Type',
            'name' => 'dr_bonus_type',
            'type' => 'select',
            'choices' => $GLOBALS['BONUS_TYPES'][$currently_selected_content_type]['fields'],
            'default_value' => 'default'
        ];
    }

    if (isset($GLOBALS['DESKTOP_OUTLINK_TYPES'][$currently_selected_content_type])){
        
        $dynamic_list_relation_fields[] = [
            'key' => 'dr_desktop_outlink_type',
            'label' => 'Desktop OutLink Type',
            'name' => 'dr_desktop_outlink_type',
            'type' => 'select',
            'choices' => $GLOBALS['DESKTOP_OUTLINK_TYPES'][$currently_selected_content_type]['fields'],
            'default_value' => 'default'
        ];
    }

    if (isset($GLOBALS['MOBILE_OUTLINK_TYPES'][$currently_selected_content_type])){
         
        $dynamic_list_relation_fields[] = [
            'key' => 'dr_mobile_outlink_type',
            'label' => 'Mobile OutLink Type',
            'name' => 'dr_mobile_outlink_type',
            'type' => 'select',
            'choices' => $GLOBALS['MOBILE_OUTLINK_TYPES'][$currently_selected_content_type]['fields'],
            'default_value' => 'default'
        ];
    }
}

/**
 * Add Dynamic Selection of the bonus and outlink on every dynamic list page. 
 */
register_field_group([
    'id' => 'dynamic_relation',
    'title' => 'Dynamic Relation',
    'fields' => $dynamic_list_relation_fields,
    'location' => $only_dynamic_list_templates,
    'options' => [
        'position' => 'side',
        'layout' => 'default',
        'hide_on_screen' => [],
    ],
    'menu_order' => 0
]);

/**
 * Add Rating fields
 */

$rating_fields_for_all_brands = [
  [
    'key' => 'default_dynamic_list_stars',
    'label' => 'Default Dynamic List Stars',
    'name' => 'default_dynamic_list_stars',
    'type' => 'number',
    'instructions' => 'Choose a default value for when the Unicorn list type is "Scoring by review".',
    'required' => 1,
    'default_value' => 4,
    'placeholder' => '',
    'prepend' => '',
    'append' => '',
    'min' => '',
    'max' => '',
    'step' => '',
  ],
  [
    'key' => 'default_dynamic_list_score',
    'label' => 'Default Dynamic List Score',
    'name' => 'default_dynamic_list_score',
    'type' => 'number',
    'instructions' => 'Choose a default value for when the Unicorn list type is "Scoring by review".',
    'required' => 1,
    'default_value' => '7.5',
    'placeholder' => '',
    'prepend' => '',
    'append' => '',
    'min' => '',
    'max' => '',
    'step' => '',
  ],
  [
    'key' => 'default_dynamic_list_score_text',
    'label' => 'Default Dynamic List Score Text',
    'name' => 'default_dynamic_list_score_text',
    'type' => 'text',
    'instructions' => 'Choose a default value for when the Unicorn list type is "Scoring by review".',
    'required' => 1,
    'default_value' => '',
    'placeholder' => '',
    'prepend' => '',
    'append' => '',
    'formatting' => 'none',
    'maxlength' => '',
  ],
];


foreach ($GLOBALS['RATINGS'] as $brand => $fields) {

  $location = [[[
    'param' => 'post_type',
    'operator' => '==',
    'value' => $brand,
    'order_no' => 0,
    'group_no' => 2,
  ]]];

  $rating_fields = [];

  foreach ($fields as $field_name => $field_label) {
    $rating_fields[] = [
      'key' => 'field_rating_'. $field_name,
      'label' => $field_label,
      'name' => 'rating_'.$field_name,
      'type' => 'number',
      'required' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
    ];
  }

  $rating_fields = array_merge($rating_fields_for_all_brands, $rating_fields);

  register_field_group([
    'id' => 'acf_rating_'. $brand,
    'title' => 'Rating',
    'fields' => $rating_fields,
    'location' => $location,
    'options' => [
      'position' => 'side',
      'layout' => 'default',
      'hide_on_screen' => [],
    ],
    'options' => [
      'position' => 'side',
      'layout' => 'default',
      'hide_on_screen' => [
      ],
    ],
    'menu_order' => 0
  ]);
}
