<?php
/**
 * This file will hold some small functions that will be used by the template.
 */


if(!isset($GLOBALS['_js'])){
	$GLOBALS['_js'] = [];
}


function make_slots_list($values){
    $items = '';
    $template = '<div class="side-features-list">%s</div>';
    $item_template = '<div class="item">%s: <strong>%s</strong></div>';

    foreach($values as $key => $value){
        if ($value === true){
            $value = '<i class="icon-checkmark"></i>';
        }
        if ($value === false){
            $value = '<i class="icon-cancel---close"></i>';
        }
        $items .= sprintf($item_template,$key,$value);
    }

    return sprintf($template,$items);
}

/**
 * Export a value to Javascript.
 * 
 * @param string $key
 * @param mixed  $value
 * @param bool   $is_array
 * 
 * @return bool
 */
function js_export($key,$value,$is_array = false){

    if ($is_array){
        if (!isset($GLOBALS['_js'][$key])){
            $GLOBALS['_js'][$key] = [];
        }
        $GLOBALS['_js'][$key][] = $value;

        return true;
    }

    $GLOBALS['_js'][$key] = $value;

    return true;
}

/**
 * Convert post type slug into text name.
 * 
 * @param string $post_type
 * 
 * @return string
 */
function post_type_to_singular($post_type){
    switch($post_type){
        case 'slots-review':
            return 'Slot';
        break;
        case 'casino-review':
            return 'Casino';
        break;
        case 'betting-review':
            return 'Betting';
        break;
        case 'bingo-review':
            return 'Bingo';
        break;
    }

    return $post_type;
}

/**
 * Generate icons list for the review page.
 * determines if options are available and visible
 * 
 * @param string $visible_key - the settings key for the icon set values
 * @param string $values_key - the settings key for the selected icons
 * @param string $template - template file
 * 
 * @return void
 */
function icon_list($visible_key,$values_key,$template = 'review/icon-list/vertical.php'){
    $values = get_field($values_key);
    $visible = get_field($visible_key);
    
    $items = [];
    
    if (is_array($visible)){
        foreach($visible as $key => $value){
            $items[$value] = false;
        }
    }
    
    if (is_array($values)){
        foreach($values as $key => $value){
            if (isset($items[$value])){
                $items[$value] = true; 
            }
        }
    }

    include __DIR__ . '/../../reuse/' . $template;
}

/**
 * Generate icons list for review pages with custom fields settings in customizer.
 * all options are always available, determines if options are visible
 *  via customizer. e.g  sports-markets
 * 
 * @param string $available_key - the settings key for the icon set values
 * @param string $template - template file
 * 
 * @return void
 */
function icon_list_visible_customized($available_key, $template = 'review/icon-list/vertical.php'){

    $availables = get_field($available_key."_available");
    
    $items = [];

    if (is_array($availables)){
        foreach($availables as $available){
            $is_visible = get_theme_mod($available_key."_".$available);
            
            $items[$available] = $is_visible;
        }
    }
    
    include __DIR__ . '/../../reuse/' . $template;
}

/**
 * Generate icons list for review page
 * all options are always available, determines if options are visible
 * e.g  Special / In-Play Features
 * 
 * @param string $visible_key - the settings key for the icon set values
 * @param string $template - template file
 * 
 * @return void
 */
function icon_list_visible($visible_key, $template = 'review/icon-list/vertical.php'){

    $visibles = get_field($visible_key);
    
    $items = [];

    if (is_array($visibles)){
        foreach($visibles as $visible){  
            $items[$visible] = true;
        }
    }
    
    include __DIR__ . '/../../reuse/' . $template;
}

/**
 * Generate icons list for review page
 * all options always visible, determines if available
 * e.g supported-devices
 *  
 * @param string $visible_key - the settings key for the icon set values
 * @param string $template - template file
 * 
 * @return void
 */
function icon_list_available($visible_key, $template = 'review/icon-list/vertical.php'){

    $visibles = get_field_object($visible_key);
    
    $items = [];
    
    if (is_array($visibles)){

        foreach($visibles['choices'] as $key => $value){
            $items[$value] = false;
        }

        foreach($visibles['value'] as $key => $value){
            if (isset($items[$value])){
                $items[$value] = true; 
            }
        }  
    }
  
    include __DIR__ . '/../../reuse/' . $template;
}


/**
 * Generate icons list for an array of custom fields
 * all options always visible, determines if available
 * e.g 'Other Games'
 *  
 * @param array $visible_keys - the settings keys for the icon set values
 * @param string $template - template file
 * 
 * @return void
 */
function icon_list_customized($visible_keys, $template = 'review/icon-list/vertical.php'){

    
    $items = [];
    
    if (is_array($visible_keys)){

        foreach($visible_keys as $key => $value){
            $is_available = get_field($value);
            $label = get_field_object($value)['label'];
            $items[$label] = $is_available;
        }
    }
  
    include __DIR__ . '/../../reuse/' . $template;
}

/**
 * Generate a rating area.
 * 
 * @param array $ratings
 * 
 * @return void
 */
function review_rating($post_type){
    if (!is_array($GLOBALS['RATINGS'][$post_type])){
        return;
    }

    $ratings = [];
    foreach ($GLOBALS['RATINGS'][$post_type] as $key => $title) {
      $ratings[] = [
        'title' => $title,
        'value' => get_field('rating_'.$key)
      ];
    }

    include __DIR__ . '/../../reuse/review/rating.php';
}

/**
 * Generate breadcrumbs for the current http path.
 * 
 * @param string $separator
 * @param string $home
 * 
 * @return string
 */
function breadcrumbs($separator = ' ', $home = 'Home') {
    // This gets the REQUEST_URI (/path/to/file.php), splits the string (using '/') into an array, and then filters out any empty values
    $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));

    // This will build our "base URL" ... Also accounts for HTTPS :)
    $base = '//' . $_SERVER['HTTP_HOST'] . '/';

    // Initialize a temporary array with our breadcrumbs. (starting with our home page, which I'm assuming will be the base URL)
    $breadcrumbs = ["<li><a href=\"$base\">$home</a></li>"];

    $keys_of_path = array_keys($path);

    // Find out the index for the last value in our path array
    $last = end($keys_of_path);

    // Build the rest of the breadcrumbs
    foreach ($path as $x => $crumb) {
        // Our "title" is the text that will be displayed (strip out .php and turn '_' into a space)
        $title = ucwords(str_replace('-', ' ',$crumb));

        // If we are not on the last index, then display an <a> tag
        if ($x != $last)
            $breadcrumbs[] = "<li><i class=\"icon icon-arrow\"></i><a href=\"$base$crumb\">$title</a></li>";
        // Otherwise, just display the title (minus)
        else
            $breadcrumbs[] = "<li><i class=\"icon icon-arrow\"></i><a href=\"#\">$title</a></li>";
    }

    // Build our temporary array (pieces of bread) into one big string :)
    return '<ul>'.implode($separator, $breadcrumbs) . '</ul>';
}


/**
 * Check if the current site visitor is using a mobile device.
 * 
 * @return bool
 */
function is_mobile_phone() : bool {
	$useragent = $_SERVER['HTTP_USER_AGENT'];
    
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
        
        return true;
    }

    return false;
}

/**
 * Check if the current device is tablet.
 * 
 * @return bool
 */
function is_tablet() : bool {
    return !is_mobile_phone() && wp_is_mobile();
}

/**
 * Get academy posts by a given category id.
 * 
 * @param integer $id
 * 
 * @return array
 */
function get_academy_posts_by_cat($id) {
    global $post;

    $posts = [];

    $query = new WP_Query([
        'post_type' => 'page',
        'tax_query' => array(
            array(
                'taxonomy' => 'academy_categories',
                'field'    => 'term_id',
                'terms'    => $id,
            ),
        ),
    ]);

    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
            $query->the_post();
            $posts[] = [
                'id' => $post->ID,
                'title' => $post->post_title,
                'link' => get_permalink($post->ID),
                'excerpt' => get_the_excerpt()
            ];
        }
        wp_reset_postdata();
    }

    return $posts;
}
