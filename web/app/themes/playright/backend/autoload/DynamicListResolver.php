<?php

namespace Autoload;

use is_mobile_phone;

class DynamicListResolver {

    /**
     * @param string  $placement
     * @param integer $page_id
     * @param integer $list_id
     */
    public function __construct($placement,$page_id,$list_id = false, $beforeBrandsRender = false){

        // Create a new unicorn client
        $unicornClient = new \Unicorn\Client();
        
        // Resolve using the Unicorn client
        if (!$list_id){
            $response = $unicornClient->getOrder($placement,$page_id);
        }else{
            $response = $unicornClient->getOrderByListId($list_id);
        }
        
        // Error detection
        if (!$response || isset($response['data']['error'])){
            js_export('errors',$response);
            return;
        }
        
        $brands = [];
        
        $device_key = 'desktop';

        if (is_mobile_phone() && isset($response['data']['isMobileSpecific']) && intval($response['data']['isMobileSpecific'])){
            $device_key = 'modile';
        }

        foreach ($response['data'][$device_key]['brands'] as $brand_fields){
            $brands[] = [
                'placement' => $placement,
                'data'=>$this->resolveBrandDetails($brand_fields['id'],$brand_fields,$page_id)
            ];
        }
        
        // Generate brands sub templates
        if ($beforeBrandsRender){
            $brands = $beforeBrandsRender($brands);
        }

        js_export('brands_collection',$brands,true);
    }

    /**
     * Resolve brand details for a given post id and page id.
     * 
     * @param integer $post_id
     * @param array   $fields
     * @param integer $page_id
     * 
     * @return array
     */
    public function resolveBrandDetails($post_id,$fields = [],$page_id = false){
        $logo_key = is_mobile_phone() ? 'mobile_logo' : 'main_logo';

        $fields_whitelist = ['score','scoreText','stars'];

        switch($page_id){
            case 'crossbanner':
                // In case of crossbanner we want to take the settings from the customizer.
                $outlink = get_field('outlink_'.get_theme_mod('cross_banner_outlink_type'),$post_id);
                $bonus_text = get_field('bonus_'.get_theme_mod('cross_banner_bonus_type'),$post_id);
            break;
            case 'sidelist':
                // In case of sidelist we want to take the settings from the customizer.
                $outlink = get_field('outlink_'.get_theme_mod('side_dynamiclist_outlink_type'),$post_id);
                $bonus_text = get_field('bonus_'.get_theme_mod('side_dynamiclist_bonus_type'),$post_id);
            break;
            default:
                // We take the values from the page definition.
                $bonus_key = get_field('dr_bonus_type',$page_id);

                // Determine outlink type by device
                $outlink_type = is_mobile_phone() ? 'dr_mobile_outlink_type' : 'dr_desktop_outlink_type';
                $outlink_key = get_field($outlink_type,$page_id);
  
                if (!$outlink_key){
                    $outlink_key = 'default';
                }

                $device = is_mobile_phone() ? 'mobile_' : 'desktop_';
                $outlink = get_field('outlink_'.$device.$outlink_key,$post_id);
                
                if (!$bonus_key){
                    $bonus_key = 'default';
                }
                
                $bonus_text = get_field('bonus_'.$bonus_key,$post_id);
            break;
        }

        $brand_details = get_post_meta($post_id);
        
        if (!$brand_details || !is_array($brand_details)){
            $brand_details = [];
        }

        $slider_device_support = $this->resolveDeviceSupport($post_id);

        $overwrite = [
            'id' => $post_id,
            'outlink' => $outlink,
            'title' => get_the_title($post_id),
            'logo' => get_field($logo_key,$post_id), // Computed logo value see more details at the top of the function.
            'review_url' => get_permalink($post_id),
            'rating' => get_field('rating',$post_id),
            'bonus_text' => strip_tags($bonus_text,'<p><strong><b><i><span><small><h1><h2><h3><h4><h5><h6><br>'),
            'slider_device_support_mobile_class' => $this->booleanToCheckmarkClass($slider_device_support['Mobile']),
            'slider_device_support_tablet_class' => $this->booleanToCheckmarkClass($slider_device_support['Tablet']),
            'slider_device_support_desktop_class' => $this->booleanToCheckmarkClass($slider_device_support['Desktop'])
        ];

        foreach($overwrite as $key => $value){
            $brand_details[$key] = $value;
        }

        if (is_array($fields)){
            foreach($fields as $field_key => $value){
                if (in_array($field_key,$fields_whitelist)){
                    $brand_details[$field_key] = $value;
                }
            }
        }

        if (!isset($fields['score'])){
            $brand_details['score'] = get_field('default_dynamic_list_score',$post_id);
        }

        if (!isset($fields['scoreText'])){
            $brand_details['scoreText'] = get_field('default_dynamic_list_score_text',$post_id);
        }

        if (!isset($fields['stars'])){
            $brand_details['stars'] = get_field('default_dynamic_list_stars',$post_id);
        }

        $brand_details = $this->serialized($brand_details);

        return $brand_details;
    }

    /***
     * Loop over a given array and serialize values that needs to be serialized.
     * 
     * @param array $fields
     * 
     * @return array
     */
    protected function serialized($fields){
        foreach($fields as $key => $value){
            if (is_array($value) && isset($value[0])){
                if (is_serialized($value[0])){
                    $fields[$key] = unserialize($value[0]);
                }else{
                    if (count($value) === 1){
                        $fields[$key] = $value[0];
                    }
                }
            }
        }

        return $fields;
    }

    /**
     * Convert bool value to a class
     * made this function to avoid duplication due to multiple checkes that need to be made.
     * 
     * @param bool $bool
     * 
     * @return string
     */
    protected function booleanToCheckmarkClass($bool){
        return $bool ? 'icon-checkmark green' : 'icon-cancel---close red';
    }

    /**
     * Convert the device support metabox values to a basic array with devices supprot.
     * 
     * @param integer $post_id
     * 
     * @return array
     */
    protected function resolveDeviceSupport($post_id){
        $brand_device_support = get_field('devices',$post_id);

        if (!is_array($brand_device_support)){
            $brand_device_support = [];
        }
        
        $devices = [
            'Desktop' => false,
            'Mobile' => false,
            'Tablet' => false
        ];

        foreach($brand_device_support as $device){
            if (isset($devices[$device])){
                $devices[$device] = true;
            }
        }

        return $devices;
    }

    /**
     * A quick way to create instace of the current class without using the new keyword.
     * 
     * @param string  $placement
     * @param integer $page_id
     * @param integer $list_id
     * 
     * @return DynamicListResolver
     */
    public static function make($placement,$page_id,$list_id = false, $beforeBrandsRender = false){
        return new static($placement,$page_id,$list_id, $beforeBrandsRender);
    }

}