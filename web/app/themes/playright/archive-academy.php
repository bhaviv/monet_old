<?php
/**
 * Template Name:  Academy Navigational 
 **/

get_header();

$categories = get_terms('academy_categories', ['hide_empty' => false]);

?>
    <div class="container academy-archive">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center"><?php echo get_theme_mod('academy_title'); ?></h1>
                <p>
                    <?php echo get_theme_mod('academy_content'); ?>
                    <br /><small><?php echo get_theme_mod('academy_small_letters'); ?></small>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center categories-wrapper">
                <?php foreach($categories as $category): ?>
                    <a class="menu-item" href="#<?php echo $category->name; ?>">
                        <div class="img-wrapper">
                            <img src="<?php echo get_field('image',$category); ?>" class="img-responsive center-block"/>
                        </div>
                        <div class="category-title text-uppercase"><?php echo $category->name; ?></div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <?php foreach($categories as $category): ?>
        <div class="row wrapper" id="<?php echo $category->name; ?>">
            <div class="col-md-12">
                <h3 class="text-uppercase category-name"><?php echo $category->name; ?></h3>
            </div>
            <?php
            $posts = array_chunk(get_academy_posts_by_cat($category->term_id),2);
            foreach($posts as $academy_chunk):
                ?>
        <?php
        foreach($academy_chunk as $academy_post):
        ?>
            <div class="col-md-6 col-sm-6 col-xs-12 category">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <img src="<?php echo get_the_post_thumbnail_url($academy_post['id']); ?>"  class="img-article"/>
                    </div>
                    <div class="col-sm-8 col-xs-12 text">
                        <h4><?php echo esc_attr($academy_post['title']); ?></h4>
                        <div class="post-excerpt">
                            <p><?php echo $academy_post['excerpt']; ?></p>
                        </div>
                        <a href="<?php echo $academy_post['link']; ?>">Read More</a>
                    </div>
                </div>


            </div>
                    <?php endforeach; ?>
            <?php endforeach; ?>
        </div>
        <?php endforeach; ?>
    </div>

<?php
get_footer();