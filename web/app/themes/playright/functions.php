<?php
define('THEME_DIR', __DIR__);
 
require_once __DIR__.'/backend/Globals.php';

/**
 * A quick way to auto-include a folder.
 * 
 * @param string $folder
 * @param string $expression
 * 
 * @return void
 */
function require_all($folder,$expression){
	$files = glob(__DIR__. DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $expression);
	foreach($files as $file){
		require_once $file;
	}
}


/**
 * Require all php files from the autoload folder.
 */
require_all('backend/autoload','*.php');

$manager = new \AutoLoad\ThemeManager();

$manager->action('save_post',function () {
	$post_data = $_POST;

	$dynamiclists = [
		'dr_primary_dynamic_list' => 'dynamiclist_area',
		'dr_home_page_betting_dynamic_list' => 'dynamiclist_area_betting',
		'dr_home_page_casino_dynamic_list' => 'dynamiclist_area_casino',
		'dr_home_page_bingo_dynamic_list' => 'dynamiclist_area_bingo'
	];

	if (isset($post_data['fields'])){
		foreach($dynamiclists as $key => $placement_name)
		{
			if (isset($post_data['fields'][$key])){
				$GLOBALS['unicorn']->storeListPlacement($post_data['post_ID'],$post_data['fields'][$key],$placement_name);
			}
		}
	}
});

// Remove some default WordPress emoji support, WordPress is injecting some scripts we don't need.
// See full implementation at the ThemeManager class.
$manager->remove_emoji_scripts();

/***
 * Register Playright's Brands
 */
$manager->registerPostType([
	'slug' => 'bingo',
	'name' => 'Bingo Review',
	'icon' => 'dashicons-tickets-alt',
  'perma_structure_prefix' => '/bingo/reviews',
])->registerPostType([
	'name' => 'Betting Review',
	'slug' => 'betting',
	'icon' => 'dashicons-welcome-write-blog',
  'perma_structure_prefix' => '/betting/reviews',
])->registerPostType([
	'slug' => 'casino',
	'name' => 'Casino Review',
	'icon' => 'dashicons-store',
  'perma_structure_prefix' => '/casino/reviews',
])->registerPostType([
	'slug' => 'slots',
	'name' => 'Slots Review',
	'icon' => 'dashicons-vault',
  'perma_structure_prefix' => '/slots/games',
])->registerPostType([
	'slug' => 'software',
	'name' => 'Software Review',
	'icon' => 'dashicons-vault',
  'perma_structure_prefix' => '/casino/software',
])->registerPostType([
  'slug' => 'tables',
  'name' => 'Table',
  'icon' => 'dashicons-welcome-learn-more',
  'extra_args' => [
    'publicly_queryable'    => false,
  ]
]);

/**
 * Register site customizers
 * 
 * @uses $GLOBALS[BONUS_TYPES],$GLOBALS[DESKTOP_OUTLINK_TYPES],$GLOBALS[MOBILE_OUTLINK_TYPES],
 * $GLOBALS[SPORTS_MARKETS],$manager,$unicorn
 */
require "backend/customizer.php";


$manager->action('wp_footer', function () {
	// Export is mobile data
	js_export('is_mobile',is_mobile_phone());
	js_export('admin_ajax_url',admin_url('admin-ajax.php'));

	// Register the main javascript bundle.
	wp_register_script('playright-script', get_template_directory_uri() . '/front/js/app.bundle.js?app_version=' . env('APP_VERSION','1.0.0'));

	// Localize server side data like unicorn list order and other information that comes from the server
	// into the window.PR var, you may add data using the `js_export` function.
	wp_localize_script('playright-script', 'PR', $GLOBALS['_js']);

	// Render the script.
	wp_enqueue_script('playright-script');

	/**
	 * Development only scripts.
	 */
	if (env('WP_ENV') === 'development'){
		wp_enqueue_script( 'dev-script' , get_template_directory_uri() . '/front/js/dev.js?t=' . time());
	}
});

$manager->action('after_setup_theme',function () {
	// Register Theme Default Menues
	register_nav_menus([
		'header' => 'Primary',
	]);
    register_nav_menus([
        'footer' => 'Footer Menu',
    ]);

	// Customize some of the posts types bihavior
	add_theme_support('title-tag');
	add_post_type_support('casino', 'excerpt');
	add_post_type_support('academy', 'excerpt');
	add_post_type_support('slots', 'excerpt');
	add_post_type_support('betting', 'excerpt');
	add_post_type_support('bingo', 'excerpt');
	add_theme_support('post-thumbnails');
	add_theme_support('customize-selective-refresh-widgets');
});


/**
 * Register academy widget area.
 */
$manager->registerWidgetArea([
	'name'          => 'Academy Sidebar',
	'id'            => 'academy-sidebar',
	'description'   => 'Sidebar for academy posts.',
	'before_widget' => ' ',
	'after_widget'  => ' ',
	'before_title'  => ' ',
	'after_title'   => ' ',
]);


require "backend/shortcode.php";

require "backend/rewrite.php";

/**
 * Make an endpoint for email submission.
 */
$manager->ajax('send_mail',function () {
	return ['example' => 1];
});



// Hook everting togther
$manager->run();

