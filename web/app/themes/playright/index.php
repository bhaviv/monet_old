<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package playright
 */

get_header();
?>
<div class="container-fluid main-content-wrapper-hp">
    <div class="container title-with-line">
        <div class="row">
            <div class="col-md-12">
                <h2>Welcome to PlayRight</h2>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <p class="subtitle text-center">Explore awesome game brands (casino, betting, bingo)</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row video-text">
            <div class="col-md-6  col-sm-12 col-xs-12 col-md-offset-1">
                <p>Brought to you by <b>PlayRight’s</b> passionate team of <b>experienced punters</b> and former industry insiders.</p>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 nopadding">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/1xhaSoE9DBg?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row brand-of-the-month">
        <div class="col-md-6 col-xs-12 border space-from-right ">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-wrapper">
                        <img src="http://monet.dev/app/uploads/2017/12/10bet-casino.jpg">
                        <div class="text">
                            <p>Casino of the Month</p>
                            <p>10Bet Casino</p>
                        </div>
                    </div>
                    <p class="post-paragraph">10Bet combines a strong games catalogue sourced from a diverse group of software providers with some of the best bonus offers around. Unlike many of its competitors, all the deals are within reach of even the most casual player, thanks to reasonable wagering requirements and fair conditions.</p>
                    <div class="tablet-link">
                        <a class="default-button text-center" title="Read 10Bet Casino Review" href="#" target=“_blank”>Read 10BET casino Review</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 space-from-left">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-wrapper">
                        <img src="http://monet.dev/app/uploads/2017/12/10bet-casino.jpg">
                        <div class="text">
                            <p>Slot of the Month</p>
                            <p>Slot of the Month</p>
                        </div>
                    </div>
                    <p class="post-paragraph">The slot’s two main features are Cleopatra Wilds and the Death Mask Scatter symbol. The former substitutes for any symbol in the game, and doubles any win when they part of a winning payline.</p>
                    <div class="tablet-link">
                        <a class="default-button text-center" title="Read 10Bet Casino Review" href="#" target=“_blank”>Read 10BET casino Review</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="container title-with-line">
        <div class="row">
            <div class="col-md-12">
                <h2>PlayRight Recommends</h2>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <p class="subtitle text-center">There are lots of things an online gambling site has to get right if it’s to earn a thumbs-up from us. When one of our review team sits down to run the rule over a new site they do so with a long checklist of criteria on hand. Here are some of the key factors.</p>
            </div>
        </div>
    </div>

<div class="container dynamic-list-hp">
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <h3 class="text-center text-uppercase betting">Betting</h3>
            </div>
                <div class="col-md-12 col-sm-12 wrapper">
                    <div class="row top-wrapper">
                        <div class="col-xs-5">
                            <img src="<?php echo get_template_directory_uri(); ?>/front/img/Skybet-MobileLogo-2.png" class="img-responsive">
                        </div>
                        <div class="col-xs-7">
                            <a class="default-button green-bg pull-right text-center">PLAY NOW</a>
                        </div>
                    </div>
                    <div class="row bottom-wrapper">
                        <div class="col-xs-4 rating">
                            <div class="rate-it">
                                <i class="stars" data-rate_amount="4.5"></i>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-8">
                            <a class="review-link pull-right text-right" title="Sky Bet Review" href="#" target="_blank">Sky Bet Review</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <a class="check-button text-center text-uppercase center-block" title="Check betting list" href="#" target=“_blank”>Check betting list</a>
                </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <h3 class="text-center text-uppercase casino">Casino</h3>
            </div>
            <div class="col-md-12 col-sm-12 wrapper">
                <div class="row top-wrapper">
                    <div class="col-xs-5">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/Skybet-MobileLogo-2.png" class="img-responsive">
                    </div>
                    <div class="col-xs-7">
                        <a class="default-button green-bg pull-right text-center">PLAY NOW</a>
                    </div>
                </div>
                <div class="row bottom-wrapper">
                    <div class="col-xs-4 rating">
                        <div class="rate-it">
                            <i class="stars" data-rate_amount="4.5"></i>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-8">
                        <a class="review-link pull-right text-right" title="Sky Bet Review" href="#" target="_blank">Sky Bet Review</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <a class="check-button text-center text-uppercase center-block" title="Check betting list" href="#" target=“_blank”>Check betting list</a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <h3 class="text-center text-uppercase bingo">Bingo</h3>
            </div>
            <div class="col-md-12 col-sm-12 wrapper">
                <div class="row top-wrapper">
                    <div class="col-xs-5">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/Skybet-MobileLogo-2.png" class="img-responsive">
                    </div>
                    <div class="col-xs-7">
                        <a class="default-button green-bg pull-right text-center">PLAY NOW</a>
                    </div>
                </div>
                <div class="row bottom-wrapper">
                    <div class="col-xs-4 rating">
                        <div class="rate-it">
                            <i class="stars" data-rate_amount="4.5"></i>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-8">
                        <a class="review-link pull-right text-right" title="Sky Bet Review" href="#" target="_blank">Sky Bet Review</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <a class="check-button text-center text-uppercase center-block" title="Check betting list" href="#" target=“_blank”>Check betting list</a>
            </div>
        </div>
    </div>

</div>

    <div class="container title-with-line">
        <div class="row">
            <div class="col-md-12">
                <h2>How We Compare</h2>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <p class="subtitle text-center">There are lots of things an online gambling site has to get right if it’s to earn a thumbs-up from us. When one of our review team sits down to run the rule over a new site they do so with a long checklist of criteria on hand. Here are some of the key factors.</p>
            </div>
        </div>
    </div>
    <div class="container compare-hp">
        <div class="row">
            <div class="col-lg-4 col-md-7 col-sm-6 col-xs-12 item">
                <div class="sprite-hp star center-block"></div>
                <h3 class="text-center">How We Rate Gambling Sites</h3>
                <p class="text-center">We showing to much staff , like a safety and security , software , game selection and much more</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp shield center-block"></div>
                <h3 class="text-center">Safety & Security</h3>
                <p class="text-center">Does the site take care of its responsibilities to customers?</p>
            </div>
            <div class="col-lg-4 col-md-7 col-sm-6 col-xs-12 item">
                <div class="sprite-hp deposits center-block"></div>
                <h3 class="text-center">Deposits & Withdrawals</h3>
                <p class="text-center">How quick and easy it to get your money on and off the site?</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp game center-block"></div>
                <h3 class="text-center">Game Selection</h3>
                <p class="text-center">Whether casino, bingo or betting – we want lots of options.</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp software center-block"></div>
                <h3 class="text-center">Software Quality</h3>
                <p class="text-center">Is the platform stable? How easy is it to find what you’re looking for?</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp devices center-block"></div>
                <h3 class="text-center">Device Compatibility</h3>
                <p class="text-center">Can you get the same experience on your phone or tablet as on your laptop?</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item">
                <div class="sprite-hp bonuses center-block"></div>
                <h3 class="text-center">Bonuses & Promotions</h3>
                <p class="text-center">Is there a good welcome bonus? How easy is it to meet the terms and conditions?</p>
            </div>
        </div>
    </div>
    <div class="container exclusive-bonus-form">
        <div class="row">
            <h2 class="text-center">Get the Latest Exclusive Bonuses</h2>
            <div class="col-md-8 col-md-offset-2">
                <p class="text-center">There are lots of great reasons to subscribe to our email list. You’ll be the first to read the latest news and freshest reviews, for a start. But we know what you really want: Exclusive Bonuses. And, fine, we have loads of those, too.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-sm-12 col-md-offset-1">
                <form class="text-center">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Your Name" required="">
                        <div class="text-error"></div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="name" placeholder="Your Email" required="">
                        <div class="text-error"></div>
                    </div>
                    <button type="submit" class="default-button">Send</button>
                    <div class="terms text-left">
                        <input type="checkbox" name="newsletter_tc" class="checkbox-custom">
                        <label for="newsletter-tc icon-checkmark"><a href="/terms-and-conditions" target="_blank">I have read and agreed to the terms &amp; conditions.</a><div><small> *We will never spam you, or pass on your personal data without permission.</small></div></label>
                        <div class="text-error"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container hidden-xs footer-images-line">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/visa.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/mastercard.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/paypal.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/american-express.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/skrill.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="payment-method-logo-container" >
                    <div class="payment-method-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/img/western-union.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();