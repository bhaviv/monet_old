<?php
/**
 * Template Name: Academy Single Template
 */

/**
 * For some template we allow the editor to choose a custom background image.
 * The value will contain the full url of the image and will be used to display the image.
 *
 * @ver string
 */

get_header();
if ( have_posts() ) while ( have_posts() )  the_post();
$reviewer = get_field('reviewer');
?>
<div class="container academy-single">
    <div class="row content-wrapper">
        <div class="col-md-12 col-lg-8">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo esc_attr(get_field('header_image')); ?>"  class="img-responsive img-title"/>
                    <h1 class="main-title text-capitalize"><?php the_title(); ?></h1>
                    <div class="features">
                        <span class="social-attributes">
                            <span class="date"><?php the_date('d.m.y'); ?></span>
                            <span class="likes disabled posts-likes-wrapper" data-type="like" data-post_id="<?php echo get_the_ID(); ?>">
                                <i class="icon-article-like"></i>
                                <span class="count"><?php echo post_likes(get_the_ID()); ?></span>
                            </span>
                            <span class="article-comment">
                                <i class="icon-article-comment"></i>
                                <span class="count"> 0 </span>
                            </span>
                            <span class="author"> by <?php printf('%s %s',$reviewer['user_firstname'],$reviewer['user_lastname']); ?> </span>
                        </span>
                    </div>
                    <div>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- Author -->
                    <?php include "reuse/review/author.php" ?>
                    <!-- End Author -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- Review Title -->
                    <h2 class="review-title">Comments</h2>
                    <!-- End Review Title -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- Comments -->
                    <?php include "reuse/comment-form.php" ?>
                    <!-- End Comments -->
                </div>
            </div>

        </div>
        <div class="col-lg-4 hidden-sm hidden-xs hidden-md right-part">
            <div class="row">
                <div class="col-md-12">
                    <!-- Review Title -->
                    <h3 class="academy-single-title text-uppercase">Top5 casino sites</h3>
                    <!-- End Review Title -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 bottom-40">
                    <!-- Review Title -->
                    <?php include "reuse/dynamiclists/sidelist.php" ?>
                    <!-- End Review Title -->
                </div>
            </div>
            <?php dynamic_sidebar('academy-sidebar'); ?>
            <div class="row">
                <div class="col-md-12">
                    <!-- Review Title -->
                    <h3 class="academy-single-title text-uppercase">Latest articles</h3>
                    <!-- End Review Title -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Review Title -->
                    <?php include "reuse/latest-articles.php" ?>
                    <!-- End Review Title -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
