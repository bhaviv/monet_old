<?php
/*
Plugin Name: FunFun Likes
Author: Ron Masas
Description: A fun likes plugin.
*/

function handle_ajax_like(){
    $post_id = isset($_GET['post_id']) ? $_GET['post_id'] : false;
    if (!$post_id){
        die('missing post id');
    }

    $current_post_likes = get_post_meta($post_id,'post_likes_count',true);
    update_post_meta($post_id, 'post_likes_count',intval($current_post_likes)+1);

    die('success:'.$current_post_likes);
}

function post_likes($post_id){
    $likes = get_post_meta($post_id, 'post_likes_count', true);
    if (!$likes){
        return '0';
    }

    return $likes;
}

add_action('wp_ajax_like','handle_ajax_like'); // Only for logged WordPress users
add_action('wp_ajax_nopriv_like','handle_ajax_like'); // Only for guests
