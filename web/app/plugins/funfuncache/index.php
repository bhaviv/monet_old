<?php

/*
Plugin Name: FunFunCache
Author: Ron Masas
Description: A fun caching system for WordPress.
*/

require_once __DIR__ . '/preload.php';

$GLOBALS['__cached_content'] = '';

function isCacheOffline(){
    try{

        return isset($GLOBALS['__funfuncache']) == false || $GLOBALS['__funfuncache']->getCacheClient()->isConnected() == false;
    }catch(Exception $ex){

        return false;
    }
}

/**
 * Add the "FunFunCache" button
 */
add_action('admin_bar_menu', function($wp_admin_bar){
	$wp_admin_bar->add_node([
		'id' => 'clear-cache-button',
        'title' => 'Clear Cache',
		'href' => env('WP_HOME') . '?funfuncache=1',
		'meta' => [
            'target'=>'_blank',
		    'class' => 'funfuncache-button-class'
		]
	]);
}, 50);

if (isset($GLOBALS['__funfuncache'])){
    try{
        $GLOBALS['__funfuncache']->purge();
    }catch(Exception $ex){
        // handle errors
    }
}

if (isset($GLOBALS['FUNFUNCACHE_REDIS_LOCK'])){
    add_action( 'admin_notices', function(){
        $error = 'FunFunCache is not caching! check redis server connection and delete the <i>.redis_lock</i> file located on the funfuncahce plugin folder.';
        printf('<div class="notice notice-error is-dismissible"> <p>%s</p> </div>',$error);
    });
}

add_action('template_redirect',function(){
    if (isCacheOffline()){
        // Redis client is offline don't try to store cache.
        return;
    }

	ob_start(function ($content){
        $GLOBALS['__cached_content'] .= $content;
	});
});

add_action('shutdown',function(){
    // Print site
    print $GLOBALS['__cached_content'];
    
    // Flush it to avoid redis delay
    flush();
    
    if (isCacheOffline()){
        
        return;
    }

    if (is_user_logged_in()){
        // Don't store cache for logged users.
		return; 
    }
    
	try{
        // Validate content size
		if (strlen($GLOBALS['__cached_content']) >= 30){
            // Store cache!
            $GLOBALS['__funfuncache']->set($GLOBALS['__cached_content']);
		}
	}catch(Exception $error){
        // Errors handling should be here...
        // var_dump($error);
	}
});