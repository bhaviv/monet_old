<?php

/**
 * A fun cache class for WordPress caching.
 * 
 * @author Ron Masas
 * @version 0.0.1
 */

require __DIR__ . '/Mobile-Detect/MobileDetect.php';

class FunFunCache {
    
    /**
     * @ver array
     */
    protected $_before = [];

    /**
     * @ver array
     */
    protected $_purgers = [];

    /**
     * @ver string
     */
    protected $host;

    /**
     * @ver Predis\Client
     */
    protected $cache_client;

    /**
     * @ver string
     */
    protected $request_signature;

    /**
     * @ver array
     */
    protected $serve_conditions = [];

    /**
     * @ver array
     */
    protected $redis_timeout_files = [
        'lock' => __DIR__ . '/.redis_lock',
        'timeout' => __DIR__ . '/.redis_timeout_'
    ];

    /**
     * @param Predis\Client $redis
     */
    public function __construct($redis){
        $this->cache_client = $redis;
        $this->host = $_SERVER['HTTP_HOST'];
        $this->request_signature = $this->resolveRequestSignature();

        if (file_exists($this->redis_timeout_files['lock'])){
            $GLOBALS['FUNFUNCACHE_REDIS_LOCK'] = true;
        } else {
          try{
              $redis->connect(); // Open redis connection
          }catch(Exception $redis_timeout_exception){
              try{
                  // Handle redis timeout errors
                  $this->handleTimeoutError();
              }catch(Exception $read_write_premission_exception){
                  // Catch file write read premission errors
                  // To avoid down time in case of premissions type fetal errors.
              }
          }
        }
    }

    /**
     * Register a method to checked before serving cache.
     * If the registered method returns true the cache will not be served.$_COOKIE
     * 
     * @param function $fn
     * 
     * @return FunFunCache
     */
    public function dontServeWhen($fn){
        $this->serve_conditions[] = $fn;

        return $this;
    }

    /**
     * Register a new purge logic, try to keep it simple
     * No database connection or time consuming calculations.
     * 
     * @param function $fn
     * 
     * @return FunFunCache
     */
    public function registerPurger($fn){
        $this->_purgers[] = $fn;

        return $this;
    }

    /**
     * Register a pre cache logic
     * Try to keep the methods you add using this function short
     * Since each function you add here will be executed each time the site loads. 
     * 
     * @param function $fn
     * 
     * @return FunFunCache
     */
    public function before($fn){
        $this->_before[] = $fn;

        return $this;
    }

    /**
     * Serve cache and handle related logic.
     * 
     * @return mixed
     */
    public function serve(){
        if (!$this->cache_client->isConnected()){
            // Redis is offline
            return false;
        }

        foreach($this->serve_conditions as $fn){
            if ($fn() === true){
                return false;
            }
        }

        // Run all purge functions
        $this->purge();

        // Check if cache exists for the requested resource
        if ($this->exists($this->request_signature)){

            // Run pre cache required logic
            foreach($this->_before as $fn){
                $fn();
            }

            header('FunFunCache: HIT');

            // Fetch html from cache
            $html = $this->get($this->request_signature);

            print $html;

            flush(); // Flush to avoid delay of redis disconnect

            // Close redis connection
            $this->cache_client->disconnect();

            die;
        }

        // Move on to WordPress where the funfuncache function will handle storing the cache.
        // For more details see /wp-content/plugins/funfuncache/index.php
        return false;
    }

    /**
     * Handle possible redis timeout to avoid
     * keep trying to connect after a certain threshold.
     * 
     * @return void
     */
    protected function handleTimeoutError(){
        $redis_timeout_file = $this->redis_timeout_files['timeout'];

        for ($threshold = 0;$threshold<=3;$threshold++){
            if (!file_exists($redis_timeout_file . $threshold)){
                return file_put_contents($redis_timeout_file . $threshold,time());
            }
        }

        if (!file_exists($this->redis_timeout_files['lock'])){
            // Write the redis lock file
            file_put_contents($this->redis_timeout_files['lock'],time());

            // Delete the timeout files
            $timeout_files = glob($redis_timeout_file . '*');
            foreach($timeout_files as $file){
                unlink($file);
            }
        }
    }

    /**
     * Run all purge logic
     * 
     * @return boolean
     */
    public function purge(){
        foreach($this->_purgers as $purger){
            if ($purger() === true){
                $this->purgeCurrentSiteCache();

                return true;
            }
        }

        return false;
    }

    /**
     * Resolve request signature
     * 
     * @return string
     */
    protected function resolveRequestSignature(){        
        $device = $this->getDevice();
        $request   = preg_replace('/\?.*/', '',strtolower($_SERVER['REQUEST_URI']));
        $iso       = isset($_SERVER["HTTP_CF_IPCOUNTRY"]) ? $_SERVER["HTTP_CF_IPCOUNTRY"] : 'UNKNOWN';

        $supported_params = ['s','p','page','author','page_id','post_id','post'];

        foreach($supported_params as $param){
            if (isset($_GET[$param])){
                $request .= $param . '=' . $_GET[$param];
            }
        }

        return $this->host . ':' . $device . '/' . $iso . '/' . md5($request);
    }

    /**
     * Resolve the current visitor device name.
     * 
     * @return string
     */
    protected function getDevice(){
        $mobileDetectionClient = new MobileDetect();

        if ($mobileDetectionClient->isTablet()){
            return 'tablet';
        }

        if ($mobileDetectionClient->isMobile()){
            return 'mobile';
        }

        return 'desktop';
    }

   /**
    * Get the cache client instance
    *
    * @return Predis\Client
    */
    public function getCacheClient(){
        return $this->cache_client;
    }  

    /**
     * Delete the current site cache (by http request contaxt using HTTP_HOST)
     * 
     * @return FunFunCache
     */
    public function purgeCurrentSiteCache(){
        $this->cache_client->flushDb();

        return $this;
    }

    /** Redis Wrapper **/

    /**
     * Cache a value for the current request signature.
     * 
     * @param string $value
     * 
     * @return FunFunCache
     */
    public function set($value){
        $this->cache_client->set($this->request_signature,$value);
        $this->cache_client->expireat("expire in 1 day", strtotime("+1 days"));

        return $this;
    }

    /**
     * Get a key value from redis.
     * 
     * @param string $key
     * 
     * @return string
     */
    public function get($key){
        return $this->cache_client->get($key);
    }

    /**
     * Check if a key exists on redis.
     * 
     * @param string $key
     * 
     * @return boolean
     */
    public function exists($key){
        return $this->cache_client->exists($key);
    }
}
