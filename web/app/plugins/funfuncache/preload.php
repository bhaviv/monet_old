<?php

/**
 * This file need to be required at the very start of your application
 * Or alternatively in the earliest stage possible.
 */

require __DIR__ . '/FunFunCache.php';
require __DIR__ . '/predis/src/Autoloader.php';

Predis\Autoloader::register();

/**
 * Resolve the redis database id by the current domain.
 * 
 * @return integer
 */
function resolve_redis_database(){
    // Default database id
    $database = 0;

    switch($_SERVER['HTTP_HOST']){
        case 'top5voipproviders.com':
            $database = 1;
        break;
        case 'top5casinosites.co.uk':
            $database = 2;
        break;
        case 'top5-vpn.com':
            $database = 3;
        break;
        case 'top5bingosites.co.uk':
            $database = 4;
        break;
        case 'playright.co.uk':
            $database = 5;
        break;
    }

    return $database;
}

$redis = new Predis\Client([
    'host'     => 'top5redis.npodij.0001.euw1.cache.amazonaws.com',
    'timeout'  => 2,
    'database' => resolve_redis_database()
]);

$funfuncache = $GLOBALS['__funfuncache'] = new FunFunCache($redis);

/**
 * Empty cache for the current redis database
 * when `funfuncache` parameter exists in the current request.
 * 
 * @return boolean
 */
$funfuncache->registerPurger(function (){
    return isset($_GET['funfuncache']);
});

/**
 * Hook into the "empty all cache" button.
 * When clicked all cache for the current redis database will be deleted.
 * 
 * @return boolean
 */
$funfuncache->registerPurger(function (){
    return isset($_GET['page']) && $_GET['page'] === 'w3tc_dashboard'
           && isset($_GET['w3tc_note']) && $_GET['w3tc_note'] === 'flush_all';
});

/**
 * Redirect https to http
 */
$funfuncache->before(function (){
    $is_ajax_request = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

    if ($is_ajax_request){
        return;
    }

    $visitor = isset($_SERVER['HTTP_CF_VISITOR']) ? $_SERVER['HTTP_CF_VISITOR'] : 'http';
    $protocol = strstr($visitor,'https') ? 'https' : 'http';

    if ($protocol === 'https'){
        header("Location: http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        die;
    }
});

/**
 * Run country restriction logic before cache loads.
 * 
 * @return void
 */
$funfuncache->before(function (){
    require_once __DIR__ . '/../region-restricter/Restricter.php';

    $restriction = new Restricter();
    $restriction->blockRegions(['IL'])
                ->withCredentials('tp','tp');

    $region = isset($_SERVER['HTTP_CF_IPCOUNTRY']) ? $_SERVER['HTTP_CF_IPCOUNTRY'] : 'UNKNOWN';
    
    $il_blocked_domains = ['new.playright.co.uk','playright.co.uk'];

    if ($restriction->isRegionBlocked($region)
        && in_array($_SERVER['HTTP_HOST'],$il_blocked_domains) && !isset($_GET['tp-bypass'])){
        $restriction->restrict();
    }
});

/**
 * Don't serve cache for ajax requests.
 * 
 */
$funfuncache->dontServeWhen(function (){
    $is_ajax_request = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    
    return $is_ajax_request;
});

/**
 * Support for the wordpress preview option
 * 
 */
$funfuncache->dontServeWhen(function (){
    return isset($_GET['preview']);
});

// Serve cached response
$funfuncache->serve();