<?php

class Restricter{

   /**
    * @ver array
    */
    protected $credentials = [];

   /**
    * @ver array
    */
    protected $blocked_regions = [];

    /**
     * Check if a given region iso is blocked.
     *
     * @param string $region
     *
     * @return boolean
     */
    public function isRegionBlocked($region){
        return in_array($region,$this->blocked_regions);
    }

    /**
     * Set a username and password for the current restricter.
     *
     * @param string $username
     * @param string $password
     *
     * @return Restricter
     */
    public function withCredentials($username,$password){
        $this->credentials[$username] = $password;
        return $this;
    }

    /**
     * Set blocked regions.
     *
     * @param array $regions
     *
     * @return Restricter
     */
    public function blockRegions($regions){
        $this->blocked_regions = $regions;
        return $this;
    }

    /**
     * Send out the Authentication headers for guests.
     *
     * @return void
     */
    public function restrict(){
        $realm = 'Restricted.';

        if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Digest realm="'.$realm.
            '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
            die('Sorry.');
        }

        $data = $this->parseHttpAuthDigest($_SERVER['PHP_AUTH_DIGEST']);

        if (!is_array($data)){
            return $this->block();
        }

        if (!isset($this->credentials[$data['username']])){
            return $this->block();
        }

        $A1 = md5($data['username'] . ':' . $realm . ':' . $this->credentials[$data['username']]);
        $A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
        $valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

        if ($data['response'] != $valid_response){
            return $this->block();
        }

        return true;
    }

    /**
     * Render the blocked response.
     *
     * @return void
     */
    protected function block(){
        header('HTTP/1.1 401 Unauthorized');
        die('Wrong Credentials!');
    }

    /**
     * Parse the auth digest text data.
     *
     * @param string $txt
     *
     * @return mixed
     */
    protected function parseHttpAuthDigest($txt)
    {
        // protect against missing data
        $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
        $data = array();
        $keys = implode('|', array_keys($needed_parts));
    
        preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);
    
        foreach ($matches as $m) {
            $data[$m[1]] = $m[3] ? $this->prepare($m[3]) : $this->prepare($m[4]);
            unset($needed_parts[$m[1]]);
        }
    
        return $needed_parts ? false : $data;
    }

    /**
     * Clean the auth data to avoid bugs on diffrent server configurations.
     *
     * @param string $text
     * 
     * @return string
     */
    protected function prepare($text){
        return str_replace('"','',stripslashes($text));
    }

}