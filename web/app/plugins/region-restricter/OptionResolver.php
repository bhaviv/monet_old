<?php

class OptionResolver {

    /**
     * Get an option from the current website, if not exists set a new one.
     *
     * @param string $key
     * @param string $default
     *
     * @return string
     */
    public static function get($key,$default = ''){
        
        $value = get_option($key,null);

        if ($value === null){
            update_option($key,$default);

            return $default;
        }

        return $value;
    }

    /**
     * Convert option string to an array.
     *
     * @param string $text
     * @param string $split
     *
     * @return array
     */
    public static function toArray($text,$split = ','){
        if (!strstr($text,$split)){

            return [$text];
        }

        return explode(',',$text);
    }

}