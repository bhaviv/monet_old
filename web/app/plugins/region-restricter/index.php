<?php

/*
Plugin Name: Region Restricter
Description: Restrict a given region from accessing this site, using basic authentication.
Author: Traffic Point Development Team
Version: 1.0
*/

require_once 'Restricter.php';
require_once 'OptionResolver.php';

$regions = OptionResolver::toArray(OptionResolver::get('rr_regions_to_block'));

$restriction = new Restricter();

$restriction->blockRegions($regions)
            ->withCredentials('tp','tp');

add_action('template_redirect',function () use ($restriction) {
    
    $region = isset($_SERVER['HTTP_CF_IPCOUNTRY']) ? $_SERVER['HTTP_CF_IPCOUNTRY'] : 'UNKNOWN';

    if ($restriction->isRegionBlocked($region)){
        $restriction->restrict();
    }
    
});

