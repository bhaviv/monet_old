'use strict'

Vue.component('alert',{
    props: ['msg'],
    template: '<div class="updated settings-error notice is-dismissible" v-show="msg.length > 0"> <p> <strong>{{ msg }}</strong> </p> </div>'
});

var FunFunListEditor = new Vue({
    el: '#app',
    data:{
        msg: '',
        selected: '',
        bulk_list_id: '',
        selected_content_type: -1,
        main_loader: false,
        options: [
          { text: 'Order (Dynamic List Temaplte)', value: 'dr_primary_order', post_type: 'page' },
          { text: 'OutLink (Dynamic List Temaplte)', value: 'dr_desktop_outlink_type', post_type: 'page' },
          { text: 'Bonus (Dynamic List Temaplte)', value: 'dr_bonus_type', post_type: 'page'}
        ],
        pages: []
    },
    mounted: function(){
        //...
    },
    computed: {
        selected_pages: function(){
            return this.pages.filter(function (page){
                return page.checked === true
            })
        },
        global_list: function(){
            var list = []

            switch(this.selected){
                case 'dr_desktop_outlink_type':
                    list = window.DesktopOutlinkTypes
                break;
                case 'dr_bonus_type':
                    list = window.BonusTypes
                break;
                default:
                    list = window.UnicornLists
                break;
            }

            for (var id in list){
                    this.bulk_list_id = id
                break
            }

            return list
        }
    },
    methods: {
        toggle: function(event){
            var checked = event.target.checked
            this.pages = this.pages.map(function(page){
                page.checked = checked
                return page
            })
        },
        bulkUpdate: function(){
            var request = JSON.parse(JSON.stringify(this.selected_pages))
            
            request = request.map(function(page){
                page.meta_value = this.bulk_list_id
                return page
            }.bind(this))

            this.main_loader = true;

            Http.post('update_pages_meta',{update: request}).then(function (results){
                this.getPages()
            }.bind(this))
        },
        update: function(page){
            page.loading = true
            Http.post('update_pages_meta',{update: [page]}).then(function (results){
                page.loading = false
                this.msg = 'Page list was successfully updated.'
            }.bind(this))
        },
        getPages: function(){
            this.msg = ''
            this.main_loader = true;
            
            return new Promise(function (resolve,reject){
                this.pages = []
                
                var request = {key:this.selected};

                if (this.selected_content_type != -1){
                    request.content_type = this.selected_content_type
                }

                Http.get('get_pages_by_metakey',request).then(function (response){
                    this.main_loader = false

                    if(response.error){
                        this.msg = 'Whoops something went wrong.'
                        return reject(false)
                    }
    
                    if (response.results.length === 0){
                        this.msg = 'No pages found.'
                        return reject(false)
                    }
    
                    var pages = response.results.map(function (page){
                        page.checked = false
                        page.loading = false
                        return page
                    });
    
                    this.pages = pages

                    this.msg = pages.length + ' results found.'

                    resolve(pages)
                }.bind(this))
            }.bind(this))
        }
    }
})

var Http = {
    get: function(action,qs){
        return new Promise(function (resolve,reject){
            qs.action = action
            jQuery.get('./admin-ajax.php',qs,resolve)
        })
    },
    post: function(action,data){
        return new Promise(function (resolve,reject){
            jQuery.post('./admin-ajax.php?action=' + action,data,resolve)
        })
    }
}