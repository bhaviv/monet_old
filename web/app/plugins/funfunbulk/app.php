<?php

/*
Plugin Name: FunFunBulk
Author: Ron Masas
Description: A quick way to update posts relations to Unicorn lists, outlinks and bonuses.
*/

function register_ajax_endpoint($route,$fn){
    add_action("wp_ajax_$route",function () use ($fn){
        global $wpdb;

        $results = ['results' => $fn($_REQUEST,$wpdb)];
        $results['error'] = $wpdb->last_error;

        header('Content-Type: application/json');
        die(json_encode($results));
    });
}

add_action('admin_menu', function(){
    add_menu_page('FunFunBulk', 'FunFunBulk', 'manage_options', __FILE__, function(){
        include 'views/admin.php';
    }, 'dashicons-randomize');
});

register_ajax_endpoint('update_pages_meta', function ($request,$wpdb){
    $update_requests = isset($request['update']) ? $request['update'] : [];

    $results = [];

    foreach($update_requests as $update_request){
        $page_id = isset($update_request['ID']) ? $update_request['ID'] : false;
        $meta_key = isset($update_request['meta_key']) ? $update_request['meta_key'] : false;
        $meta_value = isset($update_request['meta_value']) ? $update_request['meta_value'] : false;

        if (!$page_id || !$meta_key || !$meta_value){
            $results[] = 'Invalid request';
            continue;
        }
        
        $update_result = update_post_meta($page_id,$meta_key,$meta_value);
        if (!$update_result){
            $results[] = 'Meta was not updated with the following:' . $page_id . ' ' . $meta_key . ' ' . $meta_value;
        }else{
            $results[] = $update_result;
        }
    }

    return $results;
});

register_ajax_endpoint('get_pages_by_metakey', function ($request,$wpdb){
    
    $key = isset($request['key']) ? $request['key'] : null;
    $post_type = isset($request['post_type']) ? $request['post_type'] : 'page';
    $content_type = isset($request['content_type']) ? $request['content_type'] : false;

    if ($content_type){
        $query = $wpdb->prepare("SELECT posts.post_title,posts.ID,pm.meta_key,pm.meta_value,pm2.meta_value as content_type
                                FROM " . $wpdb->prefix . "posts AS posts
                                INNER JOIN " . $wpdb->prefix . "postmeta AS pm ON pm.post_id = posts.ID
                                INNER JOIN " . $wpdb->prefix . "postmeta AS pm2 ON pm2.post_id = posts.ID
                                WHERE
                                pm.meta_key = %s
                                AND pm2.meta_key = 'dr_content_type' and pm2.meta_value = %s
                                AND posts.post_status = 'publish'
                                AND posts.post_type = %s",$key,$content_type,$post_type);
    }else{
        $query = $wpdb->prepare("SELECT posts.post_title,posts.ID,pm.meta_key,pm.meta_value,pm2.meta_value as content_type
                            FROM " . $wpdb->prefix . "posts AS posts
                            INNER JOIN " . $wpdb->prefix . "postmeta AS pm ON pm.post_id = posts.ID
                            INNER JOIN " . $wpdb->prefix . "postmeta AS pm2 ON pm2.post_id = posts.ID
                            WHERE
                            pm.meta_key = %s
                            AND pm2.meta_key = 'dr_content_type'
                            AND posts.post_status = 'publish'
                            AND posts.post_type = %s",$key,$post_type);
    }

    return $wpdb->get_results($query,ARRAY_A);
});