<div class="wrap" id="app" style="display:none" :style="{display:'block'}">

    <h1 class="wp-heading-inline">FunFunBulk</h1>

    <alert :msg="msg"></alert>

    <div>
        <label>Page Type</label>
        <p class="description">Choose the dynamic list type you want to change.</p>
        
        <select v-model="selected" @change="getPages()">
            <option v-for="option in options" v-bind:value="option.value">
                {{ option.text }}
            </option>
        </select>

        <select v-model="selected_content_type" @change="getPages()">
            <option value="-1">Content Type Filter</option>
            <option v-for="(key,value) in ContentTypes" v-bind:value="value">
                {{ key }}
            </option>
        </select>
    </div>

    <hr />

    <div style="text-align:center;margin-top: 25px;" v-if="main_loader">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            width="50px" height="50px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
            <path fill="#0073aa" d="M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z">
            <animateTransform attributeType="xml"
            attributeName="transform"
            type="rotate"
            from="0 25 25"
            to="360 25 25"
            dur="0.6s"
            repeatCount="indefinite"/>
            </path>
        </svg>
    </div>

    <div v-if="pages.length > 0 && !main_loader">

        <div class="tablenav top">
            <div class="alignleft actions bulkactions">
                <select name="action" id="bulk-action-selector-top" v-model="bulk_list_id" :disabled="selected_pages.length == 0">
                    <option v-for="(key,value) in global_list" v-bind:value="value">
                        {{ key }}
                    </option>
                </select>
                <input type="button"
                       @click="bulkUpdate"
                       id="doaction"
                       class="button action"
                       :disabled="selected_pages.length == 0"
                       :value="'Update ' + selected_pages.length + ' Lists'">
            </div>
        </div><!-- .tablenav -->

        <table class="wp-list-table widefat fixed striped pages">
            <thead>
                <tr>
                    
                    <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1">Select All</label><input id="cb-select-all-1" @change="toggle" type="checkbox"></td>
                    <th scope="col" class="manage-column column-title column-primary">
                        <span>Title</span>
                    </th>
                    <th scope="col" class="manage-column column-title column-primary">
                        <span>Content Type</span>
                    </th>
                    <th scope="col" class="manage-column column-title column-primary">
                        <span>Selected List</span>
                    </th>
                </tr>
            </thead>
            <tbody id="the-list">
                <tr v-for="page in pages">
                    <td><input type="checkbox" v-model="page.checked" /></td>
                    <td><a target="_blank" :href="'//' + window.location.host + '/?p=' + page.ID">{{ page.post_title }}</a></td>
                    <td>{{ page.content_type }}</td>
                    <td>
                        <div v-show="!page.checked">
                            <select v-model="page.meta_value">
                                <option v-for="(key,value) in global_list" v-bind:value="value">
                                    {{ key }}
                                </option>
                            </select>
                            <input type="button"
                                v-if="!page.loading"
                                @click="update(page)"
                                class="button action"
                                value="Update" />
                                <svg version="1.1" style="position:absolute;" v-if="page.loading" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="25px" height="25px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                                    <path fill="#0073aa" d="M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z">
                                    <animateTransform attributeType="xml"
                                    attributeName="transform"
                                    type="rotate"
                                    from="0 25 25"
                                    to="360 25 25"
                                    dur="0.6s"
                                    repeatCount="indefinite"/>
                                    </path>
                                </svg>
                        </div>
                        <i v-show="page.checked">{{ global_list[page.meta_value] }} - Update From Bulk</i>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    
</div>

<script>
    window.UnicornLists = <?php echo json_encode($GLOBALS['unicorn']->getLists()); ?>;
    window.BonusTypes = <?php echo json_encode($GLOBALS['BONUS_TYPES']); ?>;
    window.DesktopOutlinkTypes = <?php echo json_encode($GLOBALS['DESKTOP_OUTLINK_TYPES']); ?>;
    window.MobileOutlinkTypes = <?php echo json_encode($GLOBALS['MOBILE_OUTLINK_TYPES']); ?>;
    window.ContentTypes = <?php echo json_encode($GLOBALS['POST_TYPES']); ?>;
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.3/vue.min.js"></script>
<script src="<?php echo plugins_url('funfunbulk/js/app.js?random=' . time()) ?>"></script>