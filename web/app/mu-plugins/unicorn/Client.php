<?php
/*
Plugin Name: Unicorn
Description: Integration to the Unicorn system.
Author: Ron Masas
Version: 0.0.1
*/

namespace Unicorn;
use \Firebase\JWT\JWT;

class Client {
    
    /**
     * @ver integer
     */
    protected $siteId = 1;

    /**
     * @ver string
     */
    protected $host = UNICORN_URL;

    
    /**
     * Get a collection of all unicorns lists.
     * 
     * @return array
     */
    public function getLists(){
        $response = $this->httpGet('api/v1/sites/'.$this->siteId.'/lists.json');

        if (!$response){
            return [];
        }

        $lists = [];

        foreach($response['data'] as $list){
            $lists[$list['id']] = $list['name'];
        }

        return $lists;
    }

    /**
     * Get the order object for a given extra 
     * 
     * @param string  $placement_name
     * @param integer $page_id
     * 
     * @return array
     */
    public function getOrder($placement_name,$page_id){
        $query_string = [
            'siteId' => $this->siteId,
            'extra' => $placement_name,
            'pageId' => $page_id
        ];

        return $this->httpGet('api/v1/sites/'.$this->siteId.'/list-placements/get-list-from-params.json',$query_string);
    }

    /**
     * Get order for a given list id
     * 
     * @param integer $list_id
     * 
     * @return array
     */
    public function getOrderByListId($list_id){
        $url = sprintf('api/v1/sites/%s/lists/%s.json',$this->siteId,$list_id);
        return $this->httpGet($url,[
            'forSite' => true
        ]);
    }

    /**
     * @param integer $page_id
     * @param integer $list_id
     * @param string  $placement_name
     */
    public function storeListPlacement($page_id,$list_id,$placement_name){
        $response = $this->httpPost('api/v1/sites/'.$this->siteId.'/list-placements.json',[
            'body' => [
                'siteId' => $this->siteId,
                'pageId' => $page_id,
                'listId' => $list_id,
                'extra'  => $placement_name
            ]
        ]);

        return $response;
    }   

    /**
     * Get jwt token
     *
     * return string
     */
    public function getJwtToken() {
        static $_token;
        if(!$_token) {
          $key = UNICORN_SECRET_KEY;
          $issuedAt = time();
          $token = [
              "iss" => $this->host, 
              "aud" => "http://example.com",
              "iat" => $issuedAt,
              "nbf" => $issuedAt,
              "sub" => UNICORN_API_USER          
          ];

          $_token = JWT::encode($token, $key);
        }

        return $_token;
    }

    /**
     * @param string $path
     * @param array  $parameters
     */
    protected function httpPost($path,array $parameters = []) {
        $params = $this->resolveHttpParameters($parameters);
        $response = wp_remote_post($this->host . $path, $params);

        return $this->handleResponse(wp_remote_post($this->host . $path, $params));
    }

    /**
     * @param string $path
     * @param array  $query_parameters
     * @param array  $parameters
     * 
     * @return array
     */
    protected function httpGet($path,array $query_parameters = [],array $parameters = []) {
        $path .= !empty($query_parameters) ? ('?' . http_build_query($query_parameters)) : '';
        
        $params = $this->resolveHttpParameters($parameters);

        return $this->handleResponse(wp_remote_get($this->host . $path, $params));
    }

    /**
     * @param array $response
     */
    protected function handleResponse($response) {
        try {
            return json_decode(wp_remote_retrieve_body($response), true);
        }catch(Exception $httpException) {
            return false;
        }
    }

    /**
     * @param array $custom_parameters
     * 
     * @return array
     */
    protected function resolveHttpParameters($custom_parameters = []){
        $params = [
            'headers' => [
                'jwt_token' => $this->getJwtToken(),
                'Content-Type' => 'application/json; charset=utf-8'
            ]
        ];

        if (isset($custom_parameters['body'])){
            $custom_parameters['body'] = json_encode($custom_parameters['body']);
        }

        return array_merge($custom_parameters,$params);
    }

}
