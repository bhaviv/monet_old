<?php

/** WordPress view bootstrapper */
define('WP_USE_THEMES', true);

/** FunFunCache preloader **/
require(__DIR__ . '/app/plugins/funfuncache/preload.php');

require(__DIR__ . '/wp/wp-blog-header.php');
